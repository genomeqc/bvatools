# Here are release instructions

# Version bump BVATools.java to The next version (remove -dev) 1.7
vim src/main/java/ca/mcgill/genome/bvatools/BVATools.java
vim pom.xml
git commit -m "Version bump to 1.7" src/main/java/ca/mcgill/genome/bvatools/BVATools.java pom.xml

git tag -s 1.7 -m 'Release 1.7'

mvn clean install javadoc:jar source:jar deploy:deploy -DaltDeploymentRepository=mugqic-bitbucket::default::file:///home/mbourgey/work/repo/maven-repo/release/ -DaltReleaseDeploymentRepository=file:///home/mbourgey/work/repo/maven-repo/release/ -DupdateReleaseInfo=true

mvn clean install assembly:single

cd target
mkdir bvatools-1.6/
mv bvatools-1.6-jar-with-dependencies.jar bvatools-1.6/bvatools-1.7-full.jar
cp ../LICENSE.txt bvatools-1.7/
cp -r ../src/main/scripts bvatools-1.7/

zip -r -9 bvatools-1.7.zip bvatools-1.7

# Add the zip to the 'Downloads' of bitbucket

# Version bump BVATools.java to 1.8-dev
cd ..
vim pom.xml src/main/java/ca/mcgill/genome/bvatools/BVATools.java README-release.txt
git commit -m "Version bump to 1.8-dev" src/main/java/ca/mcgill/genome/bvatools/BVATools.java pom.xml README-release.txt
