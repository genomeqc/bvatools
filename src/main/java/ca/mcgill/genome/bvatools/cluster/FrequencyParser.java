/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.cluster;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;

import ca.mcgill.genome.bvatools.util.AlleleCounts;
import ca.mcgill.mcb.pcingola.util.Gpr;

import htsjdk.samtools.Defaults;

public class FrequencyParser implements Callable<SampleFrequency> {
  private final String name;
  private final List<AlleleCounts> positions;
  private final File freqToParse;

  public FrequencyParser(String name, List<AlleleCounts> positions, File freqToParse) {
    super();
    this.name = name;
    this.positions = positions;
    this.freqToParse = freqToParse;
  }

  @Override
  public SampleFrequency call() {
    BufferedReader reader = null;
    SampleFrequency sampleFreq = new SampleFrequency(name, positions.size());
    Iterator<AlleleCounts> posIter = positions.iterator();
    AlleleCounts currentPosition = posIter.next();
    try {
      reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(new FileInputStream(freqToParse), Defaults.BUFFER_SIZE),
          StandardCharsets.US_ASCII));
      int idx = 0;
      while (true) {
        String line = reader.readLine();
        if (line == null) {
          break;
        }

        if (line.length() == 0 || line.startsWith("#") || line.startsWith("Chr")) {
          continue;
        }
        String[] values = Gpr.split(line, ',');
        String chr = values[0];
        if (!currentPosition.getChromosome().equals(chr)) {
          continue;
        }

        int pos = Gpr.parseIntSafe(values[1]);
        if (currentPosition.getPosition() > pos) {
          continue;
        }

        if (!currentPosition.getChromosome().equals(chr) || currentPosition.getPosition() != pos) {
          throw new RuntimeException("Positions don't match: " + currentPosition.getChromosome() + ':' + currentPosition.getPosition() + " vs " + line);
        }

        AlleleCounts sampleCount = AlleleCounts.valueOf(chr, pos, values[2]);
        double alleleADepth = sampleCount.getBamCounts().get(currentPosition.getAlleleA());
        double alleleBDepth = sampleCount.getBamCounts().get(currentPosition.getAlleleB());
        if (alleleBDepth == 0) {
          sampleFreq.getFrequencies()[idx] = 0;
        } else {
          sampleFreq.getFrequencies()[idx] = alleleBDepth / (alleleADepth + alleleBDepth);
        }
        idx++;

        if (!posIter.hasNext()) {
          break;
        }
        currentPosition = posIter.next();
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    } catch (Exception e) {
      System.err.println(e);
      System.err.println("File: " + freqToParse);
      e.printStackTrace();
      throw e;
    } finally {
      if (reader != null) {
        try {
          reader.close();
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      }
    }
    if (posIter.hasNext()) {
      throw new RuntimeException("There are less allele frequencies in the sample than in the reference position file");
    }
    return sampleFreq;
  }
}
