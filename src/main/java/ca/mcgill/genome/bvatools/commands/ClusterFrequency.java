/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import ca.mcgill.genome.bvatools.DefaultTool;
import ca.mcgill.genome.bvatools.cluster.FrequencyParser;
import ca.mcgill.genome.bvatools.cluster.SampleFrequency;
import ca.mcgill.genome.bvatools.util.AlleleCounts;
import ca.mcgill.mcb.pcingola.util.Gpr;

import htsjdk.samtools.Defaults;

public class ClusterFrequency extends DefaultTool {
  private int threads = 1;
  private String prefix = null;
  private boolean outputFreq = false;
  private List<String> names = new ArrayList<>();
  private List<File> freqs = new ArrayList<>();
  private File snppos = null;

  @Override
  public String getCmdName() {
    return "clustfreq";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("\t--snppos        <file>          SNP positions in TSV with alleles. chr<TAB>1-basedPosition<TAB>ID<TAB>Allele_A<TAB>Allele_B");
    System.out.println("\t--freq          <name> <file>   Frequency file to use. Can be given multiple times");
    System.out.println("\t--threads       <int>           Threads to use. (default: " + threads + ")");
    System.out.println("\t--prefix        <path>          Output prefix");
    System.out.println("\t--outputFreq                    Output frequency vector (default: " + outputFreq + ")");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {
    while (!arguments.isEmpty()) {
      String arg = arguments.pop();
      switch (arg) {
      case "--snppos":
        snppos = new File(arguments.pop());
        break;
      case "--freq":
        names.add(arguments.pop());
        freqs.add(new File(arguments.pop()));
        break;
      case "--threads":
        threads = Integer.parseInt(arguments.pop());
        break;
      case "--outputFreq":
        outputFreq = true;
        break;
      case "--prefix":
        prefix = arguments.pop();
        break;
      default:
        unusedArgs.add(arg);
        break;
      }
    }

    if (snppos == null) {
      printUsage("snppos not set");
      return 1;
    }

    if (freqs.size() == 0) {
      printUsage("At least one freq file needs to be specified");
      return 1;
    }
    if (prefix == null) {
      printUsage("A prefix needs to be specified");
      return 1;
    }
    return 0;
  }

  @Override
  public int run() {
    List<AlleleCounts> positions = parsePositions(snppos);
    clusterFrequencies(names, freqs, positions);
    return 0;
  }

  public List<AlleleCounts> parsePositions(File positionsFile) {
    List<AlleleCounts> retVal = new ArrayList<>(500000);

    try (BufferedReader positionReader = new BufferedReader(new InputStreamReader(new FileInputStream(positionsFile), StandardCharsets.US_ASCII),
        Defaults.BUFFER_SIZE)) {

      while (true) {
        String posLine;
        posLine = positionReader.readLine();
        if (posLine == null) {
          break;
        }

        if (posLine.length() > 0 && posLine.charAt(0) == '#') {
          continue;
        }

        String[] posValues = Gpr.split(posLine, '\t');
        String posChr = posValues[0];
        AlleleCounts ac = new AlleleCounts(posChr, Integer.parseInt(posValues[1]));
        ac.setAlleleA(posValues[2]);
        ac.setAlleleB(posValues[3]);
        retVal.add(ac);
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    return retVal;
  }

  public void clusterFrequencies(List<String> names, List<File> freqs, List<AlleleCounts> positions) {
    ExecutorService workExecutor = Executors.newFixedThreadPool(threads);

    try {
      List<Future<SampleFrequency>> frequencyFutures = new ArrayList<>();
      for (int idx = 0; idx < freqs.size(); idx++) {
        frequencyFutures.add(workExecutor.submit(new FrequencyParser(names.get(idx), positions, freqs.get(idx))));
      }

      List<SampleFrequency> sampleFreqs = new ArrayList<>(frequencyFutures.size());
      for (Future<SampleFrequency> future : frequencyFutures) {
        SampleFrequency sampleFreq = future.get();
        sampleFreqs.add(sampleFreq);
      } // for futures

      double[][] distances = new double[sampleFreqs.size()][sampleFreqs.size()];

      List<Future<?>> futures = new ArrayList<>();
      for (int x = 0; x < sampleFreqs.size(); x++) {
        SampleFrequency xFreq = sampleFreqs.get(x);
        for (int y = 0; y < sampleFreqs.size(); y++) {
          futures.add(workExecutor.submit(new EuclideanComputer(x, y, xFreq, sampleFreqs.get(y), distances)));
        }
      }

      for (Future<?> future : futures) {
        future.get();
      }

      printDistances(sampleFreqs, distances);
    } catch (InterruptedException | ExecutionException | FileNotFoundException e) {
      throw new RuntimeException(e);
    } finally {
      workExecutor.shutdownNow();
    }
  }

  private void printDistances(List<SampleFrequency> sampleFreqs, double[][] distances) throws FileNotFoundException {

    PrintStream freqWriter = null;
    try (PrintStream writer = new PrintStream(new BufferedOutputStream(new FileOutputStream(new File(prefix + ".dist.csv")), Defaults.BUFFER_SIZE), false,
        StandardCharsets.US_ASCII)) {
      if (outputFreq) {
        freqWriter = new PrintStream(new BufferedOutputStream(new FileOutputStream(new File(prefix + ".freq.csv")), Defaults.BUFFER_SIZE), false,
            StandardCharsets.US_ASCII);
      }

      for (SampleFrequency sampleFreq : sampleFreqs) {
        if (freqWriter != null) {
          freqWriter.print(sampleFreq.getName());
          for (double freq : sampleFreq.getFrequencies()) {
            freqWriter.print(',');
            freqWriter.print(freq);
          }
          freqWriter.println();
        }

        writer.print(',');
        writer.print(sampleFreq.getName());
      }
      if (freqWriter != null) {
        freqWriter.close();
      }
      writer.println();

      for (int y = 0; y < sampleFreqs.size(); y++) {
        writer.print(sampleFreqs.get(y).getName());
        for (int x = 0; x < sampleFreqs.size(); x++) {
          writer.print(',');
          writer.print(distances[x][y]);
        }
        writer.println();
      }
    }

  }

  private static class EuclideanComputer implements Runnable {
    private final double[][] distances;
    private final SampleFrequency xFreqs;
    private final SampleFrequency yFreqs;
    private final int x;
    private final int y;

    public EuclideanComputer(int x, int y, SampleFrequency xFreqs, SampleFrequency yFreqs, double[][] distances) {
      super();
      this.distances = distances;
      this.xFreqs = xFreqs;
      this.yFreqs = yFreqs;
      this.x = x;
      this.y = y;
    }

    @Override
    public void run() {
      double sum = 0.0;
      for (int i = 0; i < xFreqs.getFrequencies().length; i++) {
        double distance = xFreqs.getFrequencies()[i] - yFreqs.getFrequencies()[i];
        sum = sum + Math.pow(distance, 2.0);
      }
      distances[x][y] = Math.sqrt(sum);
    }
  }
}
