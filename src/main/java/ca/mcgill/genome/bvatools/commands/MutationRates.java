/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import gnu.trove.map.hash.TObjectLongHashMap;
import htsjdk.samtools.util.CloseableIterator;
import htsjdk.tribble.AbstractFeatureReader;
import htsjdk.tribble.bed.BEDCodec;
import htsjdk.tribble.bed.BEDFeature;
import htsjdk.tribble.readers.LineIterator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import ca.mcgill.genome.bvatools.DefaultTool;
import ca.mcgill.genome.bvatools.experimental.SamtoolsPairFilter;
import ca.mcgill.genome.bvatools.mutations.MutationRateComputer;
import ca.mcgill.genome.bvatools.mutations.MutationRateComputer.VcfFileInfo;
import ca.mcgill.genome.bvatools.mutations.MutationRateTarget;
import ca.mcgill.genome.bvatools.mutations.Region;
import ca.mcgill.genome.bvatools.mutations.SynchronizedRegion;
import ca.mcgill.mcb.pcingola.interval.ChromosomeSimpleName;
import ca.mcgill.mcb.pcingola.interval.Genome;
import ca.mcgill.mcb.pcingola.interval.Marker;
import ca.mcgill.mcb.pcingola.interval.Markers;

public class MutationRates extends DefaultTool {
  private List<VcfFileInfo> vcfFiles;
  private File output;
  private int minGenotypeQual = 0;
  private double minQual = 0.0;
  private String filter = null;
  private Map<String, Region> regions;
  private long totalSize = 3095693981l;
  private byte threads = 1;
  private Set<String> toExclude = new HashSet<String>();

  @Override
  public String getCmdName() {
    return "mutationrates";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("Tnis tool uses the "+SamtoolsPairFilter.SOMATIC_FLAG+" flag in the info field to decide what is SOMATIC");
    System.out.println("\t--vcfsFile       File containing a list of VCF files to get positions to validate from");
    System.out.println("\t                 Optionally, 2 columns can be added at the beginning to specify which sample");
    System.out.println("\t                 is the normal and which is the tumor. Default, normal is first, tumor is second.");
    System.out.println("\t                 Possible Formats:");
    System.out.println("\t                 FILE_PATH");
    System.out.println("\t                 NORMAL,TUMOR,FILEPATH");
    System.out.println("\t--vcf            VCFs to get regions for");
    System.out.println("\t--exclude        Chromosomes to exclude. Coma seperated");
    System.out.println("\t--bed            Region to use. Can be given multiple times.");
    System.out.println("\t                 You can use NAME:FILE to use another name in the report");
    System.out.println("\t--minQual        Minimum variant quality. (Default: " + minQual + ")");
    System.out.println("\t--minGQ          Minimum genotype quality. (Default: " + minGenotypeQual + ")");
    System.out.println("\t--filter         This keyword must be in the filter field");
    System.out.println("\t--totalSize      Genome size. (Default: " + totalSize + ")");
    System.out.println("\t--output         Output file for stats");
    System.out.println("\t--threads        Threads to use. (default: " + threads + ")");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {
    File vcfsFile = null;
    vcfFiles = new ArrayList<VcfFileInfo>();
    regions = new HashMap<String, Region>();
    while (!arguments.isEmpty()) {
      String arg = arguments.pop();
      if (arg.equals("--vcf")) {
        vcfFiles.add(new VcfFileInfo(new File(arguments.pop())));
      } else if (arg.equals("--vcfsFile")) {
        vcfsFile = new File(arguments.pop());
      } else if (arg.equals("--output")) {
        output = new File(arguments.pop());
      } else if (arg.equals("--exclude")) {
        String extraArgs = arguments.pop();
        for (String chr : extraArgs.split(",")) {
          toExclude.add(chr);
        }
      } else if (arg.equals("--bed")) {
        Region region = parseBED(arguments.pop());
        regions.put(region.getName(), region);
      } else if (arg.equals("--minGQ")) {
        minGenotypeQual = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--minQual")) {
        minQual = Double.parseDouble(arguments.pop());
      } else if (arg.equals("--filter")) {
        filter = arguments.pop();
      } else if (arg.equals("--totalSize")) {
        totalSize = Long.parseLong(arguments.pop());
      } else if (arg.equals("--threads")) {
        threads = Byte.parseByte(arguments.pop());
      } else {
        unusedArgs.add(arg);
      }
    }
    
    BufferedReader reader = null;
    try {
      if (vcfsFile != null) {
        reader = new BufferedReader(new InputStreamReader(new FileInputStream(vcfsFile)));
        while (true) {
          String line = reader.readLine();
          if (line == null)
            break;
          if (line.startsWith("#") || line.length() == 0)
            continue;
          if(line.indexOf(',') != -1) {
            String values[] = line.split(",");
            if(values.length != 3) {
              printUsage("vcfsFile can contain either just a path or normal,tumor,path. Nothing else. Line is missing columns or has too many.");
              return 1;
            }
            vcfFiles.add(new VcfFileInfo(new File(values[2]), values[0], values[1]));
            
          }
          else {
            vcfFiles.add(new VcfFileInfo(new File(line)));
          }
        }
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    finally {
      if(reader != null) {
        try {
          reader.close();
        } catch (IOException e) {
          // Ignored
          e.printStackTrace();
        }
      }
    }

    if (vcfFiles.size() == 0) {
      printUsage("Missing vcfFiles");
      return 1;
    }
    if (output == null) {
      printUsage("Missing output");
      return 1;
    }

    for(Region region : regions.values()) {
      region.build();
    }

    return 0;
  }

  @Override
  public int run() {
    computeRates();
    return 0;
  }

  public void computeRates() {
    MutationRateTarget somaticMutRateTarget = new MutationRateTarget();
    MutationRateTarget tumorMutRateTarget = new MutationRateTarget();
    MutationRateTarget normalMutRateTarget = new MutationRateTarget();

    ExecutorService rateExecutor = Executors.newFixedThreadPool(threads);
    try {
      List<Future<MutationRateComputer>> futures = new ArrayList<Future<MutationRateComputer>>();
      for (VcfFileInfo vcfFileInfo : vcfFiles) {
        MutationRateComputer mutComputer = new MutationRateComputer(vcfFileInfo, regions, toExclude, minQual, minGenotypeQual, filter);
        futures.add(rateExecutor.submit(mutComputer, mutComputer));
      }
      for (Future<MutationRateComputer> future : futures) {
        MutationRateComputer mutComputer = future.get();
        somaticMutRateTarget.add(mutComputer.getSomaticMutRateTarget());
        tumorMutRateTarget.add(mutComputer.getTumorMutRateTarget());
        normalMutRateTarget.add(mutComputer.getNormalMutRateTarget());
      }
      futures = null;
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    } catch (ExecutionException e) {
      throw new RuntimeException(e);
    } finally {
      rateExecutor.shutdownNow();
      rateExecutor = null;
    }

    List<String> orderedNames = new ArrayList<String>();
    for (String regionName : regions.keySet()) {
      orderedNames.add(regionName);
    }

    System.err.println("Writing output...");
    PrintWriter writer;
    try {
      writer = new PrintWriter(output, "ASCII");
      writeRegionsHeader(writer,orderedNames);
      writer.println();
      writer.print("Sample");
      writer.print('\t');
      writeHeader(writer, "Somatic", orderedNames);
      writer.print('\t');
      writeHeader(writer, "Tumor", orderedNames);
      writer.print('\t');
      writeHeader(writer, "Normal", orderedNames);
      writer.print('\t');
      writer.println();

      for (String sampleName : somaticMutRateTarget.getCountPerTypePerSample().keySet()) {
        writer.print(sampleName);
        writeCounts(writer, sampleName, somaticMutRateTarget, orderedNames);
        writeCounts(writer, sampleName, tumorMutRateTarget, orderedNames);
        writeCounts(writer, sampleName, normalMutRateTarget, orderedNames);
        writer.println();
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    writer.close();
  }

  private void writeCounts(PrintWriter writer, String sampleName, MutationRateTarget mutRateTarget, List<String> orderedNames) {
    TObjectLongHashMap<String> sampleMap = mutRateTarget.getCountPerTypePerSample().get(sampleName);
    writer.print('\t');
    writer.print(sampleMap.get("total"));
    for (String name : orderedNames) {
      writer.print('\t');
      writer.print(sampleMap.get(name));
    }

    long totalCount = 0;
    for (String name : MutationRateTarget.substitutionOrder) {
      writer.print('\t');
      totalCount += mutRateTarget.getSubstitutionCountPerSample().get(sampleName).get(name);
      writer.print(mutRateTarget.getSubstitutionCountPerSample().get(sampleName).get(name));
    }
    if (totalCount != sampleMap.get("total")) {
      System.err.println("Wrong total: " + sampleName);
    }

    for (String regionName : orderedNames) {
      int totalPerRegion=0;
      for (String name : MutationRateTarget.substitutionOrder) {
        writer.print('\t');
        writer.print((mutRateTarget.getSubstitutionCountPerTypePerSample().get(sampleName).get(name + '_' + regionName)));
        totalPerRegion += mutRateTarget.getSubstitutionCountPerTypePerSample().get(sampleName).get(name + '_' + regionName);
      }
      if (totalPerRegion != sampleMap.get(regionName)) {
        System.err.println("Wrong total on region '"+regionName+"' : " + sampleName);
      }
    }
  }

  private void writeRegionsHeader(PrintWriter writer, List<String> orderedNames) {
    writer.println("RegionName\tSize");
    for (String regionName : orderedNames) {
      writer.print(regionName);
      writer.print('\t');
      writer.println(regions.get(regionName).getRegionSize());
    }
  }

  private void writeHeader(PrintWriter writer, String prefix, List<String> orderedNames) {
    writer.print(prefix + "Total");
    for (String name : orderedNames) {
      writer.print("\t" + prefix);
      writer.print(name + "_counts");
    }
    for (String name : MutationRateTarget.substitutionOrder) {
      writer.print("\t" + prefix + '_');
      writer.print(name + "_counts");
    }
    for (String regionName : orderedNames) {
      for (String name : MutationRateTarget.substitutionOrder) {
        writer.print("\t" + prefix + '_');
        writer.print(name + '_' + regionName + "_counts");
      }
    }
  }

  public Region parseBED(String bed) {
    int sep = bed.indexOf(':');
    String name;
    File file;
    if(sep != -1) {
      name = bed.substring(0, sep);
      file = new File(bed.substring(sep+1));
    }
    else if(bed.toLowerCase().endsWith(".bed")){
      name = bed.substring(0, bed.length()-4);
      file = new File(bed);
    }
    else {
      name = bed;
      file = new File(bed);
    }

    Region region = new SynchronizedRegion(name);
    AbstractFeatureReader<BEDFeature, LineIterator> featureReader = null;
    try {
      featureReader = AbstractFeatureReader.getFeatureReader(file.getAbsolutePath(), new BEDCodec(), false);
      CloseableIterator<BEDFeature> iter = featureReader.iterator();
      Genome genome = new Genome();
      // Flatten the region so the size is representative and not inflated
      Markers regionMarkers = new Markers();
      while(iter.hasNext()) {
        BEDFeature feature = iter.next();
        String chr = feature.getChr();
        chr = ChromosomeSimpleName.get(chr);
  
        Marker interval = new Marker(genome.getOrCreateChromosome(chr), feature.getStart(), feature.getEnd(), false, name);
        regionMarkers.add(interval);
      }
      iter.close();

      Markers regionsMerged = regionMarkers.merge();
      for (Marker marker : regionsMerged) {
        region.add(marker);
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    finally {
      if(featureReader != null)
        try {
          featureReader.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
    }

    return region;
  }
}
