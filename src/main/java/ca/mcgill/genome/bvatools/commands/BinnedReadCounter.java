/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;

import ca.mcgill.genome.bvatools.DefaultTool;
import ca.mcgill.genome.bvatools.util.Bin;
import ca.mcgill.genome.bvatools.util.GenomeBins;

import gnu.trove.list.array.TDoubleArrayList;
import htsjdk.samtools.BAMIndexMetaData;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMSequenceRecord;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;
import htsjdk.samtools.reference.ReferenceSequence;
import htsjdk.samtools.reference.ReferenceSequenceFile;
import htsjdk.samtools.reference.ReferenceSequenceFileFactory;
import htsjdk.samtools.util.StringUtil;

public class BinnedReadCounter extends DefaultTool {
  public enum NormalizeBy {
    CHR, GENOME
  }

  private int minMapQ = 0;
  private NormalizeBy normalizeBy = NormalizeBy.GENOME;
  private int window = 200;
  private ReferenceSequenceFile reference = null;
  private boolean computeGC = false;
  private File sampleBAM = null;
  private File refBAM = null;

  @Override
  public String getCmdName() {
    return "bincounter";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("\t--minMapQ   Min mapping quality (default " + minMapQ + ")");
    System.out.println("\t--gc        Compute GC% per window (needs --ref)");
    System.out.println("\t--ref       Indexed Reference Genome. (optional)");
    System.out.println("\t--refbam    Reference (or normal in cancer) BAM to count");
    System.out.println("\t--bam       BAM to count");
    System.out.println("\t--norm      Normalize with chromosome: 'chr' or genome: 'genome'. (default " + normalizeBy.toString().toLowerCase() + ")");
    System.out.println("\t--window    Window interval (default " + window + "bp)");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {
    while (!arguments.isEmpty()) {
      String arg = arguments.pop();

      switch (arg) {
      case "--minMapQ":
        minMapQ = Integer.parseInt(arguments.pop());
        break;
      case "--gc":
        computeGC = true;
        break;
      case "--ref":
        reference = ReferenceSequenceFileFactory.getReferenceSequenceFile(new File(arguments.pop()));
        break;
      case "--bam":
        sampleBAM = new File(arguments.pop());
        break;
      case "--refbam":
        refBAM = new File(arguments.pop());
        break;
      case "--window":
        window = Integer.parseInt(arguments.pop());
        break;
      case "--norm":
        normalizeBy = Enum.valueOf(NormalizeBy.class, arguments.pop().toUpperCase());
        break;
      default:
        unusedArgs.add(arg);
        break;
      }
    }

    if (sampleBAM == null) {
      printUsage("bam not set");
      return 1;
    }
    if (computeGC && reference == null) {
      printUsage("Need a reference to compute GC%");
      return 1;
    }
    return 0;
  }

  @Override
  public int run() {

    GenomeBins bamBins = getBins(sampleBAM);
    GenomeBins refBins = null;
    if (refBAM != null) {
      refBins = getBins(refBAM);
    }
    computeRatio(bamBins, refBins);

    return 0;
  }

  public void computeRatio(GenomeBins bamBins, GenomeBins refBamBins) {
    PrintStream writer = System.out;

    writer.print("chr\tstart\tend\tsample_raw\tref_raw\tsample_normalized\tref_normalized\tln(sample/ref)");
    Map<String, TDoubleArrayList> chr2GCBin = new HashMap<>();
    if (computeGC) {
      writer.print("\tGC%");
      computeGC(chr2GCBin);
    }
    writer.println();

    for (String chr : bamBins.getChromosomeNames()) {
      TDoubleArrayList gcBins = chr2GCBin.get(chr);
      Bin[] bins = bamBins.getChromosomeBins(chr);
      Bin[] refBins = null;
      if (refBamBins != null) {
        refBins = refBamBins.getChromosomeBins(chr);
      }
      for (int idx = 0; idx < bins.length; idx++) {
        double normalizedCount;
        double normalizedRefCount = -1;
        if (normalizeBy == NormalizeBy.GENOME) {
          normalizedCount = (double) bins[idx].getCount() / (double) bamBins.getTotalHits();
          if (refBamBins != null) {
            normalizedRefCount = (double) refBins[idx].getCount() / (double) refBamBins.getTotalHits();
          }
        } else if (normalizeBy == NormalizeBy.CHR) {
          normalizedCount = (double) bins[idx].getCount() / (double) bamBins.getChromosomeHitCounts(chr);
          if (refBamBins != null) {
            normalizedRefCount = (double) refBins[idx].getCount() / (double) refBamBins.getChromosomeHitCounts(chr);
          }
        } else {
          throw new RuntimeException("Unknown normalization");
        }

        writer.print(chr);
        writer.print('\t');
        writer.print(bins[idx].getStart());
        writer.print('\t');
        writer.print(bins[idx].getStop());
        writer.print('\t');
        writer.print(bins[idx].getCount());
        writer.print('\t');
        if (refBins != null) {
          writer.print(refBins[idx].getCount());
        } else {
          writer.print("");
        }
        writer.print('\t');
        writer.print(normalizedCount);
        writer.print('\t');
        if (normalizedRefCount != -1) {
          writer.print(normalizedRefCount);
        } else {
          writer.print("");
        }
        writer.print('\t');
        if (normalizedRefCount != -1) {
          writer.print(Math.log(normalizedCount / normalizedRefCount));
        } else {
          writer.print("");
        }
        if (computeGC) {
          writer.print('\t');
          writer.print(gcBins.get(idx));
        }
        writer.println();
      }
    }
  }

  private void computeGC(Map<String, TDoubleArrayList> chr2GCBin) {
    ReferenceSequence ref;
    while ((ref = reference.nextSequence()) != null) {
      final byte[] refBases = ref.getBases();
      StringUtil.toUpperCase(refBases);
      final int refLength = refBases.length;

      TDoubleArrayList gcBins = new TDoubleArrayList((int) Math.ceil((double) refLength / (double) window));
      chr2GCBin.put(ref.getName(), gcBins);

      long totalBases = 0;
      long totalGC = 0;
      int windowIdx = -1;
      for (int idx = 0; idx < refLength; idx++) {
        if (idx % window == 0) {
          if (windowIdx != -1) {
            gcBins.add((double) totalGC / (double) totalBases);
          }
          windowIdx++;
          totalBases = 0;
          totalGC = 0;
        }
        totalBases++;
        if (refBases[idx] == 'C' || refBases[idx] == 'G') {
          totalGC++;
        }
      }
      gcBins.add((double) totalGC / (double) totalBases);
    }
  }

  public GenomeBins getBins(File input) {
    System.err.println("Processing: " + input);
    SamReaderFactory samReaderFactory = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
    SamReader samReader = samReaderFactory.open(input);
    GenomeBins retVal = new GenomeBins(window);

    long totalNbReads = 0;
    for (SAMSequenceRecord seqRecord : samReader.getFileHeader().getSequenceDictionary().getSequences()) {
      retVal.addChromosome(seqRecord.getSequenceName(), seqRecord.getSequenceLength());
      if (samReader.hasIndex()) {
        BAMIndexMetaData metData = samReader.indexing().getIndex().getMetaData(seqRecord.getSequenceIndex());
        totalNbReads += metData.getAlignedRecordCount();
        totalNbReads += metData.getUnalignedRecordCount();
      }
    }

    long prevPercent = 0;
    long currentNbReads = 0;
    for (SAMRecord record : samReader) {
      if (!record.getReadUnmappedFlag() && record.getMappingQuality() >= minMapQ) {
        retVal.addRecord(record);
      }
      if (totalNbReads != 0) {
        long percent = currentNbReads * 100 / totalNbReads;
        if (percent != prevPercent) {
          prevPercent = percent;
          System.err.print('\r');
          System.err.print(percent);
        }
        currentNbReads++;
      }
    }
    if (totalNbReads != 0) {
      System.err.println("\r100%");
    }
    try {
      samReader.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return retVal;
  }
}
