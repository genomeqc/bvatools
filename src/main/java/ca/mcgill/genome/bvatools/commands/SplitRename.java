/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.SAMFileWriter;
import htsjdk.samtools.SAMFileWriterFactory;
import htsjdk.samtools.SAMProgramRecord;
import htsjdk.samtools.SAMReadGroupRecord;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.mcgill.genome.bvatools.DefaultTool;

public class SplitRename extends DefaultTool {
  private File bam = null;
  private String outputPrefix = null;
  private String originalName = null;
  private String newName = null;

  @Override
  public String getCmdName() {
    return "splitrename";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("\t--bam           BAM file");
    System.out.println("\t--outputPrefix  Output BAM");
    System.out.println("\t--originalName  Original sample name to rename");
    System.out.println("\t--newName       New name to use");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {
    while (!arguments.isEmpty()) {
      String arg = arguments.pop();
      if (arg.equals("--bam")) {
        bam = new File(arguments.pop());
      } else if (arg.equals("--outputPrefix")) {
        outputPrefix = arguments.pop();
      } else if (arg.equals("--originalName")) {
        originalName = arguments.pop();
      } else if (arg.equals("--newName")) {
        newName = arguments.pop();
      } else {
        unusedArgs.add(arg);
      }
    }

    if (bam == null) {
      printUsage("bam not set");
      return 1;
    }
    if (outputPrefix == null) {
      printUsage("bam not set");
      return 1;
    }

    return 0;
  }

  @Override
  public int run() {
    
    SamReaderFactory samReaderFactory = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
    SamReader in = samReaderFactory.open(bam);

    Map<String, List<SAMReadGroupRecord>> runReadGroups = new HashMap<String, List<SAMReadGroupRecord>>();
    SAMFileHeader header = in.getFileHeader();
    for (SAMReadGroupRecord rg : header.getReadGroups()) {
      String runId = rg.getPlatformUnit();
      int laneSepIndex = runId.lastIndexOf('_');
      if (laneSepIndex != -1) {
        runId = runId.substring(0, laneSepIndex);
      }

      if (!runReadGroups.containsKey(runId)) {
        runReadGroups.put(runId, new ArrayList<SAMReadGroupRecord>());
      }
      rg.setSample(rg.getSample().replace(originalName, newName));
      runReadGroups.get(runId).add(rg);
    }

    Map<String, SAMFileWriter> writers = new HashMap<String, SAMFileWriter>();
    Map<String, SAMFileWriter> runWriters = new HashMap<String, SAMFileWriter>();
    for (String runId : runReadGroups.keySet()) {
      SAMFileHeader outHeader = header.clone();
      outHeader.setReadGroups(runReadGroups.get(runId));

      // Might be unsafe to blindly replace...
      for (SAMProgramRecord pgr : outHeader.getProgramRecords()) {
        if (pgr.getCommandLine() != null) {
          pgr.setCommandLine(pgr.getCommandLine().replace(originalName, newName));
        }
      }
      SAMFileWriter writer = new SAMFileWriterFactory().makeSAMOrBAMWriter(outHeader, true, new File(outputPrefix + newName + '.' + runId + ".bam"));
      runWriters.put(runId, writer);
      for (SAMReadGroupRecord rg : runReadGroups.get(runId)) {
        writers.put(rg.getId(), writer);
      }
    }

    for (final SAMRecord read : in) {
      writers.get(read.getReadGroup().getId()).addAlignment(read);
    }

    // cleanup
    try {
      in.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    for (String runId : runWriters.keySet()) {
      runWriters.get(runId).close();
    }
    return 0;
  }
}
