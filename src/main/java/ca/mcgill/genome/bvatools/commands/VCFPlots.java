/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import gnu.trove.list.array.TFloatArrayList;
import gnu.trove.list.array.TIntArrayList;
import htsjdk.samtools.SAMSequenceDictionary;
import htsjdk.samtools.SAMSequenceRecord;
import htsjdk.samtools.util.IOUtil;
import htsjdk.tribble.Feature;
import htsjdk.tribble.SimpleFeature;

import java.io.File;
import java.io.IOException;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

import ca.mcgill.genome.bvatools.DefaultTool;
import ca.mcgill.genome.bvatools.experimental.SamtoolsPairFilter;
import ca.mcgill.genome.bvatools.graphs.DepthRatioBAlleleFreqPlot;
import ca.mcgill.genome.bvatools.graphs.DepthRatioBAlleleFreqPlot.DepthRatioBAF;
import ca.mcgill.genome.bvatools.util.SAMUtils;
import ca.mcgill.mcb.pcingola.fileIterator.VcfFileIterator;
import ca.mcgill.mcb.pcingola.util.Gpr;
import ca.mcgill.mcb.pcingola.vcf.VcfEntry;
import ca.mcgill.mcb.pcingola.vcf.VcfHeader;

/**
 * @deprecated Should use VCFBAF instead, but depth computation not done yet. It should be inspired by this class.
 *  
 * @author lletourn
 *
 */
public class VCFPlots extends DefaultTool {
  public static final String substitutionOrder[] = { "A>T", "A>C", "A>G", "C>A", "C>G", "C>T", "G>A", "G>C", "G>T", "T>A", "T>C", "T>G" };
  
  private SAMSequenceDictionary referenceDictionary;
  private int width = 1280;
  private int height = 768;
  private int minDepth = 20;
  private int window = 3000;
  private String somaticFlag = SamtoolsPairFilter.SOMATIC_FLAG;
  private String adKey = VCFFrequency.ALLELE_DEPTH_KEY;
  private String filter = "";
  private String sampleName = null;
  private boolean generateSVG = false;
  private Set<String> toExclude = new HashSet<String>();
  private Set<String> depthExclusion = new HashSet<String>();
  private File vcf = null;
  private String outputPrefix = null;


  @Override
  public String getCmdName() {
    return "vcfplots";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("\t--vcf         Input VCF");
    System.out.println("\t--prefix      Output file prefix");
    System.out.println("\t--refdict     Reference dictionary");
    System.out.println("\t--width       Plot width. (default: " + width + ")");
    System.out.println("\t--height      Plot height. (default: " + height + ")");
    System.out.println("\t--minDepth    Minimum depth on which to use a position. (default: " + minDepth + ")");
    System.out.println("\t--exclude     Chromosomes to exclude. Comma seperated");
    System.out.println("\t--sampleName  Sample to use (default: first sample)");
    System.out.println("\t--somaticFlag Info flag to use for somatic variants (default: "+somaticFlag+")");
    System.out.println("\t--filter      Filter on this (default: "+filter+")");
    System.out.println("\t--adKey       Depth key name to use. Can be switched to AD to resemble Haplotype Caller or MuTect. default("+adKey+")");
    System.out.println("\t--svg         Generate SVG of the plot. Can crash if plot is too big");
    System.out.println("\t--window      Window size for Depth Ratio/BAF plots. (default: " + window + ")");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {
    while (!arguments.isEmpty()) {
      String arg = arguments.pop();
      if (arg.equals("--vcf")) {
        vcf = new File(arguments.pop());
      } else if (arg.equals("--minDepth")) {
        minDepth = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--width")) {
        width = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--height")) {
        height = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--svg")) {
        generateSVG = true;
      } else if (arg.equals("--prefix")) {
        outputPrefix = arguments.pop();
      } else if (arg.equals("--sampleName")) {
        sampleName = arguments.pop();
      } else if (arg.equals("--somaticFlag")) {
        somaticFlag = arguments.pop();
      } else if (arg.equals("--filter")) {
        filter = arguments.pop();
      } else if (arg.equals("--adKey")) {
        adKey = arguments.pop();
      } else if (arg.equals("--refdict")) {
        referenceDictionary = SAMUtils.getSequenceDictionary(new File(arguments.pop()));
      } else if (arg.equals("--exclude")) {
        arg = arguments.pop();
        for (String chr : arg.split(",")) {
          toExclude.add(chr);
        }
      } else if (arguments.pop().equals("--window")) {
        window = Integer.parseInt(arguments.pop());
      } else {
        unusedArgs.add(arg);
      }
    }
    if (vcf == null) {
      printUsage("vcf not set");
      return 1;
    }
    if (outputPrefix == null) {
      printUsage("prefix not set");
      return 1;
    }
    if (referenceDictionary == null) {
      printUsage("refdict not set");
      return 1;
    }
    if(minDepth < 1) {
      printUsage("minDepth needs to at least be 1");
      return 1;
    }
    return 0;
  }

  @Override
  public int run() {
    depthExclusion.add("X");
    depthExclusion.add("Y");
    depthExclusion.add("chrX");
    depthExclusion.add("chrY");
    depthExclusion.add("MT");
    depthExclusion.add("chrM");

    try {
      computePlots(vcf, outputPrefix);
    }
    catch(IOException e) {
      throw new RuntimeException(e);
    }

    return 0;
  }

  public void computePlots(File vcf, String outputPrefix) throws IOException {
    LinkedHashMap<String, Feature> genome = new LinkedHashMap<String, Feature>();
    TIntArrayList allDepth = new TIntArrayList();
    long depthSum=0;
    LinkedHashMap<String, TIntArrayList[]> chrLRR = new LinkedHashMap<String, TIntArrayList[]>();
    LinkedHashMap<String, TFloatArrayList[]> chrBAF = new LinkedHashMap<String, TFloatArrayList[]>();
    for (SAMSequenceRecord seq : referenceDictionary.getSequences()) {
      if(!toExclude.contains(seq.getSequenceName())) {
        Feature chr = new SimpleFeature(seq.getSequenceName(), 1, seq.getSequenceLength());
        genome.put(chr.getChr(), chr);
        
        TIntArrayList lrrList[] = new TIntArrayList[(int)Math.ceil((double)seq.getSequenceLength()/(double)window)];
        TFloatArrayList bafList[] = new TFloatArrayList[(int)Math.ceil((double)seq.getSequenceLength()/(double)window)];
        for(int i=0; i< lrrList.length; i++) {
          lrrList[i] = new TIntArrayList();
          bafList[i] = new TFloatArrayList();
        }        
        chrLRR.put(seq.getSequenceName(), lrrList);
        chrBAF.put(seq.getSequenceName(), bafList);
      }
    }
    
    VcfFileIterator vcfParser = new VcfFileIterator(IOUtil.openFileForBufferedReading(vcf));
    int sampleIdx = 0;
    if(sampleName != null) {
      VcfHeader header = vcfParser.readHeader();
      sampleIdx = -1;
      for(int i=0; i < header.getSampleNames().size(); i++) {
        if(sampleName.equals(header.getSampleNames().get(i))) {
          sampleIdx = i;
          break;
        }
      }
      if(sampleIdx == -1)
        throw new RuntimeException("Sample "+sampleName+" was not found in vcf");
    }

    for (VcfEntry vcfEntry : vcfParser) {
      if(!vcfEntry.isSnp() || vcfEntry.getRef().indexOf('N') != -1 || vcfEntry.getAltsStr().indexOf('N') != -1) {
        continue;
      }

      // DepthRatio
      String depthField[] = vcfEntry.getVcfGenotype(sampleIdx).get(adKey).split(","); 
      int refDP = Gpr.parseIntSafe(depthField[0]);
      int altDP = Gpr.parseIntSafe(depthField[1]);
      int depth=refDP+altDP;

      if(filter.isEmpty() || vcfEntry.getFilterPass().indexOf(filter) != -1) {
        if(chrLRR.containsKey(vcfEntry.getChromosomeName())) {
          if(!depthExclusion.contains(vcfEntry.getChromosomeName())) {
          allDepth.add(depth);
          depthSum += (long)depth;
          }
          if(depth > minDepth) {
            chrLRR.get(vcfEntry.getChromosomeName())[vcfEntry.getStart()/window].add(depth);
            chrBAF.get(vcfEntry.getChromosomeName())[vcfEntry.getStart()/window].add((float)altDP/(float)depth);
          }
        }
      }
    }
    vcfParser.close();
    
    allDepth.sort();
    double median = allDepth.get(allDepth.size()/2);
    double average = (double)(depthSum / (double)allDepth.size());
    System.err.println("Genome median depth : " + median);
    System.err.println("Genome average depth: " + average);
    allDepth = null;
    
    DepthRatioBAlleleFreqPlot depthRatioBAFPlot = new DepthRatioBAlleleFreqPlot(genome, width, height, generateSVG);
    for(String chrName : chrLRR.keySet()) {
      for(int bin=0; bin < chrLRR.get(chrName).length; bin++) {
        int position = bin * window;
        int midPosition = position + window/2;
        for(int i=0; i< chrLRR.get(chrName)[bin].size(); i++) {
          depthRatioBAFPlot.addPoint(DepthRatioBAF.DEPTH_RATIO, chrName, midPosition, Math.log(chrLRR.get(chrName)[bin].get(i)/median)/Math.log(2));
          System.out.println(chrName + ',' + midPosition + ',' + Math.log(chrLRR.get(chrName)[bin].get(i)/median)/Math.log(2) + ','+chrLRR.get(chrName)[bin].get(i)+','+median);
          depthRatioBAFPlot.addPoint(DepthRatioBAF.BAF, chrName, midPosition, chrBAF.get(chrName)[bin].get(i));
        }
      }
    }
    depthRatioBAFPlot.write(outputPrefix + ".lrrbaf");
  }
}
