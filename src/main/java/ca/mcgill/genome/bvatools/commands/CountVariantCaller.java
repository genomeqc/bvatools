/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Deque;

import ca.mcgill.genome.bvatools.DefaultTool;
import ca.mcgill.genome.bvatools.pileup.BamPileup;
import ca.mcgill.genome.bvatools.snpcall.VariantCallPileupHandler;

import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.SAMFileHeader.SortOrder;
import htsjdk.samtools.SAMReadGroupRecord;
import htsjdk.samtools.SAMSequenceDictionary;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;
import htsjdk.samtools.reference.IndexedFastaSequenceFile;
import htsjdk.variant.variantcontext.writer.Options;
import htsjdk.variant.variantcontext.writer.VariantContextWriter;
import htsjdk.variant.variantcontext.writer.VariantContextWriterBuilder;
import htsjdk.variant.variantcontext.writer.VariantContextWriterBuilder.OutputType;

public class CountVariantCaller extends DefaultTool {
  private int minDepth = 10;
  private int minMappingQuality = 15;
  private int minBaseQuality = 15;
  private float hetThreshold = 0.35f;
  private float emitAt = 0.01f;
  private IndexedFastaSequenceFile refSequence = null;
  private File bam = null;
  private File vcf = null;
  private String regionChr = null;
  private int regionStart = -1;
  private int regionEnd = -1;

  @Override
  public String getCmdName() {
    return "countcall";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("\t--bam                  Input bam");
    System.out.println("\t--minDepth             Minimum depth on which to use a Variant. (default: " + minDepth + ")");
    System.out.println("\t--minMappingQuality    Only use reads with a mapping quality higher than this value (default: " + minMappingQuality + ")");
    System.out.println("\t--minBaseQuality       Only count bases with a base quality higher than this value. (default: " + minBaseQuality + ")");
    System.out.println("\t--ref                  Indexed reference genome");
    System.out.println("\t--het                  At which threshold do we call a valid het. (default: " + hetThreshold + ")");
    System.out.println("\t--emit-threshold       At which TAF (Top Allele Fraction) do we write a call. (default: " + emitAt + ")");
    System.out.println("\t--vcf                  Output vcf");
    System.out.println("\t--region               Region to call in. chr:start-end. start 0-based, end 1-based (BED format)");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {
    String regionStr = null;
    while (!arguments.isEmpty()) {
      String arg = arguments.pop();
      switch (arg) {
      case "--bam":
        bam = new File(arguments.pop());
        break;
      case "--vcf":
        vcf = new File(arguments.pop());
        break;
      case "--emit-threshold":
        emitAt = Float.parseFloat(arguments.pop());
        break;
      case "--het":
        hetThreshold = Float.parseFloat(arguments.pop());
        break;
      case "--ref":
        try {
          refSequence = new IndexedFastaSequenceFile(new File(arguments.pop()));
        } catch (FileNotFoundException e) {
          throw new RuntimeException(e);
        }
        break;
      case "--minMappingQuality":
        minMappingQuality = Integer.parseInt(arguments.pop());
        break;
      case "--minBaseQuality":
        minBaseQuality = Integer.parseInt(arguments.pop());
        break;
      case "--minDepth":
        minDepth = Integer.parseInt(arguments.pop());
        break;
      case "--region":
        regionStr = arguments.pop();
        break;
      default:
        unusedArgs.add(arg);
        break;
      }
    }

    if (bam == null) {
      printUsage("You need to pass --bam");
      return 1;
    }

    if (vcf == null) {
      printUsage("vcf not set");
      return 1;
    }

    if (regionStr != null) {
      String[] values = regionStr.split(":");
      if (values.length != 2) {
        printUsage("bad region format");
        return 1;
      }
      regionChr = values[0];
      values = values[1].split("-");
      regionStart = Integer.parseInt(values[0]);
      // 0 to 1 based;
      regionStart++;
      regionEnd = Integer.parseInt(values[1]);

      if (regionEnd < regionStart) {
        printUsage("in region end is before start");
        return 1;
      }
    }

    return 0;
  }

  @Override
  public int run() {

    try {
      callSnps();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    return 0;
  }

  public void callSnps() throws IOException {
    SAMSequenceDictionary dict;
    String sampleName = null;
    SamReaderFactory samReaderFactory = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
    try (SamReader samReader = samReaderFactory.open(bam)) {
      SAMFileHeader header = samReader.getFileHeader();
      if (header.getSortOrder() != SortOrder.coordinate) {
        throw new RuntimeException("BAM has to be coordinate sorted");
      }

      dict = header.getSequenceDictionary();
      for (SAMReadGroupRecord rg : header.getReadGroups()) {
        if (sampleName == null) {
          sampleName = rg.getSample();
        } else if (!sampleName.equals(rg.getSample())) {
          throw new RuntimeException("We only support one sample per BAM file");
        }
      }
      if (sampleName == null) {
        sampleName = bam.getName();
      }
    }
    VariantContextWriterBuilder ctxBuilder = new VariantContextWriterBuilder();
    ctxBuilder.setOutputFile(vcf);
    ctxBuilder.setOutputFileType(OutputType.VCF);
    ctxBuilder.setReferenceDictionary(dict);
    ctxBuilder.setOption(Options.INDEX_ON_THE_FLY);

    VariantContextWriter writer = ctxBuilder.build();

    VariantCallPileupHandler pileupHandler = new VariantCallPileupHandler(sampleName, writer, refSequence, minMappingQuality, minBaseQuality, hetThreshold,
        emitAt);
    BamPileup bamParser = new BamPileup(bam, regionChr, regionStart, regionEnd);
    bamParser.readBam(pileupHandler);
    writer.close();
  }
}
