/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import java.io.File;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ca.mcgill.genome.bvatools.DefaultTool;
import ca.mcgill.mcb.pcingola.fileIterator.VcfFileIterator;
import ca.mcgill.mcb.pcingola.util.Gpr;
import ca.mcgill.mcb.pcingola.vcf.VcfEntry;
import ca.mcgill.mcb.pcingola.vcf.VcfGenotype;
import ca.mcgill.mcb.pcingola.vcf.VcfHeader;

public class CompareSamples extends DefaultTool {
  private int minDepth = 10;
  private int minGQ = 0;
  private double minQual = 0;
  private boolean testRef = false;
  private boolean onlyHom = false;
  private boolean outputBadCalls = false;
  private String[] sampleNames = null;
  private boolean firstvcfref = false;
  private boolean useAllVariants = false;
  private boolean excludeFiltered = false;
  private List<File> vcfs = new ArrayList<>();

  @Override
  public String getCmdName() {
    return "cmpsamples";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("\t--vcf             VCFs to load");
    System.out.println("\t--sampleNames     Comma separated list of per vcf sample names to compare. (default: intersect all samples from all vcfs).");
    System.out.println(
        "\t                  If you submit 3 vcfs and want to compare sample RENAL from the first, KIDNEY from the second and BRAIN from the third input this:");
    System.out.println("\t                  RENAL,KIDNEY,BRAIN (no spaces)");
    System.out.println("\t--minDepth        Minimum depth on which to use a Variant. (default: " + minDepth + ")");
    System.out.println("\t--minQual         Minimum Variant Quality (not genotype quality) on which to use a Variant. (default: " + minQual + ")");
    System.out.println("\t--minGQ           Minimum Genotype Quality (not variant quality) on which to use a Variant. (default: " + minGQ + ")");
    System.out.println("\t--testref         Test the REF at the same position between VCFs. (default: off)");
    System.out.println("\t--onlyhom         Use only Homozygous calls. (default: off)");
    System.out.println("\t--firstvcfref     The first vcf is the reference to compare to.");
    System.out.println("\t                  Test all of the variants from this one. usefull to compare with Snp Arrays");
    System.out.println("\t--outputBadCalls  Output the calls that fail. (default: off)");
    System.out.println("\t--useAllVariants  Don't just use ALT variants, use HOM Ref too. (default: off)");
    System.out.println("\t--excludeFiltered Don't count as a test a variant filtered in any sample. (default: off)");
    System.out.println("\t                  Usefull when comparing SnpArray with NGS. If you wan't good het calls you wan't a Depth >= 15.");
    System.out.println("\t                  If you don't have it it's not a failed test, it just can't be used for the comparison.");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {
    while (!arguments.isEmpty()) {
      String arg = arguments.pop();
      switch (arg) {
      case "--vcf":
        vcfs.add(new File(arguments.pop()));
        break;
      case "--minDepth":
        minDepth = Integer.parseInt(arguments.pop());
        break;
      case "--sampleNames":
        sampleNames = Gpr.split(arguments.pop(), ',');
        break;
      case "--firstvcfref":
        firstvcfref = true;
        break;
      case "--testref":
        testRef = true;
        break;
      case "--onlyhom":
        onlyHom = true;
        break;
      case "--outputBadCalls":
        outputBadCalls = true;
        break;
      case "--useAllVariants":
        useAllVariants = true;
        break;
      case "--excludeFiltered":
        excludeFiltered = true;
        break;
      case "--minQual":
        minQual = Integer.parseInt(arguments.pop());
        break;
      case "--minGQ":
        minGQ = Integer.parseInt(arguments.pop());
        break;
      default:
        unusedArgs.add(arg);
        break;
      }
    }

    if (vcfs.size() == 0) {
      printUsage("vcf not set");
      return 1;
    }
    if (sampleNames != null && sampleNames.length != vcfs.size()) {
      printUsage("If sampleNames is used it must have one value per vcf");
      return 1;
    }

    return 0;
  }

  @Override
  public int run() {
    compareSnps();

    return 0;
  }

  public void compareSnps() {
    Map<String, Map<String, String>> loc2genotype = new HashMap<>();
    Map<String, String> pos2ref = new HashMap<>();
    Set<String> filteredForASample = new HashSet<>();
    int totalSamples = 0;
    for (int vcfIdx = 0; vcfIdx < vcfs.size(); vcfIdx++) {
      File vcf = vcfs.get(vcfIdx);
      System.err.println("Parsing: " + vcf);
      VcfFileIterator vcfParser = new VcfFileIterator(vcf.toString());
      VcfHeader vcfHeader = vcfParser.readHeader();
      List<String> allSampleNames = vcfHeader.getSampleNames();
      List<Integer> indexesToUse = new ArrayList<>();
      if (sampleNames != null) {
        boolean found = false;
        String sampleName = sampleNames[vcfIdx];
        for (int idx = 0; idx < allSampleNames.size(); idx++) {
          if (sampleName.equals(allSampleNames.get(idx))) {
            indexesToUse.add(idx);
            found = true;
            break;
          }
        }
        if (!found) {
          throw new RuntimeException("Can't find sample: " + sampleName + " in vcf: " + vcf);
        }
      } else {
        for (int idx = 0; idx < allSampleNames.size(); idx++) {
          indexesToUse.add(idx);
        }
      }

      totalSamples += indexesToUse.size();

      for (VcfEntry vcfEntry : vcfParser) {
        // Skip indels for now.
        if (vcfEntry.isInDel() || vcfEntry.getQuality() < minQual) {
          continue;
        }

        String key = vcfEntry.getChromosomeName() + ':' + vcfEntry.getStart();

        if (testRef) {
          if (!pos2ref.containsKey(key)) {
            pos2ref.put(key, vcfEntry.getRef());
          } else {
            if (!vcfEntry.getRef().equals(pos2ref.get(key))) {
              System.err.println("Refs don't match: Original:" + pos2ref.get(key) + " New:" + vcfEntry.getRef() + " NewFile: " + vcf);
            }
          }
        }

        for (Integer idx : indexesToUse) {
          VcfGenotype sampleGenotype;
          try {
            sampleGenotype = vcfEntry.getVcfGenotype(idx);
          } catch (RuntimeException e) {
            System.err.println("Problem at: " + vcf + " Pos: " + key);
            throw new RuntimeException(e);
          }

          int depth = Gpr.parseIntSafe(sampleGenotype.get("DP"));
          if (depth >= minDepth && Gpr.parseIntSafe(sampleGenotype.get("GQ")) >= minGQ) {
            if (!onlyHom || sampleGenotype.isHomozygous()) {
              if (!loc2genotype.containsKey(key)) {
                if (!firstvcfref || vcfIdx == 0) {
                  if (useAllVariants || sampleGenotype.isVariant()) {
                    loc2genotype.put(key, new HashMap<>());
                  }
                }
              }

              Map<String, String> vcf2GT = loc2genotype.get(key);
              if (vcf2GT != null) {
                vcf2GT.put(vcf.toString() + '-' + allSampleNames.get(idx), sampleGenotype.getGenotypeStr());
              }
            }
          } else {
            if (excludeFiltered) {
              // Remove it since one of the sample has the variant, but doesn't have enough depth or quality to be sure of the outcome.
              filteredForASample.add(key);
            }
          }
        }
      }
    } // for vcfs

    int nbTests = 0;
    int nbMatch = 0;
    for (String key : loc2genotype.keySet()) {
      if (filteredForASample.contains(key)) {
        continue;
      }

      Map<String, String> smplGenotype = loc2genotype.get(key);
      if (smplGenotype.size() == totalSamples) {
        nbTests++;
        boolean sameGenotype = true;

        String genotype = null;
        String sampleName = null;
        for (String sample : smplGenotype.keySet()) {
          if (genotype == null) {
            genotype = smplGenotype.get(sample);
            sampleName = sample.substring(0, sample.indexOf('-'));
          } else {
            if (!genotype.equals(smplGenotype.get(sample))) {
              sameGenotype = false;
              if (this.outputBadCalls) {
                String testSampleName = sample.substring(0, sample.indexOf('-'));
                System.err
                    .println("Bad    : " + key + " Samples: " + sampleName + ',' + testSampleName + " Genotypes:" + genotype + "," + smplGenotype.get(sample));
              }
              break;
            }
          }
        } // for samples

        if (sameGenotype) {
          nbMatch++;
        }
      } else if (firstvcfref) {
        nbTests++;
        if (this.outputBadCalls) {
          System.err.println("Missing: " + key);
        }

      }
    }

    int nbSnps = loc2genotype.size();
    System.out.println("Nb SNPS : " + nbSnps);
    System.out.println("Nb Tests: " + nbTests);
    System.out.println("Nb Match: " + nbMatch);
    System.out.println("% Match : " + nbMatch * 100.0 / nbTests);
  }
}
