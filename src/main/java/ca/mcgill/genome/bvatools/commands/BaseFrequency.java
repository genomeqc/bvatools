/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ca.mcgill.genome.bvatools.DefaultTool;
import ca.mcgill.genome.bvatools.homology.AlleleFrequencyComputer;
import ca.mcgill.genome.bvatools.parsers.CachedReferenceSequenceFile;
import ca.mcgill.genome.bvatools.util.AlleleCounts;
import ca.mcgill.genome.bvatools.util.MemoryUtils;
import ca.mcgill.genome.bvatools.util.Position;
import ca.mcgill.genome.bvatools.util.SampleAlleleCounts.ReadBases;

import gnu.trove.impl.Constants;
import gnu.trove.map.hash.TObjectIntHashMap;
import htsjdk.samtools.AlignmentBlock;
import htsjdk.samtools.Defaults;
import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.SAMReadGroupRecord;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMSequenceDictionary;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;
import htsjdk.samtools.reference.ReferenceSequenceFile;
import htsjdk.samtools.util.CoordMath;

public class BaseFrequency extends DefaultTool {
  private byte threads = 1;
  private int minMappingQuality = 10;
  private int minBaseQuality = 10;
  private boolean perRG = false;
  private boolean useDups = false;
  private File refFasta = null;
  private File bamFile = null;
  private File positions = null;
  private boolean useIndex = false;
  private String output = null;

  @Override
  public String getCmdName() {
    return "basefreq";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("\t--pos                  Input position in TSV. chr<TAB>1-basedPosition");
    System.out.println("\t--bam                  BAM file");
    System.out.println("\t--out                  Output file, or output prefix in RG mode");
    System.out.println("\t--perRG                Output counts per Read Group. (default: " + perRG + ")");
    System.out.println("\t                       When used with 'useIndex' it can be very memory consuming.");
    System.out.println("\t--threads              Threads to use. Only used in indexed mode. (default: " + threads + ")");
    System.out.println("\t--printElapsed         Prints elapsed percentage");
    System.out.println("\t--useIndex             Use the index instead of going over the whole BAM. Only efficient when you have a < ~1000 sites");
    System.out.println("\t--ref                  Optional. Reference genome this will add the reference base at the given positions.");
    System.out.println("\t--minMappingQuality    Only use reads with a mapping quality higher than this value (default: " + minMappingQuality + ")");
    System.out.println("\t--minBaseQuality       Only count bases with a base quality higher than this value. (default: " + minBaseQuality + ")");
    System.out.println("\t--countDups            Don't ignore duplicates. (default: false)");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {

    while (!arguments.isEmpty()) {
      String arg = arguments.pop();
      switch (arg) {
      case "--pos":
        positions = new File(arguments.pop());
        break;
      case "--threads":
        threads = Byte.parseByte(arguments.pop());
        break;
      case "--perRG":
        perRG = true;
        break;
      case "--out":
        output = arguments.pop();
        break;
      case "--bam":
        bamFile = new File(arguments.pop());
        break;
      case "--useIndex":
        useIndex = true;
        break;
      case "--ref":
        refFasta = new File(arguments.pop());
        break;
      case "--countDups":
        useDups = true;
        break;
      case "--minMappingQuality":
        minMappingQuality = Integer.parseInt(arguments.pop());
        break;
      case "--minBaseQuality":
        minBaseQuality = Integer.parseInt(arguments.pop());
        break;
      default:
        unusedArgs.add(arg);
        break;
      }
    }

    if (bamFile == null) {
      printUsage("bam not set");
      return 1;
    }

    if (positions == null) {
      printUsage("pos not set");
      return 1;
    }

    if (output == null) {
      printUsage("out not set");
      return 1;
    }

    return 0;
  }

  @Override
  public int run() {

    SamReaderFactory samReaderFactory = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);

    try (SamReader samReader = samReaderFactory.open(bamFile)) {
      SAMFileHeader header = samReader.getFileHeader();
      if (!header.getSortOrder().equals(SAMFileHeader.SortOrder.coordinate)) {
        throw new RuntimeException("BAM must be coordinate sorted");
      }
      final SAMSequenceDictionary dictionary = header.getSequenceDictionary();
      List<Position> positionsToTest = parsePositions(dictionary, positions);
      if (useIndex) {
        computeFreq(output, positionsToTest, bamFile, header);
      } else {
        computeFreqWithoutIndex(output, positionsToTest, bamFile);
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    return 0;
  }

  public List<Position> parsePositions(final SAMSequenceDictionary dictionary, File positionsFile) {
    Set<Position> positions = new HashSet<>();

    ReferenceSequenceFile refSequences = null;
    try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(positionsFile), StandardCharsets.US_ASCII), 100 * 1024)) {
      if (refFasta != null) {
        refSequences = new CachedReferenceSequenceFile(refFasta);
      }

      String line;
      while ((line = reader.readLine()) != null) {
        if (line.length() > 0 && line.charAt(0) == '#') {
          continue;
        }
        String[] nextLine = line.split("\t");
        String chromosome = nextLine[0];
        int position = Integer.parseInt(nextLine[1]);
        if (refSequences == null) {
          positions.add(new Position(chromosome, position, (byte) 0));
        } else {
          byte ref = refSequences.getSubsequenceAt(chromosome, position, position).getBases()[0];
          positions.add(new Position(chromosome, position, ref));
        }
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    } finally {
      RuntimeException ex = null;

      if (refSequences != null) {
        try {
          refSequences.close();
        } catch (IOException e) {
          ex = new RuntimeException(e);
        }
      }

      if (ex != null) {
        throw ex;
      }
    }

    List<Position> orderedPositions = new ArrayList<>(positions);
    orderPositions(dictionary, orderedPositions);
    return orderedPositions;
  }

  public void computeFreqWithoutIndex(String output, List<Position> positionsToTest, File bamFile) throws IOException {
    SamReaderFactory samReaderFactory = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
    SamReader samReader = samReaderFactory.open(bamFile);
    SAMFileHeader header = samReader.getFileHeader();
    PrintStream allWriter = null;
    Map<String, PrintStream> rg2Writer = null;
    try {
      Map<String, AlleleCounts> allCounts = new HashMap<>();
      Map<String, Map<String, AlleleCounts>> rgCounts = null;
      if (perRG) {
        rgCounts = new HashMap<>();
        allWriter = new PrintStream(new BufferedOutputStream(new FileOutputStream(output + ".alleleFreq.csv"), Defaults.BUFFER_SIZE), false,
            StandardCharsets.US_ASCII);
        rg2Writer = new HashMap<>();
        for (SAMReadGroupRecord rg : header.getReadGroups()) {
          PrintStream tmp = new PrintStream(new BufferedOutputStream(new FileOutputStream(output + '.' + rg.getId() + ".alleleFreq.csv"), Defaults.BUFFER_SIZE),
              false, StandardCharsets.US_ASCII);
          tmp.println("Chr,Position,AlleleCounts,AlleleFrequency,RefBase");
          rg2Writer.put(rg.getId(), tmp);

          rgCounts.put(rg.getId(), new HashMap<>());
        }
      } else {
        allWriter = new PrintStream(new BufferedOutputStream(new FileOutputStream(output), Defaults.BUFFER_SIZE), false, StandardCharsets.US_ASCII);
      }
      allWriter.println("Chr,Position,AlleleCounts,AlleleFrequency,RefBase");

      browsePositions(samReader, positionsToTest, allCounts, rgCounts, allWriter, rg2Writer);
    } finally {
      samReader.close();
      if (allWriter != null) {
        allWriter.close();
      }
      if (rg2Writer != null) {
        for (PrintStream rgWriter : rg2Writer.values()) {
          rgWriter.close();
        }
      }
    }
  }

  private void browsePositions(SamReader samReader, List<Position> positionsToTest, Map<String, AlleleCounts> allCounts,
      Map<String, Map<String, AlleleCounts>> rgCounts, PrintStream allWriter, Map<String, PrintStream> rg2Writer) {
    SAMSequenceDictionary dictionary = samReader.getFileHeader().getSequenceDictionary();

    int currentIdx = 0;
    for (SAMRecord record : samReader) {
      if (record.getReadUnmappedFlag() || record.getMappingQuality() < minMappingQuality) {
        continue;
      }
      if (!useDups && record.getDuplicateReadFlag()) {
        continue;
      }

      int start = record.getAlignmentStart();
      int end = record.getAlignmentEnd();

      boolean skipRecord = false;
      while (currentIdx < positionsToTest.size()) {
        if (!record.getReferenceName().equals(positionsToTest.get(currentIdx).getChromosome())) {
          if (record.getReferenceIndex() < dictionary.getSequenceIndex(positionsToTest.get(currentIdx).getChromosome())) {
            skipRecord = true;
            break;
          }
        } else if (positionsToTest.get(currentIdx).getPosition() >= start) {
          break;
        }
        Position position = positionsToTest.get(currentIdx);
        String key = position.getChromosome() + '#' + position.getPosition();
        initEntryCount(key, position, allCounts, rgCounts);
        printCount(allWriter, allCounts.get(key));
        allCounts.remove(key);
        if (perRG) {
          for (String rgId : rgCounts.keySet()) {
            printCount(rg2Writer.get(rgId), rgCounts.get(rgId).get(key));
            rgCounts.get(rgId).remove(key);
          }
        }

        currentIdx++;
      }
      if (currentIdx >= positionsToTest.size()) {
        break;
      }
      if (skipRecord) {
        continue;
      }

      if (positionsToTest.get(currentIdx).getPosition() >= start && positionsToTest.get(currentIdx).getPosition() <= end) {
        for (int tmpIdx = currentIdx;
             tmpIdx < positionsToTest.size() && record.getReferenceName().equals(positionsToTest.get(tmpIdx).getChromosome()); tmpIdx++) {
          Position position = positionsToTest.get(tmpIdx);

          if (position.getPosition() <= end) {
            for (AlignmentBlock block : record.getAlignmentBlocks()) {
              if (block.getReferenceStart() <= position.getPosition() && position.getPosition() <= CoordMath
                  .getEnd(block.getReferenceStart(), block.getLength())) {
                int refOffset = position.getPosition() - block.getReferenceStart();
                int readBaseOffset = block.getReadStart() - 1 + refOffset;
                if (record.getBaseQualities()[readBaseOffset] >= minBaseQuality) {
                  byte[] bases = record.getReadBases();
                  String baseStr = String.valueOf((char) bases[readBaseOffset]);

                  String key = position.getChromosome() + '#' + position.getPosition();
                  // Init missing entry
                  initEntryCount(key, position, allCounts, rgCounts);

                  allCounts.get(key).getBamCounts().increment(baseStr);
                  if (perRG) {
                    Map<String, AlleleCounts> rgCount = rgCounts.get(record.getReadGroup().getId());
                    rgCount.get(key).getBamCounts().increment(baseStr);
                  }
                }
                break;
              }
            }
          } else {
            break;
          }
        }
      }
    }

    // Finish the remaining positions 
    for (; currentIdx < positionsToTest.size(); currentIdx++) {
      Position position = positionsToTest.get(currentIdx);
      String key = position.getChromosome() + '#' + position.getPosition();
      initEntryCount(key, position, allCounts, rgCounts);
      printCount(allWriter, allCounts.get(key));
      allCounts.remove(key);
      if (perRG) {
        for (String rgId : rgCounts.keySet()) {
          printCount(rg2Writer.get(rgId), rgCounts.get(rgId).get(key));
          rgCounts.get(rgId).remove(key);
        }
      }
    }
  }

  private void printCount(PrintStream writer, AlleleCounts alleleCounts) {
    writer.print(alleleCounts.getChromosome());
    writer.print(',');
    writer.print(alleleCounts.getPosition());
    writer.print(',');
    writer.print(alleleCounts.format(false));
    writer.print(',');
    writer.print(AlleleCounts.formatFractionCounts(AlleleCounts.computeAlleleFractions(alleleCounts.getBamCounts())));
    writer.print(',');
    if (alleleCounts.getAlleleA() != null && !alleleCounts.getAlleleA().isEmpty()) {
      writer.println(alleleCounts.getAlleleA());
    } else {
      writer.println();
    }
  }

  private void initEntryCount(String key, Position position, Map<String, AlleleCounts> allCounts, Map<String, Map<String, AlleleCounts>> rgCounts) {
    if (!allCounts.containsKey(key)) {
      AlleleCounts alleleCounts = new AlleleCounts(position.getChromosome(), position.getPosition());
      if (position.getReferenceBase() != 0) {
        alleleCounts.setAlleleA(String.valueOf((char) position.getReferenceBase()));
      }

      alleleCounts.setBamCounts(new TObjectIntHashMap<>(Constants.DEFAULT_CAPACITY, Constants.DEFAULT_LOAD_FACTOR, 0));
      for (ReadBases readBases : ReadBases.values()) {
        alleleCounts.getBamCounts().put(readBases.toString(), 0);
      }
      allCounts.put(key, alleleCounts);

      if (perRG) {
        for (Map<String, AlleleCounts> rgCount : rgCounts.values()) {
          alleleCounts = new AlleleCounts(position.getChromosome(), position.getPosition());
          if (position.getReferenceBase() != 0) {
            alleleCounts.setAlleleA(String.valueOf((char) position.getReferenceBase()));
          }

          alleleCounts.setBamCounts(new TObjectIntHashMap<>(Constants.DEFAULT_CAPACITY, Constants.DEFAULT_LOAD_FACTOR, 0));
          for (ReadBases readBases : ReadBases.values()) {
            alleleCounts.getBamCounts().put(readBases.toString(), 0);
          }
          rgCount.put(key, alleleCounts);
        }
      }
    }
  }

  private void orderPositions(final SAMSequenceDictionary dictionary, List<Position> positionsToTest) {
    positionsToTest.sort((o1, o2) -> {
      int idx1 = dictionary.getSequenceIndex(o1.getChromosome());
      if (idx1 == -1) {
        System.err.println("Couldn't find: " + o1.getChromosome() + " in bam");
      }
      int idx2 = dictionary.getSequenceIndex(o2.getChromosome());
      if (idx2 == -1) {
        System.err.println("Couldn't find: " + o2.getChromosome() + " in bam");
      }
      int delta = idx1 - idx2;
      if (delta != 0) {
        return delta;
      }

      return o1.getPosition() - o2.getPosition();
    });
  }

  public void computeFreq(String output, List<Position> positionsToPileup, File bamFile, SAMFileHeader header) throws IOException {
    Map<String, AlleleCounts> allCounts = new HashMap<>();
    Map<String, Map<String, AlleleCounts>> rgCounts = null;
    if (perRG) {
      rgCounts = new HashMap<>();
      for (SAMReadGroupRecord rg : header.getReadGroups()) {
        rgCounts.put(rg.getId(), new HashMap<>());
      }
    }

    for (Position position : positionsToPileup) {
      String key = position.getChromosome() + '#' + position.getPosition();
      initEntryCount(key, position, allCounts, rgCounts);
    }

    if (positionsToPileup.size() < threads) {
      threads = (byte) positionsToPileup.size();
    }
    int parts = positionsToPileup.size() / threads;
    AlleleFrequencyComputer[] alleleFrequencyComputers = new AlleleFrequencyComputer[threads];
    int idx = 0;
    for (; idx < threads - 1; idx++) {
      int offset = idx * parts;
      List<Position> newIntervals = positionsToPileup.subList(offset, offset + parts);
      alleleFrequencyComputers[idx] = new AlleleFrequencyComputer(bamFile, newIntervals, perRG, useDups, allCounts, rgCounts, minMappingQuality,
          minBaseQuality);
    }
    int offset = idx * parts;
    List<Position> newIntervals = positionsToPileup.subList(offset, positionsToPileup.size());
    alleleFrequencyComputers[idx] = new AlleleFrequencyComputer(bamFile, newIntervals, perRG, useDups, allCounts, rgCounts, minMappingQuality, minBaseQuality);

    Thread[] workers = new Thread[threads];
    ThreadGroup computerGrp = new ThreadGroup("AlleleFrequencyComputers");
    for (idx = 0; idx < threads; idx++) {
      workers[idx] = new Thread(computerGrp, alleleFrequencyComputers[idx]);
      workers[idx].setUncaughtExceptionHandler(alleleFrequencyComputers[idx]);
      workers[idx].setDaemon(true);
      workers[idx].start();
    }

    for (Thread worker : workers) {
      try {
        worker.join();
        for (AlleleFrequencyComputer alleleFrequencyComputer : alleleFrequencyComputers) {
          if (alleleFrequencyComputer.getCaughtException() != null) {
            throw new RuntimeException(alleleFrequencyComputer.getCaughtException());
          }
        }
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }
    }

    PrintStream allWriter = null;
    Map<String, PrintStream> rg2Writer = null;
    try {
      if (perRG) {
        allWriter = new PrintStream(new BufferedOutputStream(new FileOutputStream(output + ".alleleFreq.csv"), Defaults.BUFFER_SIZE), false,
            StandardCharsets.US_ASCII);
        rg2Writer = new HashMap<>();
        for (String rgId : rgCounts.keySet()) {
          PrintStream tmp = new PrintStream(new BufferedOutputStream(new FileOutputStream(output + '.' + rgId + ".alleleFreq.csv"), Defaults.BUFFER_SIZE),
              false, StandardCharsets.US_ASCII);
          tmp.println("Chr,Position,AlleleCounts,AlleleFrequency,RefBase");
          rg2Writer.put(rgId, tmp);
        }
      } else {
        allWriter = new PrintStream(new BufferedOutputStream(new FileOutputStream(output), Defaults.BUFFER_SIZE), false, StandardCharsets.US_ASCII);
      }
      allWriter.println("Chr,Position,AlleleCounts,AlleleFrequency,RefBase");

      // Finish the remaining positions 
      for (Position position : positionsToPileup) {
        String key = position.getChromosome() + '#' + position.getPosition();
        printCount(allWriter, allCounts.get(key));
        if (perRG) {
          for (String rgId : rgCounts.keySet()) {
            printCount(rg2Writer.get(rgId), rgCounts.get(rgId).get(key));
          }
        }
      }
      System.out.print("MemoryUsed: ");
      System.out.println(MemoryUtils.printGC());
    } finally {
      if (allWriter != null) {
        allWriter.close();
      }
      if (rg2Writer != null) {
        for (PrintStream rgWriter : rg2Writer.values()) {
          rgWriter.close();
        }
      }
    }
  }

}
