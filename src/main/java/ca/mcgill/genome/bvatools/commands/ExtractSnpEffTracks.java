/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import java.io.File;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ca.mcgill.genome.bvatools.DefaultTool;
import ca.mcgill.genome.bvatools.parsers.GeneMapper;
import ca.mcgill.genome.bvatools.util.NaturalOrderComparator;
import ca.mcgill.mcb.pcingola.interval.Chromosome;
import ca.mcgill.mcb.pcingola.interval.Exon;
import ca.mcgill.mcb.pcingola.interval.Gene;
import ca.mcgill.mcb.pcingola.interval.Genes;
import ca.mcgill.mcb.pcingola.interval.Genome;
import ca.mcgill.mcb.pcingola.interval.Intergenic;
import ca.mcgill.mcb.pcingola.interval.Intron;
import ca.mcgill.mcb.pcingola.interval.Marker;
import ca.mcgill.mcb.pcingola.interval.Markers;
import ca.mcgill.mcb.pcingola.interval.Transcript;
import ca.mcgill.mcb.pcingola.snpEffect.Config;
import ca.mcgill.mcb.pcingola.snpEffect.EffectType;
import ca.mcgill.mcb.pcingola.snpEffect.SnpEffectPredictor;

import gnu.trove.map.hash.TObjectLongHashMap;

public class ExtractSnpEffTracks extends DefaultTool {
  private SnpEffectPredictor predictor;
  private Set<String> toExclude = new HashSet<>();
  private File config = null;
  private String snpEffGenome = null;
  private GeneMapper mapper = null;

  @Override
  public String getCmdName() {
    return "extractSnpEffTracks";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("\t -c <FILE> <ID>   SNPEff config file");
    System.out.println("\t--exclude         Chromosomes to exclude. Coma seperated");
    System.out.println("\t--mapper          Genelist to use (filter by refseq)");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {
    while (!arguments.isEmpty()) {
      String arg = arguments.pop();
      switch (arg) {
      case "-c":
        config = new File(arguments.pop());
        snpEffGenome = arguments.pop();
        break;
      case "--exclude":
        String extraArgs = arguments.pop();
        Collections.addAll(toExclude, extraArgs.split(","));
        break;
      case "--mapper":
        mapper = new GeneMapper(new File(arguments.pop()));
        break;
      default:
        unusedArgs.add(arg);
        break;
      }
    }

    if (config == null || !config.exists() || snpEffGenome == null) {
      printUsage("Missing snp eff config file");
      return 1;
    }

    return 0;
  }

  @Override
  public int run() {
    Config snpEffConfig = new Config(snpEffGenome, config.toString());
    predictor = snpEffConfig.loadSnpEffectPredictor();
    Genome genome = predictor.getGenome();

    Genes genes = genome.getGenes();
    for (Gene gene : genes) {
      Iterator<Transcript> trIter = gene.iterator();
      while (trIter.hasNext()) {
        Transcript tr = trIter.next();
        if (!tr.isProteinCoding()) {
          trIter.remove();
        } else if (mapper != null) {
          if (mapper.getTranscriptId(tr.getId()) == null) {
            trIter.remove();
          } else if (!tr.getBioType().equalsIgnoreCase("protein_coding") && !tr.getBioType().equalsIgnoreCase("mRNA")) {
            trIter.remove();
          }
        }
      }
    }

    Iterator<Gene> geneIter = genes.iterator();
    Map<String, Integer> ends = new HashMap<>();
    while (geneIter.hasNext()) {
      Gene gene = geneIter.next();
      if (gene.numChilds() == 0) {
        geneIter.remove();
      } else {
        List<Transcript> trs = gene.sorted();
        if (trs.get(0).getStart() > gene.getStart()) {
          gene.setStart(trs.get(0).getStart());
        }
        if (trs.get(trs.size() - 1).getEnd() < gene.getEnd()) {
          gene.setEnd(trs.get(trs.size() - 1).getEnd());
        }

        if (!ends.containsKey(gene.getChromosomeName())) {
          ends.put(gene.getChromosomeName(), gene.getEnd());
        }
        if (ends.get(gene.getChromosomeName()) < gene.getEnd()) {
          ends.put(gene.getChromosomeName(), gene.getEnd());
        }
      }
    }

    for (String chrName : ends.keySet()) {
      int endPos = ends.get(chrName);
      predictor.add(new Intergenic(genome.getChromosome(chrName), endPos, genome.getChromosome(chrName).getEnd(), false, "...", "..."));
    }
    predictor.buildForest();

    TObjectLongHashMap<String> regionSizes = new TObjectLongHashMap<>();
    computeRegionSizes(regionSizes);

    return 0;
  }

  public Markers merge(Markers intsSorted) {
    intsSorted.getMarkers().sort((i1, i2) -> {
      // Compare chromosome
      NaturalOrderComparator natComp = new NaturalOrderComparator();
      int c = natComp.compare(i1.getChromosomeName(), i2.getChromosomeName());
      if (c != 0) {
        return c;
      }

      // Start
      if (i1.getStart() > i2.getStart()) {
        return 1;
      }
      if (i1.getStart() < i2.getStart()) {
        return -1;
      }

      // End
      return Integer.compare(i1.getEnd(), i2.getEnd());
    });

    // Merge intervals
    Markers intsMerged = new Markers();
    String tag = "", chromoName = "";
    Chromosome chromo = null;
    int start = -1, end = -1;
    for (Marker i : intsSorted) {

      // Different chromosome? => Re-start
      Chromosome ichromo = i.getChromosome();
      String ichromoName = ichromo.getId();
      if (!chromoName.equals(ichromoName)) {
        // Save current interval (if a any)
        if ((start >= 0) && (end >= 0)) {
          Marker im = new Marker(chromo, start, end, false, tag);
          intsMerged.add(im);
        }

        chromoName = ichromoName;
        chromo = ichromo;
        start = end = -1;
        tag = "";
      }

      // Previous interval finished? => add it to list
      if (i.getStart() > end) {
        if ((start >= 0) && (end >= 0)) {
          Marker im = new Marker(chromo, start, end, false, tag);
          intsMerged.add(im);
        }
        start = end = -1;
        tag = "";
      }

      // Update interval 'start'
      if (start < 0) {
        start = i.getStart();
      }

      // Update 'end'
      end = Math.max(end, i.getEnd());

      // Update tag
      if (tag.length() <= 0) {
        tag = i.getId();
      } else {
        tag += " " + i.getId();
      }
    }

    if ((start >= 0) && (end >= 0)) {
      Marker im = new Marker(chromo, start, end, false, tag);
      intsMerged.add(im);
    }

    return intsMerged;
  }

  public void computeRegionSizes(TObjectLongHashMap<String> regionSizes) {
    Markers utr5Prime = new Markers();
    Markers utr3Prime = new Markers();
    Markers cds = new Markers();
    Markers upstream = new Markers();
    Markers downstream = new Markers();
    Markers introns = new Markers();

    for (Gene gene : predictor.getGenome().getGenes()) {
      for (Transcript tr : gene) {
        int intronLength = 0;
        int exonLength = 0;

        utr5Prime.addAll(tr.get5primeUtrs());
        utr3Prime.addAll(tr.get3primeUtrs());
        cds.addAll(tr.getCds());
        for (Intron intron : tr.introns()) {
          introns.add(intron);
          intronLength += intron.size();
        }
        for (Exon exon : tr) {
          exonLength += exon.size();
        }
        int estIntron = tr.size() - exonLength;
        if (estIntron != intronLength) {
          System.err.println("Not a good intron estimator");
        }
        upstream.add(tr.getUpstream());
        downstream.add(tr.getDownstream());
      }
      if (gene.sizeof(EffectType.INTRAGENIC.toString()) > 0) {
        System.err.println("Gene has intragenic: " + gene.getGeneName());
      }
    }
    //utr5Prime.merge();
    Markers utr5PrimeMerged = merge(utr5Prime);
    Markers utr3PrimeMerged = merge(utr3Prime);
    Markers cdsMerged = merge(cds);
    Markers intronsMerged = merge(introns);
    Markers upstreamMerged = merge(upstream);
    Markers downstreamMerged = merge(downstream);

    int size;
    size = 0;
    System.out.println("track name=utr5");
    for (Marker marker : utr5PrimeMerged) {
      size += marker.size();
      System.out.println(marker.getChromosomeName() + "\t" + (marker.getStart() - 1) + "\t" + marker.getEnd() + "\tUTR5");
    }
    regionSizes.adjustOrPutValue(EffectType.UTR_5_PRIME.toString(), size, size);
    size = 0;
    System.out.println("track name=utr3");
    for (Marker marker : utr3PrimeMerged) {
      size += marker.size();
      System.out.println(marker.getChromosomeName() + "\t" + (marker.getStart() - 1) + "\t" + marker.getEnd() + "\tUTR3");
    }
    regionSizes.adjustOrPutValue(EffectType.UTR_3_PRIME.toString(), size, size);
    size = 0;
    System.out.println("track name=CDS");
    for (Marker marker : cdsMerged) {
      size += marker.size();
      System.out.println(marker.getChromosomeName() + "\t" + (marker.getStart() - 1) + "\t" + marker.getEnd() + "\tCDS");
    }
    regionSizes.adjustOrPutValue(EffectType.CDS.toString(), size, size);
    size = 0;
    System.out.println("track name=INTRON");
    for (Marker marker : intronsMerged) {
      size += marker.size();
      System.out.println(marker.getChromosomeName() + "\t" + (marker.getStart() - 1) + "\t" + marker.getEnd() + "\tINTRON");
    }
    regionSizes.adjustOrPutValue(EffectType.INTRON.toString(), size, size);
    size = 0;
    System.out.println("track name=UPSTREAM");
    for (Marker marker : upstreamMerged) {
      size += marker.size();
      System.out.println(marker.getChromosomeName() + "\t" + (marker.getStart() - 1) + "\t" + marker.getEnd() + "\tUPSTREAM");
    }
    regionSizes.adjustOrPutValue(EffectType.UPSTREAM.toString(), size, size);
    size = 0;
    System.out.println("track name=DOWNSTREAM");
    for (Marker marker : downstreamMerged) {
      size += marker.size();
      System.out.println(marker.getChromosomeName() + "\t" + (marker.getStart() - 1) + "\t" + marker.getEnd() + "\tDOWNSTREAM");
    }
    regionSizes.adjustOrPutValue(EffectType.DOWNSTREAM.toString(), size, size);
  }
}
