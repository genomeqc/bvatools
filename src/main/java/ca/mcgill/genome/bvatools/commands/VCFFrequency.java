/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;
import htsjdk.variant.variantcontext.Allele;
import htsjdk.variant.variantcontext.GenotypeBuilder;
import htsjdk.variant.variantcontext.GenotypesContext;
import htsjdk.variant.variantcontext.VariantContext;
import htsjdk.variant.variantcontext.VariantContextBuilder;
import htsjdk.variant.variantcontext.writer.Options;
import htsjdk.variant.variantcontext.writer.VariantContextWriter;
import htsjdk.variant.variantcontext.writer.VariantContextWriterBuilder;
import htsjdk.variant.vcf.VCFConstants;
import htsjdk.variant.vcf.VCFFileReader;
import htsjdk.variant.vcf.VCFFormatHeaderLine;
import htsjdk.variant.vcf.VCFHeader;
import htsjdk.variant.vcf.VCFHeaderLine;
import htsjdk.variant.vcf.VCFHeaderLineCount;
import htsjdk.variant.vcf.VCFHeaderLineType;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import ca.mcgill.genome.bvatools.DefaultTool;
import ca.mcgill.genome.bvatools.util.CountAtLoci;

public class VCFFrequency extends DefaultTool {
  public static final String ALLELE_DEPTH_KEY = "BVAD";
  private byte threads = 1;
  private String adKey = ALLELE_DEPTH_KEY;
  private int minMappingQuality = 10;
  private int minBaseQuality = 10;
  private boolean useDups = false;
  private String sampleName = null;
  private File bamFile = null;
  private File vcf = null;
  private File output = null;

  @Override
  public String getCmdName() {
    return "vcffreq";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("\t--vcf                  Input VCF");
    System.out.println("\t--bam                  BAM file");
    System.out.println("\t--adKey                Key name to use. Can be switched to AD to resemble Haplotype Caller or MuTect. default("+adKey+")");
    System.out.println("\t--out                  Output file");
    System.out.println("\t--threads              Threads to use. Will index the VCF is > 1. (default: " + threads + ")");
    System.out.println("\t--minMappingQuality    Only use reads with a mapping quality higher than this value (default: " + minMappingQuality + ")");
    System.out.println("\t--minBaseQuality       Only count bases with a base quality higher than this value. (default: " + minBaseQuality + ")");
    System.out.println("\t--countDups            Don't ignore duplicates. (default: false)");
    System.out.println("\t--sample               Sample to annotate in the VCF. Default is the first");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {
    while (!arguments.isEmpty()) {
      String arg = arguments.pop();
      if (arg.equals("--vcf")) {
        vcf = new File(arguments.pop());
      } else if (arg.equals("--threads")) {
        threads = Byte.parseByte(arguments.pop());
      } else if (arg.equals("--out")) {
        output = new File(arguments.pop());
      } else if (arg.equals("--bam")) {
        bamFile = new File(arguments.pop());
      } else if (arg.equals("--adKey")) {
        adKey = arguments.pop();
      } else if (arg.equals("--countDups")) {
        useDups = true;
      } else if (arg.equals("--minMappingQuality")) {
        minMappingQuality = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--minBaseQuality")) {
        minBaseQuality = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--sample")) {
        sampleName = arguments.pop();
      } else {
        unusedArgs.add(arg);
      }
    }
    if (bamFile == null) {
      printUsage("bam not set");
      return 1;
    }
    if(StringUtils.isEmpty(adKey)) {
      printUsage("adKey cannot be empty");
      return 1;
    }
    if (vcf == null) {
      printUsage("vcf not set");
      return 1;
    }

    if (output == null) {
      printUsage("out not set");
      return 1;
    }
    
    return 0;
  }

  @Override
  public int run() {
    SamReaderFactory samReaderFactory = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
    SamReader samReader = samReaderFactory.open(bamFile);
    try {
      SAMFileHeader header = samReader.getFileHeader();
      if (!header.getSortOrder().equals(SAMFileHeader.SortOrder.coordinate)) {
        throw new RuntimeException("BAM must be coordinate sorted");
      }
      computeFreq(output, vcf, bamFile);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    finally {
      try {
        samReader.close();
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }

    return 0;
  }

  public void computeFreq(File output, File vcf, File bamFile) throws IOException {
    SamReaderFactory samReaderFactory = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
    SamReader samReader = samReaderFactory.open(bamFile);
    VCFFileReader reader = null;
    VariantContextWriter writer = null;
    try {
      reader = new VCFFileReader(vcf, false);
      final VariantContextWriterBuilder builder = new VariantContextWriterBuilder()
              .setOutputFile(output)
              .setReferenceDictionary(reader.getFileHeader().getSequenceDictionary())
              .setOption(Options.INDEX_ON_THE_FLY);
      writer = builder.build();
      
      Set<VCFHeaderLine> headerLines = new HashSet<VCFHeaderLine>(reader.getFileHeader().getMetaDataInInputOrder());
      headerLines.add(new VCFFormatHeaderLine(adKey, VCFHeaderLineCount.UNBOUNDED, VCFHeaderLineType.Integer, "Allelic depths for the ref and alt alleles in the order listed"));

      VCFHeader newHeader = new VCFHeader(headerLines, reader.getFileHeader().getGenotypeSamples());
      writer.writeHeader(newHeader);
      int sampleIdx = 0;
      if(sampleName != null) {
        sampleIdx = -1;
        for(int i=0; i < newHeader.getGenotypeSamples().size(); i++) {
          if(newHeader.getGenotypeSamples().get(i).equals(sampleName)) {
            sampleIdx = i;
            break;
          }
        }

        if(sampleIdx == -1)
          throw new RuntimeException("Couldn't find sample '"+sampleName+"' in the VCF");
      }

      CountAtLoci countLoci = new CountAtLoci(samReader, minMappingQuality,  minBaseQuality, useDups);
      for(VariantContext variantCtx : reader) {
        // Ignore the rest for the moment
        if(variantCtx.isSNP()) {
          ArrayList<String> alleleLst = new ArrayList<String>();
          alleleLst.add(variantCtx.getReference().getBaseString());
          for(Allele a : variantCtx.getAlternateAlleles()) {
            alleleLst.add(a.getBaseString());
          }
          
          String alleles[] = alleleLst.toArray(new String[0]);
          int counts[] = countLoci.count(variantCtx.getChr(), variantCtx.getStart(), alleles);
          GenotypesContext gctx = variantCtx.getGenotypes();
          GenotypeBuilder gBuilder = new GenotypeBuilder(gctx.get(sampleIdx));
          if(adKey.equals(VCFConstants.GENOTYPE_ALLELE_DEPTHS)) {
            gBuilder.AD(counts);
          }
          gBuilder.attribute(adKey, counts);
          VariantContextBuilder vcBuilder = new VariantContextBuilder(variantCtx);
          
          GenotypesContext newGenoCtx = GenotypesContext.create();
          for(int i=0; i < gctx.getSampleNames().size(); i++) {
            if(i == sampleIdx) {
              newGenoCtx.add(gBuilder.make());
            }
            else {
              newGenoCtx.add(gctx.get(i));
            }
          }
          vcBuilder.genotypes(newGenoCtx);
          VariantContext newVarCtx = vcBuilder.make();
          writer.add(newVarCtx);
        }
        else {
          writer.add(variantCtx);
        }
      }
    } finally {
      if(writer != null)
        writer.close();
      if(reader != null)
        reader.close();
      samReader.close();
    }
  }
}
