/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import gnu.trove.list.array.TFloatArrayList;
import gnu.trove.list.array.TIntArrayList;
import htsjdk.samtools.SAMSequenceDictionary;
import htsjdk.samtools.SAMSequenceRecord;
import htsjdk.samtools.util.IOUtil;
import htsjdk.tribble.Feature;
import htsjdk.tribble.SimpleFeature;

import java.io.File;
import java.io.IOException;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import ca.mcgill.genome.bvatools.DefaultTool;
import ca.mcgill.genome.bvatools.experimental.SamtoolsPairFilter;
import ca.mcgill.genome.bvatools.graphs.RainFallPlot;
import ca.mcgill.genome.bvatools.util.SAMUtils;
import ca.mcgill.mcb.pcingola.fileIterator.VcfFileIterator;
import ca.mcgill.mcb.pcingola.interval.Marker;
import ca.mcgill.mcb.pcingola.vcf.VcfEntry;

public class Rainfall extends DefaultTool {
  public static final String substitutionOrder[] = { "A>T", "A>C", "A>G", "C>A", "C>G", "C>T", "G>A", "G>C", "G>T", "T>A", "T>C", "T>G" };
  
  private SAMSequenceDictionary referenceDictionary;
  private int width = 1280;
  private int height = 768;
  private int minDepth = 0;
  private int window = 3000;
  private String somaticFlag = SamtoolsPairFilter.SOMATIC_FLAG;
  private String adKey = VCFFrequency.ALLELE_DEPTH_KEY;
  private String filter = "";
  private boolean generateSVG = false;
  private Set<String> toExclude = new HashSet<String>();
  private File vcf = null;
  private String outputPrefix = null;


  @Override
  public String getCmdName() {
    return "rainfall";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("\t--vcf         Input VCF");
    System.out.println("\t--prefix      Output file prefix");
    System.out.println("\t--refdict     Reference dictionary");
    System.out.println("\t--width       Plot width. (default: " + width + ")");
    System.out.println("\t--height      Plot height. (default: " + height + ")");
    System.out.println("\t--minDepth    Minimum depth on which to use a position. (default: " + minDepth + ")");
    System.out.println("\t--exclude     Chromosomes to exclude. Comma seperated");
    System.out.println("\t--somaticFlag Info flag to use for somatic variants (default: "+somaticFlag+")");
    System.out.println("\t--filter      Filter on this (default: "+filter+")");
    System.out.println("\t--adKey       Depth key name to use. Can be switched to AD to resemble Haplotype Caller or MuTect. default("+adKey+")");
    System.out.println("\t--svg         Generate SVG of the plot. Can crash if plot is too big");
    System.out.println("\t--window      Window size for Depth Ratio/BAF plots. (default: " + window + ")");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {
    while (!arguments.isEmpty()) {
      String arg = arguments.pop();
      if (arg.equals("--vcf")) {
        vcf = new File(arguments.pop());
      } else if (arg.equals("--minDepth")) {
        minDepth = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--width")) {
        width = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--height")) {
        height = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--svg")) {
        generateSVG = true;
      } else if (arg.equals("--prefix")) {
        outputPrefix = arguments.pop();
      } else if (arg.equals("--somaticFlag")) {
        somaticFlag = arguments.pop();
      } else if (arg.equals("--filter")) {
        filter = arguments.pop();
      } else if (arg.equals("--adKey")) {
        adKey = arguments.pop();
      } else if (arg.equals("--refdict")) {
        referenceDictionary = SAMUtils.getSequenceDictionary(new File(arguments.pop()));
      } else if (arg.equals("--exclude")) {
        arg = arguments.pop();
        for (String chr : arg.split(",")) {
          toExclude.add(chr);
        }
      } else if (arguments.pop().equals("--window")) {
        window = Integer.parseInt(arguments.pop());
      } else {
        unusedArgs.add(arg);
      }
    }
    if (vcf == null) {
      printUsage("vcf not set");
      return 1;
    }
    if (outputPrefix == null) {
      printUsage("prefix not set");
      return 1;
    }
    if (referenceDictionary == null) {
      printUsage("refdict not set");
      return 1;
    }
    if(minDepth < 1) {
      printUsage("minDepth needs to at least be 1");
      return 1;
    }
    return 0;
  }

  @Override
  public int run() {
    try {
      computePlots(vcf, outputPrefix);
    }
    catch(IOException e) {
      throw new RuntimeException(e);
    }

    return 0;
  }

  public void computePlots(File vcf, String outputPrefix) throws IOException {
    LinkedHashMap<String, Feature> genome = new LinkedHashMap<String, Feature>();
    for (SAMSequenceRecord seq : referenceDictionary.getSequences()) {
      if(!toExclude.contains(seq.getSequenceName())) {
        Feature chr = new SimpleFeature(seq.getSequenceName(), 1, seq.getSequenceLength());
        genome.put(chr.getChr(), chr);
      }
    }
    
    VcfFileIterator vcfParser = new VcfFileIterator(IOUtil.openFileForBufferedReading(vcf));

    Map<String, Marker> somaticSubstitutionsPrevPos = new HashMap<String, Marker>();

    RainFallPlot rainPlot = new RainFallPlot(genome, width, height, generateSVG);
    int variantIdx=1;
    for (VcfEntry vcfEntry : vcfParser) {
      if(!vcfEntry.isSnp() || vcfEntry.getRef().indexOf('N') != -1 || vcfEntry.getAltsStr().indexOf('N') != -1) {
        continue;
      }

      if(!somaticFlag.isEmpty() && vcfEntry.getInfoFlag(somaticFlag)) {
        String substKey = vcfEntry.getRef() + '>' + vcfEntry.getAlts()[0];
        
        Marker prevMarker = somaticSubstitutionsPrevPos.get(substKey);
        if(prevMarker != null && prevMarker.getChromosomeName().equals(vcfEntry.getChromosomeName())) {
          int value = vcfEntry.getStart() - prevMarker.getStart();
          //if(value == 0)
          //  continue;
          rainPlot.addPoint(substKey, vcfEntry.getChromosomeName(), variantIdx, value);
          variantIdx++;
        }
        somaticSubstitutionsPrevPos.put(substKey, vcfEntry);
      }
    }
    
    rainPlot.write(outputPrefix + ".rain");
  }
}
