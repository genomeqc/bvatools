/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.input.CountingInputStream;

import ca.mcgill.genome.bvatools.DefaultTool;
import ca.mcgill.genome.bvatools.graphs.DepthRatioBAlleleFreqPlot;
import ca.mcgill.genome.bvatools.util.NucleotideAlleleCounts;
import ca.mcgill.genome.bvatools.util.SAMUtils;
import ca.mcgill.genome.bvatools.util.SampleAlleleCounts;

import gnu.trove.list.array.TIntArrayList;
import htsjdk.samtools.Defaults;
import htsjdk.samtools.SAMSequenceDictionary;
import htsjdk.samtools.SAMSequenceRecord;
import htsjdk.tribble.Feature;
import htsjdk.tribble.SimpleFeature;

public class DepthRatioBAF extends DefaultTool {
  private SAMSequenceDictionary referenceDictionary;
  private boolean generatePlot = false;
  private int width = 1280;
  private int height = 768;
  private int minDepth = 20;
  private int maxDepth = 1000;
  private boolean useMedianForExpected = false;
  private boolean generateSVG = false;
  private Set<String> toExclude = new HashSet<>();
  private File basefreq = null;
  private File snppos = null;
  private String outputPrefix = null;

  @Override
  public String getCmdName() {
    return "ratiobaf";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("\t--snppos                SNP positions in TSV with alleles. chr<TAB>1-basedPosition<TAB>ID<TAB>Allele_A<TAB>Allele_B");
    System.out.println("\t--basefreq              BaseFreq file for given positions");
    System.out.println("\t--prefix                Output file prefix");
    System.out.println("\t--refdict               Reference dictionary (required for --plot only)");
    System.out.println("\t--plot                  If set, also output a chromosome plot of LogRRatio and B allele Frequency (--dict needed)");
    System.out.println("\t--width                 Plot width. (default: " + width + ")");
    System.out.println("\t--height                Plot height. (default: " + height + ")");
    System.out.println("\t--minDepth              Minimum depth on which to use a position. (default: " + minDepth + ")");
    System.out.println("\t--maxDepth              Maximum depth on which to use a position. (default: " + maxDepth + ")");
    System.out.println("\t--useMedianForExpected  Use median instead of average as expected value for ratio calculation. (default: false)");
    System.out.println("\t--exclude               Chromosomes to exclude. Comma seperated");
    System.out.println("\t--svg                   Generate SVG of the plot. Can crash if plot is too big");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {
    while (!arguments.isEmpty()) {
      String arg = arguments.pop();
      switch (arg) {
      case "--snppos":
        snppos = new File(arguments.pop());
        break;
      case "--minDepth":
        minDepth = Integer.parseInt(arguments.pop());
        break;
      case "--maxDepth":
        maxDepth = Integer.parseInt(arguments.pop());
        break;
      case "--basefreq":
        basefreq = new File(arguments.pop());
        break;
      case "--width":
        width = Integer.parseInt(arguments.pop());
        break;
      case "--useMedianForExpected":
        useMedianForExpected = true;
        break;
      case "--height":
        height = Integer.parseInt(arguments.pop());
        break;
      case "--svg":
        generateSVG = true;
        break;
      case "--prefix":
        outputPrefix = arguments.pop();
        break;
      case "--refdict":
        referenceDictionary = SAMUtils.getSequenceDictionary(new File(arguments.pop()));
        break;
      case "--exclude":
        String extraArgs = arguments.pop();
        Collections.addAll(toExclude, extraArgs.split(","));
        break;
      case "--plot":
        generatePlot = true;
        break;
      default:
        unusedArgs.add(arg);
        break;
      }
    }

    if (snppos == null) {
      printUsage("snppos not set");
      return 1;
    }

    if (basefreq == null) {
      printUsage("basefreq not set");
      return 1;
    }
    if (outputPrefix == null) {
      printUsage("prefix not set");
      return 1;
    }
    if (generatePlot && referenceDictionary == null) {
      printUsage("refdict is needed when generating plots");
      return 1;
    }
    return 0;
  }

  @Override
  public int run() {
    LinkedHashMap<String, Feature> genome = null;
    if (referenceDictionary != null) {
      genome = new LinkedHashMap<>();
      for (SAMSequenceRecord seq : referenceDictionary.getSequences()) {
        if (!toExclude.contains(seq.getSequenceName())) {
          Feature chr = new SimpleFeature(seq.getSequenceName(), 1, seq.getSequenceLength());
          genome.put(chr.getChr(), chr);
        }
      }
    }

    List<NucleotideAlleleCounts> positions = parsePositions(genome);
    computeLRRBAF(genome, positions);

    return 0;
  }

  public List<NucleotideAlleleCounts> parsePositions(Map<String, Feature> genome) {

    Map<String, NucleotideAlleleCounts> positions = new HashMap<>();

    try (CountingInputStream countingPosStream = new CountingInputStream(new FileInputStream(snppos));
         BufferedReader positionReader = new BufferedReader(new InputStreamReader(countingPosStream, StandardCharsets.US_ASCII), Defaults.NON_ZERO_BUFFER_SIZE);
         BufferedReader frequencyReader = new BufferedReader(new InputStreamReader(new FileInputStream(basefreq), StandardCharsets.US_ASCII),
             Defaults.NON_ZERO_BUFFER_SIZE)) {

      frequencyReader.readLine(); // skip header
      Set<String> skippedChr = new HashSet<>();

      long posFileLength = snppos.length();
      long prevPct = 0;
      while (true) {
        String posLine;
        while (true) {
          posLine = positionReader.readLine();
          if (posLine != null && posLine.length() > 0 && posLine.charAt(0) == '#') {
            continue;
          }
          break;
        }
        String freqLine;
        while (true) {
          freqLine = frequencyReader.readLine();
          if (freqLine != null && freqLine.length() > 0 && freqLine.charAt(0) == '#') {
            continue;
          }
          break;
        }

        if (posLine == null || freqLine == null) {
          break;
        }

        long pct = 100L * countingPosStream.getByteCount() / posFileLength;
        if (pct != prevPct) {
          prevPct = pct;
          System.err.print("\rCompletion: ");
          System.err.print(pct);
          System.err.print('%');
        }

        String[] freqValues = freqLine.split(",");
        String freqChr = freqValues[0];
        String freqKey = freqChr + '#' + freqValues[1];

        if (toExclude.contains(freqChr)) {
          continue;
        } else if (genome != null) {
          if (!genome.containsKey(freqChr)) {
            if (!skippedChr.contains(freqChr)) {
              skippedChr.add(freqChr);
              System.err.println("Skipping chromosome not in dictionary: " + freqChr);
            }
            continue;
          }
        }

        String[] posValues = posLine.split("\t");
        String posChr = posValues[0];
        String posKey = posChr + '#' + posValues[1];

        if (!freqKey.equals(posKey)) {
          throw new RuntimeException("BaseFreq file and SnpPos file aren't in sync: " + freqLine + " vs " + posLine);
        }

        int freqStart = Integer.parseInt(freqValues[1]);

        if (posValues[3].length() > 1 || posValues[4].length() > 1) {
          System.err.println("Ignoring indels: " + posLine);
          continue;
        }
        final SampleAlleleCounts.ReadBases alleleA = SampleAlleleCounts.ReadBases.valueOf(posValues[3]);
        final SampleAlleleCounts.ReadBases alleleB = SampleAlleleCounts.ReadBases.valueOf(posValues[4]);

        NucleotideAlleleCounts alleleCounts = NucleotideAlleleCounts.valueOf(freqChr, freqStart, freqValues[2]);
        alleleCounts.setAlleleA(alleleA);
        alleleCounts.setAlleleB(alleleB);
        positions.put(freqKey, alleleCounts);
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    System.err.println("\rCompletion: 100%");

    return new ArrayList<>(positions.values());
  }

  public void computeLRRBAF(LinkedHashMap<String, Feature> genome, List<NucleotideAlleleCounts> positions) {
    TIntArrayList posDepths = new TIntArrayList();
    long tot = 0;
    for (NucleotideAlleleCounts alleleCounts : positions) {
      int depth = alleleCounts.getBamCounts()[alleleCounts.getAlleleA().ordinal()] + alleleCounts.getBamCounts()[alleleCounts.getAlleleB().ordinal()];
      // Don't use outliers in depth computation
      if (depth < minDepth || depth > maxDepth) {
        continue;
      }
      tot += depth;
      posDepths.add(depth);
    }
    posDepths.sort();
    int median = posDepths.get(posDepths.size() / 2);
    int average = (int) (tot / posDepths.size());
    // Clear the positions
    posDepths = null;

    printLRRBAF(genome, positions, outputPrefix, median, average);
  }

  private void printLRRBAF(LinkedHashMap<String, Feature> genome, List<NucleotideAlleleCounts> alleleCountsList, String outputPrefix, int median, int average) {
    PrintWriter lrrbaf = null;

    try {
      int genomeExpectedDepth = average;
      if (useMedianForExpected) {
        genomeExpectedDepth = median;
      }
      File lrrbafOutput = new File(outputPrefix + ".csv");
      lrrbaf = new PrintWriter(lrrbafOutput, StandardCharsets.US_ASCII);
      lrrbaf.println("# Median : " + median);
      lrrbaf.println("# Average: " + average);
      lrrbaf.println("Chr,Pos,LRR,BAF,Depth");

      DepthRatioBAlleleFreqPlot chrPlot = null;
      if (generatePlot) {
        chrPlot = new DepthRatioBAlleleFreqPlot(genome, width, height, generateSVG);
      }

      for (NucleotideAlleleCounts alleleCounts : alleleCountsList) {
        double alleleADepth = alleleCounts.getBamCounts()[alleleCounts.getAlleleA().ordinal()];
        double alleleBDepth = alleleCounts.getBamCounts()[alleleCounts.getAlleleB().ordinal()];
        double depth = alleleADepth + alleleBDepth;
        if ((int) depth < minDepth) {
          continue;
        }

        double lrr = Math.log(depth / (double) genomeExpectedDepth) / Math.log(2);
        double baf = alleleBDepth / depth;

        lrrbaf.print(alleleCounts.getChromosome());
        lrrbaf.print(',');
        lrrbaf.print(alleleCounts.getPosition());
        lrrbaf.print(',');
        lrrbaf.print(lrr);
        lrrbaf.print(',');
        lrrbaf.print(baf);
        lrrbaf.print(',');
        lrrbaf.println(depth);

        if (generatePlot) {
          long chromosomeSize = referenceDictionary.getSequence(alleleCounts.getChromosome()).getSequenceLength();
          if (alleleCounts.getPosition() > chromosomeSize) {
            throw new RuntimeException("Position shoudn't be > than chrom size: " + alleleCounts.getPosition() + " > " + chromosomeSize);
          }
          chrPlot.addPoint(DepthRatioBAlleleFreqPlot.DepthRatioBAF.DEPTH_RATIO, alleleCounts.getChromosome(), alleleCounts.getPosition(), lrr);
          chrPlot.addPoint(DepthRatioBAlleleFreqPlot.DepthRatioBAF.BAF, alleleCounts.getChromosome(), alleleCounts.getPosition(), baf);

        }
      }
      lrrbaf.close();
      lrrbaf = null;

      if (generatePlot) {
        chrPlot.write(outputPrefix);
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    } finally {
      if (lrrbaf != null) {
        lrrbaf.close();
      }
    }
  }
}
