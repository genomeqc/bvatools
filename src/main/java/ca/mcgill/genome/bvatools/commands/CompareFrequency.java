/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import ca.mcgill.genome.bvatools.DefaultTool;
import ca.mcgill.genome.bvatools.homology.SampleComparator;
import ca.mcgill.genome.bvatools.homology.SampleComparator.ComparisonResults;
import ca.mcgill.genome.bvatools.util.AlleleCounts;

public class CompareFrequency extends DefaultTool {
  public enum HomozygousMatch {
    NONE, ONE, BOTH
  }

  private HomozygousMatch homMatch = HomozygousMatch.NONE;
  private int threads = 1;
  private int minDepth = 3;
  private double hetThreshold = 0.3;
  private double matchFraction = 0.95;
  private boolean printMismatches = false;
  private String sampleName = null;
  private File sampleCountsFile = null;
  private File compareListFile = null;

  @Override
  public String getCmdName() {
    return "cmpfreq";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("\t--het               At which threshold do we call a valid het. (default: " + hetThreshold + ")");
    System.out.println("\t--match             At what percent do we declare a match. (default: " + matchFraction + ")");
    System.out.println("\t--sample            Sample name");
    System.out.println("\t--sampleCounts      Sample counts file");
    System.out.println("\t--compareList       List of files to compare to. Format <Name,Alias,Path>. The name will be used to see if it corresponds");
    System.out.println("\t--minDepth          Minimum Depth to consider. (default: " + minDepth + ")");
    System.out.println("\t--homMatch          Use only homozygous calls. (default: " + homMatch + ")");
    System.out.println("\t                    " + HomozygousMatch.NONE + " : Use Homozygous and Heterozygous calls");
    System.out.println("\t                    " + HomozygousMatch.ONE + " : One of the 2 samples must be homozygous");
    System.out.println("\t                    " + HomozygousMatch.BOTH + " : Both need to be homozygous");
    System.out.println("\t--threads           Threads to use. (default: " + threads + ")");
    System.out.println("\t--printMismatches   Print mismatching sites");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {
    while (!arguments.isEmpty()) {
      String arg = arguments.pop();

      switch (arg) {
      case "--sample":
        sampleName = arguments.pop();
        break;
      case "--het":
        hetThreshold = Double.parseDouble(arguments.pop());
        break;
      case "--match":
        matchFraction = Double.parseDouble(arguments.pop());
        break;
      case "--sampleCounts":
        sampleCountsFile = new File(arguments.pop());
        break;
      case "--compareList":
        compareListFile = new File(arguments.pop());
        break;
      case "--minDepth":
        minDepth = Integer.parseInt(arguments.pop());
        break;
      case "--threads":
        threads = Integer.parseInt(arguments.pop());
        break;
      case "--homMatch":
        homMatch = Enum.valueOf(HomozygousMatch.class, arguments.pop());
        break;
      case "--printMismatches":
        printMismatches = true;
        break;
      }
    }

    if (sampleName == null || sampleCountsFile == null) {
      printUsage("Both sample and sampleCounts need to be specified");
      return 1;
    }
    if (compareListFile == null) {
      printUsage("Both compareList needs to be specified");
      return 1;
    }

    return 0;
  }

  @Override
  public int run() {
    List<SampleDetails> compareList = parseSamplesFile(compareListFile);
    SampleDetails sample = new SampleDetails(sampleName, "", sampleCountsFile);
    compareCounts(sample, compareList);
    return 0;
  }

  public List<SampleDetails> parseSamplesFile(File compareListFile) {

    List<SampleDetails> retVal = new ArrayList<>();
    try (BufferedReader reader = new BufferedReader(
        new InputStreamReader(new BufferedInputStream(new FileInputStream(compareListFile), 1024 * 1024), StandardCharsets.US_ASCII))) {

      while (true) {
        String line = reader.readLine();
        if (line == null) {
          break;
        }

        if (line.length() == 0 || line.startsWith("#")) {
          continue;
        }
        String[] values = line.split(",");

        SampleDetails sd = new SampleDetails(values[0], values[1], new File(values[2]));
        retVal.add(sd);
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    return retVal;
  }

  public void compareCounts(SampleDetails sample, List<SampleDetails> samplesToCompare) {
    ExecutorService sampleComparatorExecutor = Executors.newFixedThreadPool(threads);

    try {
      List<Future<ComparisonResults>> futures = new ArrayList<>();
      List<AlleleCounts> sampleAlleleCounts = SampleComparator.parseAlleleCounts(sample.getAlleleCountsFile());
      for (SampleDetails sampleToCompare : samplesToCompare) {
        futures.add(sampleComparatorExecutor.submit(new SampleComparator(sample, sampleToCompare, sampleAlleleCounts, minDepth, hetThreshold, homMatch,
            printMismatches)));
      }

      boolean first = true;
      for (Future<ComparisonResults> future : futures) {
        ComparisonResults result = future.get();
        if (first) {
          first = false;
          result.printHeader(System.out);
        }
        result.printResult(matchFraction, System.out);
      }
    } catch (InterruptedException | ExecutionException e) {
      throw new RuntimeException(e);
    } finally {
      sampleComparatorExecutor.shutdownNow();
    }
  }

  public static class SampleDetails {
    private final String sample;
    private final String alias;
    private final File alleleCountsFile;

    public SampleDetails(String sample, String alias, File alleleCountsFile) {
      this.sample = sample;
      this.alias = alias;
      this.alleleCountsFile = alleleCountsFile;
    }

    public String getSample() {
      return sample;
    }

    public String getAlias() {
      return alias;
    }

    public File getAlleleCountsFile() {
      return alleleCountsFile;
    }
  }
}
