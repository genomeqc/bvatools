/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import ca.mcgill.genome.bvatools.DefaultTool;
import ca.mcgill.genome.bvatools.filterdups.DuplicateKmerBuilder;
import ca.mcgill.genome.bvatools.parsers.BAMSequenceParser;
import ca.mcgill.genome.bvatools.parsers.FASTQFileParser;
import ca.mcgill.genome.bvatools.parsers.FASTQPEFileParser;
import ca.mcgill.genome.bvatools.parsers.FASTQPEFileParser.FastqFormat;
import ca.mcgill.genome.bvatools.parsers.Fragment;
import ca.mcgill.genome.bvatools.parsers.MultiSequenceParser;
import ca.mcgill.genome.bvatools.parsers.SFFFileParser;
import ca.mcgill.genome.bvatools.parsers.SequenceFileParser;
import ca.mcgill.genome.bvatools.readsQC.DuplicateKMerPercentPlugin;
import ca.mcgill.genome.bvatools.readsQC.IQCPlugin;
import ca.mcgill.genome.bvatools.readsQC.KmerPositionPlugin;
import ca.mcgill.genome.bvatools.readsQC.KnownSequenceCountPlugin;
import ca.mcgill.genome.bvatools.readsQC.KnownSequenceCountPlugin.IdentifiedSequence;
import ca.mcgill.genome.bvatools.readsQC.QCXml;
import ca.mcgill.genome.bvatools.readsQC.ReadSoftclipsPlugin;
import ca.mcgill.genome.bvatools.readsQC.RunTrimmingPlugin;
import ca.mcgill.genome.bvatools.readsQC.SequenceContentPlugin;
import ca.mcgill.genome.bvatools.readsQC.SequenceQualityPlugin;

import gnu.trove.map.TObjectLongMap;
import gnu.trove.map.hash.TObjectLongHashMap;
import htsjdk.samtools.SAMReadGroupRecord;

public class ReadsQC extends DefaultTool {
  private static final String ALL_READS_DEFAULT_KEY = "all";

  public enum MetricAccumulationLevel {
    ALL_READS, SAMPLE, LIBRARY, READ_GROUP
  }

  public enum FileFormats {
    FASTQ, SFF, BAM
  }

  ;

  private int threads = 1;
  private byte qualityOffset = 33;
  private boolean generateGraphs = true;
  private boolean perRG = false;
  private boolean noDups = false;
  private boolean opticalDups = false;
  private String regionName = null;
  private FileFormats type = FileFormats.FASTQ;
  private FastqFormat headerType = FastqFormat.AUTO;
  private int dupKmerSize = DuplicateKmerBuilder.DEFAULT_KMER_SIZE;
  private int dupOffset = DuplicateKmerBuilder.DEFAULT_OFFSET;
  private List<File> read1s = new ArrayList<File>();
  private List<File> read2s = new ArrayList<File>();
  private File knownSequences = null;
  private File outputDirectory = null;

  @Override
  public String getCmdName() {
    return "readsqc";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("\t--read1                   Read 1 file (used for sff and bams). Can be used multiple times");
    System.out.println("\t--read2                   Read 2 file. Can be used multiple times");
    System.out.println("\t--knowns                  Known sequences file. For example, adapters. If not given, a default set is used.");
    System.out.println("\t--threads                 Threads to use. (default: " + threads + ")");
    System.out.println("\t--noGraph                 Don't generate graphs, only the xml stats file");
    System.out.println("\t--regionName              Region Name to put in output graph file name");
    System.out.println("\t--perRG                   Also extract QCs per PlatformUnit and per Library. BAM only");
    System.out.println("\t--noDups                  Do not compute duplicates. Saves a lot of RAM");
    System.out.println("\t--opticalDups             Compute optical duplicates. Uses a lot of RAM and processing");
    System.out.println("\t--dupOffset               Read offset to get kmer (default: " + dupOffset + ")");
    System.out.println("\t--dupKmer                 kmer size (default: " + dupKmerSize + ")");
    System.out.println("\t--output                  Output directory");
    System.out.println("\t--type                    File types, one of: " + Arrays.toString(FileFormats.values()) + " (default: " + type + ")");
    System.out.println(
        "\t--pairedHeaderValidation  Header format for paired validation. One of: " + Arrays.toString(FastqFormat.values()) + " (default: " + headerType + ")");
    System.out.println("\t--quality                 Quality Offset (default: " + qualityOffset + ")");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {
    while (!arguments.isEmpty()) {
      String arg = arguments.pop();
      if (arg.equals("--read1")) {
        read1s.add(new File(arguments.pop()));
      } else if (arg.equals("--read2")) {
        read2s.add(new File(arguments.pop()));
      } else if (arg.equals("--perRG")) {
        perRG = true;
      } else if (arg.equals("--noDups")) {
        noDups = true;
      } else if (arg.equals("--opticalDups")) {
        opticalDups = true;
      } else if (arg.equals("--dupKmer")) {
        dupKmerSize = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--dupOffset")) {
        dupOffset = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--knowns")) {
        knownSequences = new File(arguments.pop());
      } else if (arg.equals("--threads")) {
        threads = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--noGraph")) {
        generateGraphs = false;
      } else if (arg.equals("--regionName")) {
        regionName = arguments.pop();
      } else if (arg.equals("--output")) {
        outputDirectory = new File(arguments.pop());
      } else if (arg.equals("--type")) {
        type = Enum.valueOf(FileFormats.class, arguments.pop());
      } else if (arg.equals("--pairedHeaderValidation")) {
        headerType = Enum.valueOf(FastqFormat.class, arguments.pop());
      } else if (arg.equals("--quality")) {
        qualityOffset = Byte.parseByte(arguments.pop());
      } else {
        unusedArgs.add(arg);
      }
    }

    if (read1s.size() == 0) {
      printUsage("read1 not set");
      return 1;
    }

    if (perRG && type != FileFormats.BAM) {
      printUsage("perRG is only valid for BAM files");
      return 1;
    }

    return 0;
  }

  @Override
  public int run() {
    return generateQC();
  }

  private int generateQC() {
    try {
      EnumMap<MetricAccumulationLevel, Map<String, List<IQCPlugin>>> pluginsPerAccumulator = new EnumMap<MetricAccumulationLevel, Map<String, List<IQCPlugin>>>(
          MetricAccumulationLevel.class);
      EnumMap<MetricAccumulationLevel, TObjectLongMap<String>> nbFragmentsPerAccumulator = new EnumMap<MetricAccumulationLevel, TObjectLongMap<String>>(
          MetricAccumulationLevel.class);
      pluginsPerAccumulator.put(MetricAccumulationLevel.ALL_READS, new HashMap<String, List<IQCPlugin>>());
      pluginsPerAccumulator.get(MetricAccumulationLevel.ALL_READS).put(ALL_READS_DEFAULT_KEY, new ArrayList<IQCPlugin>());
      nbFragmentsPerAccumulator.put(MetricAccumulationLevel.ALL_READS, new TObjectLongHashMap<String>());
      nbFragmentsPerAccumulator.get(MetricAccumulationLevel.ALL_READS).put(ALL_READS_DEFAULT_KEY, 0);
      if (perRG) {
        pluginsPerAccumulator.put(MetricAccumulationLevel.LIBRARY, new HashMap<String, List<IQCPlugin>>());
        nbFragmentsPerAccumulator.put(MetricAccumulationLevel.LIBRARY, new TObjectLongHashMap<String>());
        pluginsPerAccumulator.put(MetricAccumulationLevel.READ_GROUP, new HashMap<String, List<IQCPlugin>>());
        nbFragmentsPerAccumulator.put(MetricAccumulationLevel.READ_GROUP, new TObjectLongHashMap<String>());
      }

      SequenceFileParser fileParsers[] = new SequenceFileParser[read1s.size()];
      long totalFileSize = setupStream(read1s, read2s, knownSequences, fileParsers, pluginsPerAccumulator, nbFragmentsPerAccumulator, qualityOffset);

      MultiSequenceParser fileParser = new MultiSequenceParser(fileParsers);
      long lastPercent = -1;
      System.out.println();
      while (true) {
        Fragment fragment = fileParser.nextSequence();
        if (fragment == null)
          break;

        for (MetricAccumulationLevel level : pluginsPerAccumulator.keySet()) {
          if (level == MetricAccumulationLevel.ALL_READS) {
            nbFragmentsPerAccumulator.get(MetricAccumulationLevel.ALL_READS).increment(ALL_READS_DEFAULT_KEY);
            for (IQCPlugin plugin : pluginsPerAccumulator.get(level).get(ALL_READS_DEFAULT_KEY)) {
              plugin.processSequence(fragment);
            }
          } else if (level == MetricAccumulationLevel.READ_GROUP) {
            nbFragmentsPerAccumulator.get(MetricAccumulationLevel.READ_GROUP).increment(fragment.getReadGroup());
            for (IQCPlugin plugin : pluginsPerAccumulator.get(level).get(fragment.getReadGroup())) {
              plugin.processSequence(fragment);
            }
          } else if (level == MetricAccumulationLevel.LIBRARY) {
            nbFragmentsPerAccumulator.get(MetricAccumulationLevel.LIBRARY).increment(fragment.getLibrary());
            for (IQCPlugin plugin : pluginsPerAccumulator.get(level).get(fragment.getLibrary())) {
              plugin.processSequence(fragment);
            }
          }
        }

        long percent = fileParser.getByteCount() * 100l / totalFileSize;
        if (percent != lastPercent) {
          lastPercent = percent;
          System.out.print("\rFile read: " + percent);
        }
      }
      System.out.println();

      if (generateGraphs) {
        for (MetricAccumulationLevel level : pluginsPerAccumulator.keySet()) {
          for (String levelKey : pluginsPerAccumulator.get(level).keySet()) {
            for (IQCPlugin plugin : pluginsPerAccumulator.get(level).get(levelKey)) {
              String outputName = generateOutputName(level, levelKey);

              if (outputName.length() > 0) {
                plugin.setOutputRegionName(outputName);
              }
              plugin.generateGraph(outputDirectory);
            }
          }
        }
      }
      fileParser.close();

      printSummary(outputDirectory, pluginsPerAccumulator, nbFragmentsPerAccumulator);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    return 0;
  }

  private String generateOutputName(MetricAccumulationLevel level, String levelKey) {
    String outputName = "";
    if (regionName != null) {
      outputName = regionName;
    }
    if (level == MetricAccumulationLevel.READ_GROUP) {
      outputName += "-RG-" + levelKey;
    } else if (level == MetricAccumulationLevel.LIBRARY) {
      outputName += "-LIB-" + levelKey;
    }
    return outputName;
  }

  private void printSummary(File outputDirectory, EnumMap<MetricAccumulationLevel, Map<String, List<IQCPlugin>>> pluginsPerAccumulator,
      EnumMap<MetricAccumulationLevel, TObjectLongMap<String>> nbFragmentsPerAccumulator) throws IOException {
    for (MetricAccumulationLevel level : pluginsPerAccumulator.keySet()) {
      for (String levelKey : pluginsPerAccumulator.get(level).keySet()) {
        DuplicateKMerPercentPlugin duplicateKMerPercentPlugin = null;
        KnownSequenceCountPlugin knownSequenceCountPlugin = null;
        RunTrimmingPlugin runTrimmingPlugin = null;
        SequenceQualityPlugin sequenceQualityPlugin = null;
        SequenceContentPlugin sequenceContentPlugin = null;
        for (IQCPlugin plugin : pluginsPerAccumulator.get(level).get(levelKey)) {
          if (plugin instanceof DuplicateKMerPercentPlugin) {
            duplicateKMerPercentPlugin = (DuplicateKMerPercentPlugin) plugin;
          } else if (plugin instanceof KnownSequenceCountPlugin) {
            knownSequenceCountPlugin = (KnownSequenceCountPlugin) plugin;
          } else if (plugin instanceof RunTrimmingPlugin) {
            runTrimmingPlugin = (RunTrimmingPlugin) plugin;
          } else if (plugin instanceof SequenceQualityPlugin) {
            sequenceQualityPlugin = (SequenceQualityPlugin) plugin;
          } else if (plugin instanceof SequenceContentPlugin) {
            sequenceContentPlugin = (SequenceContentPlugin) plugin;
          }
        }
        long nbFragments = nbFragmentsPerAccumulator.get(level).get(levelKey);

        System.out.println("Summary Level      : " + level + " key: " + levelKey);
        System.out.println("Nb Fragments       : " + nbFragments);
        System.out.println("Quality Median     : " + sequenceQualityPlugin.getMedian());
        System.out.println("Quality Mean       : " + sequenceQualityPlugin.getArithmeticMean());
        if (!noDups) {
          System.out.println("Duplication        : " + duplicateKMerPercentPlugin.getPercentDuplication() + "%");
          if (opticalDups)
            System.out.println("Optical Duplication: " + duplicateKMerPercentPlugin.getPercentOpticalDuplication() + "%");
        }
        if (runTrimmingPlugin != null) {
          System.out.println("Length Median      : " + runTrimmingPlugin.getMedian());
          System.out.println("Length Mean        : " + runTrimmingPlugin.getArithmeticMean());
        }
        List<IdentifiedSequence> identifiedSequences = knownSequenceCountPlugin.getIdentifiedSequences();
        if (identifiedSequences.size() > 0) {
          System.out.println("Known sequences found:");
          for (IdentifiedSequence identifiedSequence : identifiedSequences) {
            System.out.print(identifiedSequence.getSequenceName());
            System.out.print(":\t");
            System.out.println(identifiedSequence.getPercent());
          }
        }

        QCXml xml = new QCXml();
        xml.setNbReads(nbFragments);
        xml.setNbBases(sequenceQualityPlugin.getTotalNbBases());
        xml.setAvgQual(sequenceQualityPlugin.getArithmeticMean());
        if (!noDups) {
          xml.setDuplicateRate((float) duplicateKMerPercentPlugin.getPercentDuplication());
          if (opticalDups)
            xml.setOpticalDuplicateRate((float) duplicateKMerPercentPlugin.getPercentOpticalDuplication());
        }

        if (sequenceContentPlugin != null) {
          xml.setAtcgnPercentageForEachCycle(sequenceContentPlugin.getBasePercentageForEachCycle());
        }

        if (runTrimmingPlugin != null) {
          xml.setAvgRawReadLength(runTrimmingPlugin.getRawAvgReadLength());
          xml.setMedianRawReadLength(runTrimmingPlugin.getRawMedianReadLength());
          xml.setStdRawReadLength(runTrimmingPlugin.getRawStdReadLength());

          xml.setAvgTrimReadLength(runTrimmingPlugin.getTrimAvgReadLength());
          xml.setMedianTrimReadLength(runTrimmingPlugin.getTrimMedianReadLength());
          xml.setStdTrimReadLength(runTrimmingPlugin.getTrimStdReadLength());

          xml.setAvgFinalReadLength(runTrimmingPlugin.getFinalAvgReadLength());
          xml.setMedianFinalReadLength(runTrimmingPlugin.getFinalMedianReadLength());
          xml.setStdFinalReadLength(runTrimmingPlugin.getFinalStdReadLength());
        }

        String outputName = generateOutputName(level, levelKey);
        File jobOutput = new File(outputDirectory, "mpsQC_" + outputName + "_stats.xml");
        xml.exportJob(jobOutput);
      }
    }
  }

  private void addPlugins(EnumMap<MetricAccumulationLevel, Map<String, List<IQCPlugin>>> pluginsPerAccumulator, File knownSequences,
      MetricAccumulationLevel level, String levelKey) throws FileNotFoundException {
    List<IQCPlugin> plugins = new ArrayList<IQCPlugin>();
    pluginsPerAccumulator.get(level).put(levelKey, plugins);
    SequenceQualityPlugin sequenceQualityPlugin = new SequenceQualityPlugin();
    plugins.add(sequenceQualityPlugin);
    if (!noDups) {
      DuplicateKMerPercentPlugin dupPlugin;
      if (opticalDups)
        dupPlugin = new DuplicateKMerPercentPlugin(true);
      else
        dupPlugin = new DuplicateKMerPercentPlugin();

      dupPlugin.setKmerSize(dupKmerSize);
      dupPlugin.setOffset(dupOffset);
      plugins.add(dupPlugin);
    }
    KnownSequenceCountPlugin knownSequenceCountPlugin;
    if (knownSequences != null) {
      knownSequenceCountPlugin = new KnownSequenceCountPlugin(knownSequences, threads);
    } else {
      knownSequenceCountPlugin = new KnownSequenceCountPlugin(threads);
    }
    plugins.add(knownSequenceCountPlugin);
    plugins.add(new SequenceContentPlugin());
  }

  private long setupStream(List<File> read1s, List<File> read2s, File knownSequences, SequenceFileParser fileParsers[],
      EnumMap<MetricAccumulationLevel, Map<String, List<IQCPlugin>>> pluginsPerAccumulator,
      EnumMap<MetricAccumulationLevel, TObjectLongMap<String>> nbFragmentsPerAccumulator, byte qualityOffset) throws IOException {
    long totalFileSize = 0;
    addPlugins(pluginsPerAccumulator, knownSequences, MetricAccumulationLevel.ALL_READS, ALL_READS_DEFAULT_KEY);
    if (type == FileFormats.FASTQ) {
      if (read2s.size() > 0) {
        if (read2s.size() != read1s.size()) {
          throw new RuntimeException("The number of read1 files (" + read1s.size() + ")!= number or read2 files (" + read2s.size() + ")");
        }

        for (int idx = 0; idx < read2s.size(); idx++) {
          System.out.println("Reading: " + read2s.get(idx));
          totalFileSize += read1s.get(idx).length() + read2s.get(idx).length();
          fileParsers[idx] = new FASTQPEFileParser(read1s.get(idx), read2s.get(idx), qualityOffset, headerType);
        }
      } else {
        for (int idx = 0; idx < read1s.size(); idx++) {
          totalFileSize += read1s.get(idx).length();
          fileParsers[idx] = new FASTQFileParser(read1s.get(idx), qualityOffset);
        }
      }

      Properties knownSequencesResource = new Properties();
      InputStream bufferedStream = new BufferedInputStream(KnownSequenceCountPlugin.getKnownSequencesStream());
      knownSequencesResource.load(bufferedStream);
      bufferedStream.close();
      for (Entry<Object, Object> entry : knownSequencesResource.entrySet()) {
        String key = (String) entry.getKey();
        if (key.startsWith("SingleJxnAdapt")) {
          KmerPositionPlugin kmerPositionPlugin = new KmerPositionPlugin();
          kmerPositionPlugin.setTitle(key + " Positions");
          kmerPositionPlugin.setSuffix(key);
          kmerPositionPlugin.setKmerToFind((String) entry.getValue());
          pluginsPerAccumulator.get(MetricAccumulationLevel.ALL_READS).get(ALL_READS_DEFAULT_KEY).add(kmerPositionPlugin);
        }
      }
    } else if (type == FileFormats.BAM) {
      for (int idx = 0; idx < read1s.size(); idx++) {
        totalFileSize += read1s.get(idx).length();

        BAMSequenceParser bamParser = new BAMSequenceParser(read1s.get(idx));
        fileParsers[idx] = bamParser;
        //bamParser.setDebug(true);

        pluginsPerAccumulator.get(MetricAccumulationLevel.ALL_READS).get(ALL_READS_DEFAULT_KEY).add(new ReadSoftclipsPlugin());
        if (perRG) {
          for (SAMReadGroupRecord rg : bamParser.getReadGroups()) {
            if (!pluginsPerAccumulator.get(MetricAccumulationLevel.READ_GROUP).containsKey(rg.getId())) {
              addPlugins(pluginsPerAccumulator, knownSequences, MetricAccumulationLevel.READ_GROUP, rg.getId());
              nbFragmentsPerAccumulator.get(MetricAccumulationLevel.READ_GROUP).put(rg.getId(), 0);
              pluginsPerAccumulator.get(MetricAccumulationLevel.READ_GROUP).get(rg.getId()).add(new ReadSoftclipsPlugin());
            }
            if (!pluginsPerAccumulator.get(MetricAccumulationLevel.LIBRARY).containsKey(rg.getLibrary())) {
              addPlugins(pluginsPerAccumulator, knownSequences, MetricAccumulationLevel.LIBRARY, rg.getLibrary());
              nbFragmentsPerAccumulator.get(MetricAccumulationLevel.LIBRARY).put(rg.getLibrary(), 0);
              pluginsPerAccumulator.get(MetricAccumulationLevel.READ_GROUP).get(rg.getLibrary()).add(new ReadSoftclipsPlugin());
            }
          }
        }
      }

      Properties knownSequencesResource = new Properties();
      InputStream bufferedStream = new BufferedInputStream(KnownSequenceCountPlugin.getKnownSequencesStream());
      knownSequencesResource.load(bufferedStream);
      bufferedStream.close();
      for (Entry<Object, Object> entry : knownSequencesResource.entrySet()) {
        String key = (String) entry.getKey();
        if (key.startsWith("SingleJxnAdapt")) {
          for (MetricAccumulationLevel level : pluginsPerAccumulator.keySet()) {
            for (String levelKey : pluginsPerAccumulator.get(level).keySet()) {
              List<IQCPlugin> plugins = pluginsPerAccumulator.get(level).get(levelKey);
              KmerPositionPlugin kmerPositionPlugin = new KmerPositionPlugin();
              kmerPositionPlugin.setTitle(key + " Positions");
              kmerPositionPlugin.setSuffix(key);
              kmerPositionPlugin.setKmerToFind((String) entry.getValue());
              plugins.add(kmerPositionPlugin);
            }
          }
        }
      }
    } else if (type == FileFormats.SFF) {
      for (int idx = 0; idx < read1s.size(); idx++) {
        totalFileSize += read1s.get(idx).length();
        fileParsers[idx] = new SFFFileParser(read1s.get(idx));
      }
      RunTrimmingPlugin runTrimmingPlugin = new RunTrimmingPlugin();
      pluginsPerAccumulator.get(MetricAccumulationLevel.ALL_READS).get(ALL_READS_DEFAULT_KEY).add(runTrimmingPlugin);

      Properties knownSequencesResource = new Properties();
      InputStream bufferedStream = new BufferedInputStream(KnownSequenceCountPlugin.getKnownSequencesStream());
      knownSequencesResource.load(bufferedStream);
      bufferedStream.close();
      for (Entry<Object, Object> entry : knownSequencesResource.entrySet()) {
        String key = (String) entry.getKey();
        if (key.startsWith("XLR_PairedLinker_")) {
          String suffix = key.substring(key.length() - 3, key.length());
          KmerPositionPlugin kmerPositionPlugin = new KmerPositionPlugin();
          kmerPositionPlugin.setTitle("Titanium " + suffix + " Linker Positions");
          kmerPositionPlugin.setSuffix(suffix);
          kmerPositionPlugin.setKmerToFind((String) entry.getValue());
          pluginsPerAccumulator.get(MetricAccumulationLevel.ALL_READS).get(ALL_READS_DEFAULT_KEY).add(kmerPositionPlugin);
        }
      }
    } else {
      throw new RuntimeException("Unsupported file type");
    }

    return totalFileSize;
  }
}
