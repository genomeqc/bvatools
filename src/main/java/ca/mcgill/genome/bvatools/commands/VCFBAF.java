/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import htsjdk.samtools.SAMSequenceDictionary;
import htsjdk.samtools.SAMSequenceRecord;
import htsjdk.samtools.util.IOUtil;
import htsjdk.tribble.Feature;
import htsjdk.tribble.SimpleFeature;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

import org.apache.commons.io.input.CountingInputStream;

import ca.mcgill.genome.bvatools.DefaultTool;
import ca.mcgill.genome.bvatools.graphs.DepthRatioBAlleleFreqPlot;
import ca.mcgill.genome.bvatools.util.SAMUtils;
import ca.mcgill.mcb.pcingola.fileIterator.VcfFileIterator;
import ca.mcgill.mcb.pcingola.util.Gpr;
import ca.mcgill.mcb.pcingola.vcf.VcfEntry;
import ca.mcgill.mcb.pcingola.vcf.VcfGenotype;
import ca.mcgill.mcb.pcingola.vcf.VcfHeader;

public class VCFBAF extends DefaultTool {
  private SAMSequenceDictionary referenceDictionary;
  private int width = 1280;
  private int height = 768;
  private boolean generateSVG = false;
  private Set<String> toExclude = new HashSet<String>();
  private File vcf = null;
  private String sampleName = null;
  private String lrrKey = "LRR";
  private String bafKey = "BAF";
  private String outputPrefix = null;

  @Override
  public String getCmdName() {
    return "vcfbaf";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("\t--vcf                   VCF file");
    System.out.println("\t--prefix                Output file prefix");
    System.out.println("\t--refdict               Reference dictionary");
    System.out.println("\t--width                 Plot width. (default: " + width + ")");
    System.out.println("\t--height                Plot height. (default: " + height + ")");
    System.out.println("\t--exclude               Chromosomes to exclude. Comma seperated");
    System.out.println("\t--svg                   Generate SVG of the plot. Can crash if plot is too big");
    System.out.println("\t--sampleName            Sample to use (default: first sample)");
    System.out.println("\t--lrrKey                Depth key name to use. Can be switched to AD to resemble Haplotype Caller or MuTect. default("+lrrKey+")");
    System.out.println("\t--bafKey                Depth key name to use. Can be switched to AD to resemble Haplotype Caller or MuTect. default("+bafKey+")");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {
    while (!arguments.isEmpty()) {
      String arg = arguments.pop();
      if (arg.equals("--vcf")) {
        vcf = new File(arguments.pop());
      } else if (arg.equals("--width")) {
        width = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--height")) {
        height = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--svg")) {
        generateSVG = true;
      } else if (arg.equals("--prefix")) {
        outputPrefix = arguments.pop();
      } else if (arg.equals("--refdict")) {
        referenceDictionary = SAMUtils.getSequenceDictionary(new File(arguments.pop()));
      } else if (arg.equals("--exclude")) {
        String extraArgs = arguments.pop();
        for (String chr : extraArgs.split(",")) {
          toExclude.add(chr);
        }
      } else if (arg.equals("--sampleName")) {
        sampleName = arguments.pop();
      } else if (arg.equals("--lrrKey")) {
        lrrKey = arguments.pop();
      } else if (arg.equals("--bafKey")) {
        bafKey = arguments.pop();
      } else {
        unusedArgs.add(arg);
      }
    }

    if (vcf == null) {
      printUsage("basefreq not set");
      return 1;
    }
    if (outputPrefix == null) {
      printUsage("prefix not set");
      return 1;
    }
    if (referenceDictionary == null) {
      printUsage("refdict is needed when generating plots");
      return 1;
    }
    return 0;
  }

  @Override
  public int run() {
    LinkedHashMap<String, Feature> genome = null;
    if (referenceDictionary != null) {
      genome = new LinkedHashMap<String, Feature>();
      for (SAMSequenceRecord seq : referenceDictionary.getSequences()) {
        if(!toExclude.contains(seq.getSequenceName())) {
          Feature chr = new SimpleFeature(seq.getSequenceName(), 1, seq.getSequenceLength());
          genome.put(chr.getChr(), chr);
        }
      }
    }

    computeLRRBAF(genome);

    return 0;
  }

  public void computeLRRBAF(LinkedHashMap<String, Feature> genome) {
    DepthRatioBAlleleFreqPlot chrPlot = null;
    chrPlot = new DepthRatioBAlleleFreqPlot(genome, width, height, generateSVG);

    try {
      CountingInputStream countingPosStream = new CountingInputStream(IOUtil.openFileForReading(vcf));
      try(BufferedReader vcfReader = new BufferedReader(new InputStreamReader(countingPosStream))) {
        VcfFileIterator vcfParser = new VcfFileIterator(vcfReader);
        int sampleIdx=0;
        if(sampleName != null) {
          VcfHeader header = vcfParser.readHeader();
          sampleIdx = -1;
          for(int i=0; i < header.getSampleNames().size(); i++) {
            if(sampleName.equals(header.getSampleNames().get(i))) {
              sampleIdx = i;
              break;
            }
          }
          if(sampleIdx == -1)
            throw new RuntimeException("Sample "+sampleName+" was not found in vcf");
        }
        
        long posFileLength = vcf.length();
        long prevPct=0;
        for (VcfEntry vcfEntry : vcfParser) {
          if(!vcfEntry.isSnp() || vcfEntry.getRef().indexOf('N') != -1 || vcfEntry.getAltsStr().indexOf('N') != -1) {
            continue;
          }
          if(!genome.containsKey(vcfEntry.getChromosomeName()))
            continue;
          
          VcfGenotype genotype = vcfEntry.getVcfGenotype(sampleIdx);
          double lrr = Gpr.parseDoubleSafe(genotype.get(lrrKey));
          double baf = Gpr.parseDoubleSafe(genotype.get(bafKey));
  
          long chromosomeSize = referenceDictionary.getSequence(vcfEntry.getChromosomeName()).getSequenceLength();
          if (vcfEntry.getStart() > chromosomeSize) {
            throw new RuntimeException("Position shoudn't be > than chrom size: " + vcfEntry.getStart() + " > " + chromosomeSize);
          }
          chrPlot.addPoint(DepthRatioBAlleleFreqPlot.DepthRatioBAF.DEPTH_RATIO, vcfEntry.getChromosomeName(), vcfEntry.getStart(), lrr);
          chrPlot.addPoint(DepthRatioBAlleleFreqPlot.DepthRatioBAF.BAF, vcfEntry.getChromosomeName(), vcfEntry.getStart(), baf);
  
          long pct = 100l*countingPosStream.getByteCount()/posFileLength;
          if(pct != prevPct) {
            prevPct = pct;
            System.err.print("\rCompletion: ");
            System.err.print(pct);
            System.err.print('%');
          }
        }
      }
      System.err.println("\rCompletion: 100%");

      chrPlot.write(outputPrefix);
    } catch(IOException e) {
      throw new RuntimeException(e);
    }
  }
}
