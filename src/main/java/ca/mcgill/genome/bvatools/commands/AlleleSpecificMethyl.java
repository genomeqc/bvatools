/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;

import ca.mcgill.genome.bvatools.Allele;
import ca.mcgill.genome.bvatools.CGSite;
import ca.mcgill.genome.bvatools.DefaultTool;
import ca.mcgill.genome.bvatools.HetSite;
import ca.mcgill.genome.bvatools.util.StrandBamFile;

import htsjdk.samtools.AlignmentBlock;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMRecordIterator;
import htsjdk.samtools.reference.IndexedFastaSequenceFile;
import htsjdk.samtools.reference.ReferenceSequence;
import htsjdk.samtools.util.CoordMath;

public class AlleleSpecificMethyl extends DefaultTool {

  private byte threads;
  private HashMap<Integer, Integer> tmpHetHash = new HashMap<Integer, Integer>();
  private File forwardBamFile1 = null;
  private File reverseBamFile1 = null;
  private File forwardBamFile2 = null;
  private File reverseBamFile2 = null;
  private File positions = null;
  private File referenceFasta = null;

  public AlleleSpecificMethyl() {
  }

  @Override
  public String getCmdName() {
    return "asm";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  @Override
  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("\t--pos           input position file in TSV");
    System.out.println("\t--fbam1          forward BAM file phase 1");
    System.out.println("\t--rbam1          reverse BAM file phase 1");
    System.out.println("\t--fbam2          forward BAM file phase 2");
    System.out.println("\t--rbam2          reverse BAM file phase 2");
    System.out.println("\t--ref           fasta reference file");
    System.out.println("\t--threads       number of threads to use. (default: " + threads + ")");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {

    while (!arguments.isEmpty()) {
      String arg = arguments.pop();
      switch (arg) {
      case "--pos":
        positions = new File(arguments.pop());
        break;
      case "--threads":
        threads = Byte.parseByte(arguments.pop());
        break;
      case "--fbam1":
        forwardBamFile1 = new File(arguments.pop());
        break;
      case "--rbam1":
        reverseBamFile1 = new File(arguments.pop());
        break;
      case "--fbam2":
        forwardBamFile2 = new File(arguments.pop());
        break;
      case "--rbam2":
        reverseBamFile2 = new File(arguments.pop());
        break;
      case "--ref":
        referenceFasta = new File(arguments.pop());
        break;
      default:
        unusedArgs.add(arg);
        break;
      }
    }

    if (forwardBamFile1 == null || reverseBamFile1 == null || forwardBamFile2 == null || reverseBamFile2 == null || referenceFasta == null) {
      printUsage("bam files not set");
      return 1;
    } else if (positions == null) {
      printUsage("pos not set");
      return 1;
    }

    return 0;
  }

  @Override
  public int run() {
    StrandBamFile forwardBamFile1SAMReader = new StrandBamFile("forward", forwardBamFile1, "first");
    StrandBamFile forwardBamFile2SAMReader = new StrandBamFile("forward", forwardBamFile2, "second");
    StrandBamFile reverseBamFile1SAMReader = new StrandBamFile("reverse", reverseBamFile1, "first");
    StrandBamFile reverseBamFile2SAMReader = new StrandBamFile("reverse", reverseBamFile2, "second");

    List<StrandBamFile> tmpList = Arrays.asList(forwardBamFile1SAMReader, forwardBamFile2SAMReader, reverseBamFile1SAMReader, reverseBamFile2SAMReader);

    for (StrandBamFile sfr : tmpList) {
      if (!sfr.getFileReader().hasIndex()) {
        throw new RuntimeException("The bam file " + sfr + " does not have an index");
      }
    }

    List<HetSite> positionsToTest = parsePositions(positions);
    IndexedFastaSequenceFile referenceFastaFile = null;
    try {
      referenceFastaFile = new IndexedFastaSequenceFile(referenceFasta);
    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    getAMR(positionsToTest, forwardBamFile1SAMReader, forwardBamFile2SAMReader, reverseBamFile1SAMReader, reverseBamFile2SAMReader, referenceFastaFile);
    return 0;
  }

  public List<HetSite> parsePositions(File positionsFile) {
    List<HetSite> positions = new ArrayList<>();
    try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(positionsFile), StandardCharsets.US_ASCII), 100 * 1024);) {
      String line;
      while ((line = reader.readLine()) != null) {
        String[] nextLine = line.split("\t");
        String chromosome = nextLine[0];
        int position = Integer.parseInt(nextLine[1]);
        char allele1 = nextLine[2].charAt(0);
        char allele2 = nextLine[3].charAt(0);
        positions.add(new HetSite(chromosome, position, allele1, allele2));
        tmpHetHash.put(position, 1);
        tmpHetHash.put(position - 1, 1);
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    return positions;
  }

  public void getAMR(List<HetSite> positionsToTest, StrandBamFile f1, StrandBamFile f2, StrandBamFile r1, StrandBamFile r2,
      IndexedFastaSequenceFile referenceFastaFile) {

    final int readLength = 100;
    HashMap<String, CGSite> globalCGSites = new HashMap<>();

    for (HetSite hetsite : positionsToTest) {

      String chromosome = hetsite.getChromosome();

      int position = hetsite.getPosition();
      // System.out.println(position);

      Character allele1 = hetsite.getAllele1();
      Character allele2 = hetsite.getAllele2();

      HashMap<String, CGSite> localCGSites = new HashMap<>();

      // Get CG positions from reference in readLength bp window around position
      ReferenceSequence sequence = referenceFastaFile.getSubsequenceAt(chromosome, position - readLength + 1, position + readLength - 1);
      String bases = new String(sequence.getBases(), StandardCharsets.US_ASCII);

      for (int i = 0; i < bases.length() - 1; i++) {
        Character a = bases.charAt(i);
        Character b = bases.charAt(i + 1);
        if (a.equals('C') && b.equals('G')) {
          int cgPosition = position - readLength + i + 1;

          if (tmpHetHash.containsKey(cgPosition)) {
            continue;
          }

          String hashKey = chromosome + "," + cgPosition;
          if (!globalCGSites.containsKey(hashKey)) {
            CGSite tmpSite = new CGSite(chromosome, position);
            tmpSite.allele1.setBase(allele1);
            tmpSite.allele2.setBase(allele2);
            localCGSites.put(hashKey, tmpSite);
            globalCGSites.put(hashKey, tmpSite);
          }
        }
      }

      List<StrandBamFile> SAMReaderList;
      SAMReaderList = Arrays.asList(f1, r1, f2, r2);
      // SAMReaderList = Arrays.asList(f1);
      // Get reads overlapping het site position
      try {
        for (StrandBamFile sfr : SAMReaderList) {
          SAMRecordIterator samIterator = null;

          samIterator = sfr.getFileReader().queryOverlapping(chromosome, position - 1, position - 1);
          // samIterator = sfr.getFileReader().queryOverlapping(chromosome, position, position);
          String bamOrder = sfr.getOrder();
          String bamStrand = sfr.getStrand();

          // For each read found overlapping het site

          while (samIterator.hasNext()) {

            SAMRecord record = samIterator.next();

            if (record.getReadUnmappedFlag() || record.getDuplicateReadFlag()) {
              continue;
            }

            String readName = record.getReadName();
            // Get alignment blocks for each read

            for (AlignmentBlock block : record.getAlignmentBlocks()) {
              // System.out.println(readName);
              // Look if the block overlaps a CG site
              int blockRefStart = block.getReferenceStart();
              int blockReadStart = block.getReadStart();
              int blockLength = block.getLength();
              int refOffsetHetSite = position - blockRefStart;
              int readBaseOffsetHetSite = blockReadStart - 1 + refOffsetHetSite;

              if (!(blockRefStart <= position && position <= CoordMath.getEnd(blockRefStart, blockLength))) {
                continue;
              }

              byte[] baseHetSite = record.getReadBases();
              // System.out.println(record.getReadString());
              // System.out.println((record.getReadString()).length());
              char baseHet = (char) baseHetSite[readBaseOffsetHetSite];
              // System.out.println("here");

              for (String key : localCGSites.keySet()) {

                int pos = Integer.parseInt(key.split(",")[1]);
                // System.out.println(pos);
                if (bamStrand.equals("reverse")) {
                  pos = pos + 1;
                }

                if (blockRefStart <= pos && pos <= CoordMath.getEnd(blockRefStart, blockLength)) { // block overlaps CG site
                  // System.out.println(readName);
                  CGSite cgSite = localCGSites.get(key);

                  int refOffset = pos - blockRefStart;
                  int readBaseOffset = blockReadStart - 1 + refOffset;
                  Character baseChar = null;
                  byte[] base = record.getReadBases();
                  baseChar = (char) base[readBaseOffset];
                  // Add if read is not already assigned to CG site
                  if (!cgSite.checkRead(readName, bamOrder, bamStrand)) {
                    if (baseHet == cgSite.allele1.getBase()) {
                      addCount(cgSite, "allele1", bamOrder, bamStrand, baseChar);
                    } else if (baseHet == cgSite.allele2.getBase()) {
                      addCount(cgSite, "allele2", bamOrder, bamStrand, baseChar);
                    }
                    cgSite.addRead(readName, bamOrder, bamStrand);
                  }
                }
              }
            }
          }
          samIterator.close();
        }
      } catch (RuntimeException e) {
        throw new RuntimeException("Problem reading file:" + e);
      }
    }
    printCGSites(globalCGSites);

  }

  public void addCount(CGSite cgsite, String allele, String bamOrder, String bamStrand, Character base) {

    if (allele.equals("allele1")) {
      if (bamOrder.equals("first")) {
        if (bamStrand.equals("forward")) {
          if (base == 'C') {
            cgsite.allele1.firstForwardCs++;
          } else if (base == 'T') {
            cgsite.allele1.firstForwardTs++;
          }
        } else if (bamStrand.equals("reverse")) {
          if (base == 'G') {
            cgsite.allele1.firstReverseCs++;
          } else if (base == 'A') {
            cgsite.allele1.firstReverseTs++;
          }
        }
      } else if (bamOrder.equals("second")) {
        if (bamStrand.equals("forward")) {
          if (base == 'C') {
            cgsite.allele1.secondForwardCs++;
          } else if (base == 'T') {
            cgsite.allele1.secondForwardTs++;
          }
        } else if (bamStrand.equals("reverse")) {
          if (base == 'G') {
            cgsite.allele1.secondReverseCs++;
          } else if (base == 'A') {
            cgsite.allele1.secondReverseTs++;
          }
        }
      }
    } else if (allele.equals("allele2")) {
      if (bamOrder.equals("first")) {
        if (bamStrand.equals("forward")) {
          if (base == 'C') {
            cgsite.allele2.firstForwardCs++;
          } else if (base == 'T') {
            cgsite.allele2.firstForwardTs++;
          }
        } else if (bamStrand.equals("reverse")) {
          if (base == 'G') {
            cgsite.allele2.firstReverseCs++;
          } else if (base == 'A') {
            cgsite.allele2.firstReverseTs++;
          }
        }
      } else if (bamOrder.equals("second")) {
        if (bamStrand.equals("forward")) {
          if (base == 'C') {
            cgsite.allele2.secondForwardCs++;
          } else if (base == 'T') {
            cgsite.allele2.secondForwardTs++;
          }
        } else if (bamStrand.equals("reverse")) {
          if (base == 'G') {
            cgsite.allele2.secondReverseCs++;
          } else if (base == 'A') {
            cgsite.allele2.secondReverseTs++;
          }
        }
      }
    }

  }

  public void printCGSites(HashMap<String, CGSite> globalCGSites) {

    System.out.println("chr,pos,a1,a2,cov_a1_first_forward,cov_a1_first_reverse,cov_a1_second_forward,cov_a1_second_reverse,cov_a2_first_forward,"
        + "cov_a2_first_reverse,cov_a2_second_forward,cov_a2_second_reverse,met_a1_first_forward,met_a1_first_reverse,met_a1_second_forward,"
        + "met_a1_second_reverse,met_a2_first_forward,met_a2_first_reverse,met_a2_second_forward,met_a2_second_reverse");

    for (String key : globalCGSites.keySet()) {
      String chr = key.split(",")[0];
      int pos = Integer.parseInt(key.split(",")[1]);
      CGSite cgsite = globalCGSites.get(key);
      Allele a1 = cgsite.allele1;
      Allele a2 = cgsite.allele2;

      double covFirstForwarda1 = a1.firstForwardTs + a1.firstForwardCs;
      double covFirstReversea1 = a1.firstReverseTs + a1.firstReverseCs;
      double covSecondForwarda1 = a1.secondForwardTs + a1.secondForwardCs;
      double covSecondReversea1 = a1.secondReverseTs + a1.secondReverseCs;
      double covFirstForwarda2 = a2.firstForwardTs + a2.firstForwardCs;
      double covFirstReversea2 = a2.firstReverseTs + a2.firstReverseCs;
      double covSecondForwarda2 = a2.secondForwardTs + a2.secondForwardCs;
      double covSecondReversea2 = a2.secondReverseTs + a2.secondReverseCs;

      double firstForwardMetha1 = -1;
      double firstReverseMetha1 = -1;
      double secondForwardMetha1 = -1;
      double secondReverseMetha1 = -1;
      double firstForwardMetha2 = -1;
      double firstReverseMetha2 = -1;
      double secondForwardMetha2 = -1;
      double secondReverseMetha2 = -1;

      if (covFirstForwarda1 != 0) {
        firstForwardMetha1 = (a1.firstForwardCs / (covFirstForwarda1)) * 100;
      }
      if (covFirstReversea1 != 0) {
        firstReverseMetha1 = (a1.firstReverseCs / (covFirstReversea1)) * 100;
      }
      if (covSecondForwarda1 != 0) {
        secondReverseMetha1 = (a1.secondForwardCs / (covSecondForwarda1)) * 100;
      }
      if (covSecondReversea1 != 0) {
        secondReverseMetha1 = (a1.secondReverseCs / (covSecondReversea1)) * 100;
      }

      if (covFirstForwarda2 != 0) {
        firstForwardMetha2 = (a2.firstForwardCs / covFirstForwarda2) * 100;
      }
      if (covFirstReversea2 != 0) {
        firstReverseMetha2 = (a2.firstReverseCs / covFirstReversea2) * 100;
      }
      if (covSecondForwarda2 != 0) {
        secondForwardMetha2 = (a2.secondForwardCs / covSecondForwarda2) * 100;
      }
      if (covSecondReversea2 != 0) {
        secondReverseMetha2 = (a2.secondReverseCs / covSecondReversea2) * 100;
      }

      firstForwardMetha1 = roundNumber(firstForwardMetha1);
      firstReverseMetha1 = roundNumber(firstReverseMetha1);
      secondReverseMetha1 = roundNumber(secondReverseMetha1);
      secondReverseMetha1 = roundNumber(secondReverseMetha1);
      firstForwardMetha2 = roundNumber(firstForwardMetha2);
      firstReverseMetha2 = roundNumber(firstReverseMetha2);
      secondForwardMetha2 = roundNumber(secondForwardMetha2);
      secondReverseMetha2 = roundNumber(secondReverseMetha2);

      String toPrint = chr + "," + pos + "," + a1.getBase() + "," + a2.getBase() + "," + (int) covFirstForwarda1 + "," + (int) covFirstReversea1 + ","
          + +(int) covSecondForwarda1 + "," + (int) covSecondReversea1 + "," + (int) covFirstForwarda2 + "," + (int) covFirstReversea2 + ","
          + (int) covSecondForwarda2 + "," + (int) covSecondReversea2 + "," + firstForwardMetha1 + "," + firstReverseMetha1 + "," + secondForwardMetha1 + ","
          + secondReverseMetha1 + "," + firstForwardMetha2 + "," + firstReverseMetha2 + "," + secondForwardMetha2 + "," + secondReverseMetha2;

      System.out.println(toPrint);
    }
  }

  public double roundNumber(double a) {
    return Math.round(a * 100) / 100.0;
  }
}
