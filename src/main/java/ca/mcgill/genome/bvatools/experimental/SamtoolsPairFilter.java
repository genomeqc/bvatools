/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.experimental;

import htsjdk.samtools.Defaults;
import htsjdk.samtools.util.IOUtil;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

import ca.mcgill.mcb.pcingola.fileIterator.VcfFileIterator;
import ca.mcgill.mcb.pcingola.vcf.VcfEntry;
import ca.mcgill.mcb.pcingola.vcf.VcfHeader;
import ca.mcgill.mcb.pcingola.vcf.VcfHeaderInfo;
import ca.mcgill.mcb.pcingola.vcf.VcfInfoType;

public class SamtoolsPairFilter {
  public static final String SOMATIC_FLAG = "SOMATIC";
  
  //private final String adKey;
  private final File vcf;
  //private final int minDP;
  private final float minQual;
  private final int minCLR;
  private final File out;
  
  public SamtoolsPairFilter(String adKey, File vcf, int minDP, float minQual, int minCLR, File out) {
    //this.adKey = adKey;
    this.vcf = vcf;
    //this.minDP = minDP;
    this.minQual = minQual;
    this.minCLR = minCLR;
    this.out = out;
  }
  
  public void run() throws UnsupportedEncodingException, FileNotFoundException {
    VcfFileIterator vcfParser = new VcfFileIterator(IOUtil.openFileForBufferedReading(vcf));

    PrintStream writer = null;
    try {
      writer = new PrintStream(new BufferedOutputStream(new FileOutputStream(out), Defaults.NON_ZERO_BUFFER_SIZE), false, "ASCII");
      VcfHeader header = vcfParser.readHeader();
      VcfHeaderInfo somaticInfo = new VcfHeaderInfo(SOMATIC_FLAG, VcfInfoType.Flag, "0", "Somatic event");
      header.addLine(somaticInfo.toString());
      VcfHeaderInfo LOHInfo = new VcfHeaderInfo("LOH", VcfInfoType.Flag, "0", "Somatic LOH");
      header.addLine(LOHInfo.toString());
      writer.println(header.toString());
      for (VcfEntry vcfEntry : vcfParser) {

        int clr = (int)vcfEntry.getInfoInt("CLR");
        if(clr >= minCLR && vcfEntry.getQuality() >= minQual && vcfEntry.getVcfGenotype(1).isVariant() && vcfEntry.getVcfGenotype(0).getGenotypeCode() != vcfEntry.getVcfGenotype(1).getGenotypeCode()) {
          if(vcfEntry.getVcfGenotype(0).isHeterozygous() && vcfEntry.getVcfGenotype(1).isHomozygous()) {
            vcfEntry.addInfo("LOH", null);
          }
          else {
            vcfEntry.addInfo(SOMATIC_FLAG, null);
          }
        }

        writer.println(vcfEntry);
      }
    }
    finally {
      writer.close();
    }
  }

  public static void main(String[] args) throws Exception{
    String key = args[0];
    File vcf = new File(args[1]);
    int minDP = Integer.parseInt(args[2]);
    float minQual = Float.parseFloat(args[3]);
    int minCLR = Integer.parseInt(args[4]);
    File out = new File(args[5]);
    SamtoolsPairFilter sve = new SamtoolsPairFilter(key, vcf, minDP, minQual, minCLR, out);
    sve.run();
  }
}
