/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.mutations;

import ca.mcgill.mcb.pcingola.interval.Marker;
import ca.mcgill.mcb.pcingola.interval.Markers;
import ca.mcgill.mcb.pcingola.interval.tree.IntervalForest;

public class Region {
  private final String name;
  private final IntervalForest forest;
  private long regionSize = 0;

  public Region(String name) {
    super();
    this.name = name;
    forest = new IntervalForest();
  }

  public void add(Marker interval) {
    // Markers markers = forest.query(interval);
    // if(markers.size() > 0) {
    // throw new RuntimeException("Interval already in region: " +interval.getId());
    // }
    regionSize += interval.size();
    forest.add(interval);
  }

  public String getName() {
    return name;
  }

  public long getRegionSize() {
    return regionSize;
  }

  public void build() {
    forest.build();
  }

  public boolean intersects(Marker interval) {
    Markers markers = forest.query(interval);
    return markers.size() > 0;
  }
}
