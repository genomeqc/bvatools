/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.util;

import gnu.trove.map.hash.TObjectDoubleHashMap;

import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.Set;

import ca.mcgill.mcb.pcingola.util.Gpr;

public class NucleotideAlleleCounts {
  private final static DecimalFormat doubleFormatter = new DecimalFormat("#0.00");

  private final String chromosome;
  private final int position;
  // Official alleles
  private SampleAlleleCounts.ReadBases alleleA;
  private SampleAlleleCounts.ReadBases alleleB;
  private int[] bamCounts;
  private int[] referenceBAMCounts;

  public NucleotideAlleleCounts(String chromosome, int position) {
    super();
    this.chromosome = chromosome;
    this.position = position;
  }

  public int getRefDepth() {
    int depth = 0;
    for (int baseCnt : referenceBAMCounts) {
      depth += baseCnt;
    }
    return depth;
  }

  public Set<String> getBasesAboveFraction(double fraction) {
    TObjectDoubleHashMap<String> alleleFractions = NucleotideAlleleCounts.computeAlleleFractions(getBamCounts());
    Set<String> keptBases = new HashSet<String>();
    for (String base : alleleFractions.keySet()) {
      if (alleleFractions.get(base) > fraction) {
        keptBases.add(base);
      }
    }
    return keptBases;
  }

  public int getDepth() {
    int depth = 0;
    for (int baseCnt : bamCounts) {
      depth += baseCnt;
    }
    return depth;
  }

  public StringBuilder format(boolean formatRef) {
    if (formatRef) {
      return formatCounts(referenceBAMCounts);
    } else {
      return formatCounts(bamCounts);
    }
  }

  public SampleAlleleCounts.ReadBases getAlleleA() {
    return alleleA;
  }

  public void setAlleleA(SampleAlleleCounts.ReadBases alleleA) {
    this.alleleA = alleleA;
  }

  public SampleAlleleCounts.ReadBases getAlleleB() {
    return alleleB;
  }

  public void setAlleleB(SampleAlleleCounts.ReadBases alleleB) {
    this.alleleB = alleleB;
  }

  private StringBuilder formatCounts(int []counts) {
    StringBuilder sb = new StringBuilder();
    boolean first = true;
    for (SampleAlleleCounts.ReadBases base : SampleAlleleCounts.ReadBases.values()) {
      if (first) {
        first = false;
      } else {
        sb.append(' ');
      }
      sb.append(base);
      sb.append(':');
      sb.append(counts[base.ordinal()]);
    }
    return sb;
  }

  public static StringBuilder formatFractionCounts(int []baseCounts) {
    StringBuilder sb = new StringBuilder();
    boolean first = true;
    for (SampleAlleleCounts.ReadBases base : SampleAlleleCounts.ReadBases.values()) {
      if (first) {
        first = false;
      } else {
        sb.append(' ');
      }
      sb.append(base);
      sb.append(':');
      sb.append(doubleFormatter.format(baseCounts[base.ordinal()]));
    }
    return sb;
  }

  public static TObjectDoubleHashMap<String> computeAlleleFractions(int baseCounts[]) {
    TObjectDoubleHashMap<String> retVal = new TObjectDoubleHashMap<String>();

    double totalBaseCount = 0;
    for (SampleAlleleCounts.ReadBases base : SampleAlleleCounts.ReadBases.values()) {
      totalBaseCount += baseCounts[base.ordinal()];
    }

    for (SampleAlleleCounts.ReadBases base : SampleAlleleCounts.ReadBases.values()) {
      if (baseCounts[base.ordinal()] == 0) {
        retVal.put(base.toString(), 0);
      } else {
        retVal.put(base.toString(), baseCounts[base.ordinal()] / totalBaseCount);
      }
    }
    return retVal;
  }

  public String getChromosome() {
    return chromosome;
  }

  public int getPosition() {
    return position;
  }

  public int[] getBamCounts() {
    return bamCounts;
  }

  public void setBamCounts(int []bamCounts) {
    this.bamCounts = bamCounts;
  }

  public int[] getReferenceBAMCounts() {
    return referenceBAMCounts;
  }

  public void setReferenceBAMCounts(int[] referenceBAMCounts) {
    this.referenceBAMCounts = referenceBAMCounts;
  }

  public static NucleotideAlleleCounts valueOf(String chr, int pos, String alleles) {
    NucleotideAlleleCounts retVal = new NucleotideAlleleCounts(chr, pos);
    retVal.setBamCounts(new int [SampleAlleleCounts.ReadBases.values().length]);
    String values[] = Gpr.split(alleles, ' ');
    for (String value : values) {
      int idx = value.indexOf(':');
      String nuc = value.substring(0, idx);
      String cntStr = value.substring(idx + 1);

      SampleAlleleCounts.ReadBases base = SampleAlleleCounts.ReadBases.valueOf(nuc);
      retVal.getBamCounts()[base.ordinal()] = Integer.valueOf(cntStr);
    }
    return retVal;
  }

  public static NucleotideAlleleCounts valueOf(String chr, int pos, String refAlleles, String alleles) {
    NucleotideAlleleCounts retVal = new NucleotideAlleleCounts(chr, pos);
    retVal.setBamCounts(new int [SampleAlleleCounts.ReadBases.values().length]);
    retVal.setReferenceBAMCounts(new int [SampleAlleleCounts.ReadBases.values().length]);

    String values[] = Gpr.split(alleles, ' ');
    for (String value : values) {
      int idx = value.indexOf(':');
      String nuc = value.substring(0, idx);
      String cntStr = value.substring(idx + 1);

      SampleAlleleCounts.ReadBases base = SampleAlleleCounts.ReadBases.valueOf(nuc);
      retVal.getBamCounts()[base.ordinal()] = Integer.valueOf(cntStr);
    }

    values = Gpr.split(refAlleles, ' ');
    for (String value : values) {
      int idx = value.indexOf(':');
      String nuc = value.substring(0, idx);
      String cntStr = value.substring(idx + 1);

      SampleAlleleCounts.ReadBases base = SampleAlleleCounts.ReadBases.valueOf(nuc);
      retVal.getReferenceBAMCounts()[base.ordinal()] = Integer.valueOf(cntStr);
    }
    return retVal;
  }
}
