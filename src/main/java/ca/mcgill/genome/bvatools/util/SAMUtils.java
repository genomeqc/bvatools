/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.util;

import htsjdk.samtools.BAMFileSpanUtils;
import htsjdk.samtools.SAMFileSpan;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMRecordIterator;
import htsjdk.samtools.SAMSequenceDictionary;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;
import htsjdk.samtools.util.CloseableIterator;
import htsjdk.samtools.util.CloserUtil;
import htsjdk.samtools.util.PeekableIterator;

import java.io.File;
import java.util.Collections;
import java.util.Comparator;

import ca.mcgill.mcb.pcingola.interval.Marker;
import ca.mcgill.mcb.pcingola.interval.Markers;

public class SAMUtils {
  /**
   * Jump to a location in a BAM and read until the end of the BAM.
   * The iterator is not only for a given chromosome it reads until the end
   * 
   * @param reader
   * @param chromosome
   * @param position
   * @return
   */
  public static CloseableIterator<SAMRecord> jumpToBAMLocation(SamReader reader, int chrIndex, int position) {
    SAMFileSpan iteratorSpan = BAMFileSpanUtils.makeSpanFollowing(reader, chrIndex, position); 
    
    if (iteratorSpan == null) {
      // Return an empty iterator
      return new CloseableIterator<SAMRecord>() {
        @Override
        public boolean hasNext() {
          return false;
        }

        @Override
        public SAMRecord next() {
          return null;
        }

        @Override
        public void remove() {
        }

        @Override
        public void close() {
        }};
    }

    SAMRecordIterator defaultIterator = reader.indexing().iterator(iteratorSpan);

    // Zoom to the exact entry since previous query might not be exactly on the right read.
    PeekableIterator<SAMRecord> recIter = new PeekableIterator<SAMRecord>(defaultIterator);
    while(recIter.hasNext()) {
      SAMRecord record = recIter.peek();
      if(record.getReferenceIndex() < chrIndex || (record.getReferenceIndex() == chrIndex && record.getAlignmentEnd() < position)) {
        recIter.next();
      }
      else {
        break;
      }
    }

    return recIter;
  }
  
  public static SAMSequenceDictionary getSequenceDictionary(final File dictionaryFile) {
    SamReaderFactory samReaderFactory = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
    SamReader samReader = samReaderFactory.open(dictionaryFile);
    final SAMSequenceDictionary dict = samReader.getFileHeader().getSequenceDictionary();
    CloserUtil.close(samReader);
    return dict;
  }
  
  public static byte[] reverseArray(byte [] array) {
    for (int i = 0; i < array.length / 2; i++) {
      byte temp = array[i];
      array[i] = array[array.length - 1 - i];
      array[array.length - 1 - i] = temp;
    }
    return array;
  }
  

  public static void sortMarkers(Markers intsSorted, final SAMSequenceDictionary dictionary) {
    intsSorted.getMarkers().sort((i1, i2) -> {
      // Compare chromosome
      int idx1 = dictionary.getSequenceIndex(i1.getChromosomeName());
      if (idx1 == -1) {
        throw new RuntimeException("Couldn't find: " + i1.getChromosomeName() + " in bam");
      }
      int idx2 = dictionary.getSequenceIndex(i2.getChromosomeName());
      if (idx2 == -1) {
        throw new RuntimeException("Couldn't find: " + i2.getChromosomeName() + " in bam");
      }
      int delta = idx1 - idx2;
      if (delta != 0) {
        return delta;
      }

      // Start
      if (i1.getStart() > i2.getStart()) {
        return 1;
      }
      if (i1.getStart() < i2.getStart()) {
        return -1;
      }

      // End
      if (i1.getEnd() > i2.getEnd()) {
        return 1;
      }
      if (i1.getEnd() < i2.getEnd()) {
        return -1;
      }
      return 0;
    });
  }
}
