/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.util;

public class Position {
  private final String chromosome;
  private final int position;
  private final byte referenceBase;
  
  public Position(String chromosome, int position, byte referenceBase) {
    super();
    this.chromosome = chromosome;
    this.position = position;
    this.referenceBase = referenceBase;
  }
  
  public String getChromosome() {
    return chromosome;
  }
  public int getPosition() {
    return position;
  }
  public byte getReferenceBase() {
    return referenceBase;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((chromosome == null) ? 0 : chromosome.hashCode());
    result = prime * result + position;
    result = prime * result + referenceBase;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Position other = (Position) obj;
    if (chromosome == null) {
      if (other.chromosome != null)
        return false;
    } else if (!chromosome.equals(other.chromosome))
      return false;
    if (position != other.position)
      return false;
    if (referenceBase != other.referenceBase)
      return false;
    return true;
  }

}
