/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.util;

import gnu.trove.list.array.TFloatArrayList;

public class GrowingFloatArrayList extends TFloatArrayList {
  private static final int DEFAULT_CAPACITY = 45;

  /**
   * Constructs an empty list with the specified initial capacity.
   * 
   * @throws IllegalArgumentException
   *           if the specified initial capacity is negative
   */
  public GrowingFloatArrayList() {
    super(DEFAULT_CAPACITY);
  }

  /**
   * Replaces the element at the specified position in this list with the specified element. Unlike ordinary
   * ArrayList.set the array automatically grows if index >= size.
   * 
   * @param index
   *          index of element to replace.
   * @param element
   *          element to be stored at the specified position.
   * 
   * @return the element previously at the specified position.
   * @throws IndexOutOfBoundsException
   *           if index out of range <tt>( index &lt; 0 )</tt>.
   */
  @Override
  public float set(int index, float element) {
    int size = this.size();
    if (index < size) {
      return super.set(index, element);
    } else {
      // Unfortunately for us:
      // super.set element must exist already.
      // add increases size by one, no more.
      // There is no setSize method.
      // setCapacity has no effect on the array size.
      // so we must painfully grow the array one element at a time.
      int dummiesRequired = index - size;
      for (; dummiesRequired > 0; dummiesRequired--) {
        super.add(0f);
      }
      super.add(element);
    }
    return -1;
  }

  @Override
  public float get(int index) {
    if (index < this.size()) {
      return super.get(index);
    } else {
      return 0l;
    }
  }
}
