/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.homology;

import htsjdk.samtools.AlignmentBlock;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMSequenceDictionary;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;
import htsjdk.samtools.util.CloseableIterator;
import htsjdk.samtools.util.CoordMath;

import java.io.File;
import java.io.IOException;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.List;
import java.util.Map;

import ca.mcgill.genome.bvatools.util.AlleleCounts;
import ca.mcgill.genome.bvatools.util.Position;
import ca.mcgill.genome.bvatools.util.SAMUtils;

public class AlleleFrequencyComputer implements Runnable, UncaughtExceptionHandler {
  private final File bamFile;
  private final List<Position> positions;
  private final boolean perRG;
  private final boolean useDups;
  private final Map<String, AlleleCounts> allCounts;
  private final Map<String, Map<String, AlleleCounts>> rgCounts;
  private final int minMappingQuality;
  private final int minBaseQuality;
  private Throwable caughtException = null;

  public AlleleFrequencyComputer(File bamFile, List<Position> positions, boolean perRG, boolean useDups, Map<String, AlleleCounts> allCounts, Map<String, Map<String, AlleleCounts>> rgCounts, int minMappingQuality, int minBaseQuality) {
    this.bamFile = bamFile;
    this.positions = positions;
    this.perRG = perRG;
    this.useDups = useDups;
    this.allCounts = allCounts;
    this.rgCounts = rgCounts;
    this.minMappingQuality = minMappingQuality;
    this.minBaseQuality = minBaseQuality;
  }

  @Override
  public void uncaughtException(Thread t, Throwable e) {
    caughtException = e;
    System.err.println("UncaughtException on thread ["+t.getName()+": "+t.getId()+"]");
  }

  public Throwable getCaughtException() {
    return caughtException;
  }

  @Override
  public void run() {
    // CACHE_FILE_BASED_INDEXES Important to seek
    SamReaderFactory samReaderFactory = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT).enable(SamReaderFactory.Option.CACHE_FILE_BASED_INDEXES);
    
    try (SamReader samReader = samReaderFactory.open(bamFile)) {
      SAMSequenceDictionary dictionary = samReader.getFileHeader().getSequenceDictionary();
  
      int currentIdx = 0;
      CloseableIterator<SAMRecord> recIter = null;
      int chrIdx = dictionary.getSequenceIndex(positions.get(0).getChromosome());
      recIter = SAMUtils.jumpToBAMLocation(samReader, chrIdx, positions.get(0).getPosition());
      while (recIter.hasNext()) {
        SAMRecord record = recIter.next();
        if (record.getReadUnmappedFlag() || record.getMappingQuality() < minMappingQuality)
          continue;
        if(!useDups && record.getDuplicateReadFlag())
          continue;
  
        int start = record.getAlignmentStart();
        int end = record.getAlignmentEnd();
  
        boolean skipRecord = false;
        while (currentIdx < positions.size()) {
          if (!record.getReferenceName().equals(positions.get(currentIdx).getChromosome())) {
            if (record.getReferenceIndex().intValue() < dictionary.getSequenceIndex(positions.get(currentIdx).getChromosome())) {
              skipRecord = true;
              break;
            } else {
  
            }
          } else if (positions.get(currentIdx).getPosition() >= start) {
            break;
          }
          
          currentIdx++;
        }
        if (currentIdx >= positions.size())
          break;
        if (skipRecord)
          continue;
  
        if (positions.get(currentIdx).getPosition() >= start && positions.get(currentIdx).getPosition() <= end) {
          for (int tmpIdx = currentIdx; tmpIdx < positions.size() && record.getReferenceName().equals(positions.get(tmpIdx).getChromosome()); tmpIdx++) {
            Position position = positions.get(tmpIdx);
  
            if (position.getPosition() <= end) {
              for (AlignmentBlock block : record.getAlignmentBlocks()) {
                if (block.getReferenceStart() <= position.getPosition() && position.getPosition() <= CoordMath.getEnd(block.getReferenceStart(), block.getLength())) {
                  int refOffset = position.getPosition() - block.getReferenceStart();
                  int readBaseOffset = block.getReadStart() - 1 + refOffset;
                  if (record.getBaseQualities()[readBaseOffset] >= minBaseQuality) {
                    byte bases[] = record.getReadBases();
                    String baseStr = String.valueOf((char) bases[readBaseOffset]);
                    
                    String key = position.getChromosome()+'#'+position.getPosition();
                    
                    allCounts.get(key).getBamCounts().put(baseStr, (allCounts.get(key).getBamCounts().get(baseStr) + 1));
                    if(perRG) {
                      Map<String, AlleleCounts> rgCount = rgCounts.get(record.getReadGroup().getId());
                      rgCount.get(key).getBamCounts().put(baseStr, (rgCount.get(key).getBamCounts().get(baseStr) + 1));
                    }
                  }
                  break;
                }
              }
            } else {
              break;
            }
          }
        }
      }
      recIter.close();
    } // try
    catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
