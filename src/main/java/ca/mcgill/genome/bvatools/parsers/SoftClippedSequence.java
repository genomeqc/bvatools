/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.parsers;

import java.util.Arrays;

public class SoftClippedSequence extends Read {
  /**
   * At what index the good sequence starts This is not a length, it's an index.
   */
  private final short clipQualLeft;
  /**
   * At what index the good sequence stops. This is not a length, it's an index. If my sequence is 500bp long and I what to trim the last 490, clipQualRight will be == to 10, not
   * 490.
   */
  private final short clipQualRight;
  private final short clipAdapterLeft;
  private final short clipAdapterRight;
  private final String unTrimmedSequence;
  private final byte[] unTrimmedBaseQualities;

  public SoftClippedSequence(String header, String unTrimmedSequence, byte[] unTrimmedBaseQualities, short clipQualLeft, short clipQualRight, short clipAdapterLeft, short clipAdapterRight) {
    super(header);
    int seqLength = unTrimmedSequence.length();
    int trimLeft = Math.max(1, Math.max(clipQualLeft, clipAdapterLeft)) - 1;
    int trimRight = Math.min((clipQualRight == 0 ? seqLength : clipQualRight), (clipAdapterRight == 0 ? seqLength : clipAdapterRight));

    setSequence(unTrimmedSequence.substring(trimLeft, trimRight));
    setBaseQualities(Arrays.copyOfRange(unTrimmedBaseQualities, trimLeft, trimRight));

    this.unTrimmedSequence = unTrimmedSequence;
    this.unTrimmedBaseQualities = unTrimmedBaseQualities;
    this.clipQualLeft = clipQualLeft;
    this.clipQualRight = clipQualRight;
    this.clipAdapterLeft = clipAdapterLeft;
    this.clipAdapterRight = clipAdapterRight;
  }

  public short getClipQualLeft() {
    return clipQualLeft;
  }

  public short getClipQualRight() {
    return clipQualRight;
  }

  public short getClipAdapterLeft() {
    return clipAdapterLeft;
  }

  public short getClipAdapterRight() {
    return clipAdapterRight;
  }

  public String getUnTrimmedSequence() {
    return unTrimmedSequence;
  }

  public byte[] getUnTrimmedBaseQualities() {
    return unTrimmedBaseQualities;
  }

}
