/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.parsers;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.nio.charset.Charset;

import org.apache.commons.io.input.CountingInputStream;
import org.obiba.bitwise.client.LargeGZIPInputStream;

public class FASTQFileParser extends SequenceFileParser {
  private static final Charset ASCII = Charset.forName("ASCII");
  private static final int BUFFER_SIZE = 128 * 1024;
  private final byte qualityOffset;

  private final CountingInputStream counter;
  private final LineNumberReader sequenceReader;

  public FASTQFileParser(File fastq, byte qualityOffset) {
    super();
    this.qualityOffset = qualityOffset;
    try {
      counter = new CountingInputStream(new BufferedInputStream(new FileInputStream(fastq), BUFFER_SIZE));
      InputStream readFrom = counter;
      if (fastq.getName().toLowerCase().endsWith(".gz")) {
        readFrom = new LargeGZIPInputStream(counter);
      }
      sequenceReader = new LineNumberReader(new InputStreamReader(readFrom, ASCII));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * This method is not precise. It is at most a BUFFER_SIZE off.
   * @return Number of bytes read
   */
  @Override
  public long getByteCount() {
    return counter.getByteCount();
  }

  @Override
  public Fragment nextSequence() throws IOException {
    String header = sequenceReader.readLine();
    if (header == null)
      return null;
    // Remove first character
    header = header.substring(1);

    String sequence = sequenceReader.readLine();
    // Quality header
    sequenceReader.readLine();
    byte encodedQual[] = sequenceReader.readLine().getBytes(ASCII);
    byte baseQualities[] = new byte[encodedQual.length];
    for (int i = 0; i < baseQualities.length; i++) {
      baseQualities[i] = (byte) (encodedQual[i] - qualityOffset);
    }
    Read read = new Read(header, sequence, baseQualities);
    return new Fragment(header, null, null, read);
  }

  @Override
  public void close() throws IOException {
    sequenceReader.close();
  }

}
