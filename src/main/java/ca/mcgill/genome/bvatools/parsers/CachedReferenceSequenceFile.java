/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.parsers;

import htsjdk.samtools.reference.FastaSequenceFile;
import htsjdk.samtools.reference.ReferenceSequence;
import htsjdk.samtools.reference.ReferenceSequenceFile;
import htsjdk.samtools.SAMSequenceDictionary;

import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class CachedReferenceSequenceFile implements ReferenceSequenceFile, Closeable {
  private Map<String, ReferenceSequence> chr2sequences;
  private Iterator<ReferenceSequence> iterator;
  private SAMSequenceDictionary seqDict;
  
  public CachedReferenceSequenceFile(File file) throws FileNotFoundException {
    chr2sequences = new HashMap<String, ReferenceSequence>();
    FastaSequenceFile seqFile = new FastaSequenceFile(file, true);
    while(true) {
      ReferenceSequence refSeq = seqFile.nextSequence();
      if(refSeq == null)
        break;
      chr2sequences.put(refSeq.getName(), refSeq);
    }
    seqDict = seqFile.getSequenceDictionary();
    seqFile.close();

    reset();
  }

  @Override
  public ReferenceSequence getSequence(String contig) {
    ReferenceSequence seq = chr2sequences.get(contig);
    return seq;
  }

  @Override
  public ReferenceSequence getSubsequenceAt(String contig, long start, long stop) {
    ReferenceSequence seq = getSequence(contig);
    byte dest[] = new byte[(int)(stop-start+1)];
    
    System.arraycopy(seq.getBases(), (int)start-1, dest, 0, dest.length);
    return new ReferenceSequence( contig, seq.getContigIndex(), dest);
  }

  @Override
  public SAMSequenceDictionary getSequenceDictionary() {
    return seqDict;
  }

  @Override
  public ReferenceSequence nextSequence() {
    if(iterator.hasNext())
      return iterator.next();
    else
      return null;
  }

  @Override
  public void reset() {
    iterator = chr2sequences.values().iterator();
  }

  @Override
  public boolean isIndexed() {
    return true;
  }

  @Override
  public void close() throws IOException {
  }
}
