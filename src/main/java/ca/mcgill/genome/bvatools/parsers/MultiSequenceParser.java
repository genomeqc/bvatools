/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 
 */
package ca.mcgill.genome.bvatools.parsers;

import java.io.IOException;


/**
 *
 */
public class MultiSequenceParser extends SequenceFileParser {
  private SequenceFileParser sequenceFileParsers[];
  private int parserIdx;

  public MultiSequenceParser(SequenceFileParser sequenceFileParsers[]) {
    this.sequenceFileParsers = sequenceFileParsers;
    this.parserIdx = 0;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Fragment nextSequence() throws IOException {
    Fragment retVal = sequenceFileParsers[parserIdx].nextSequence();
    while(retVal == null) {
      parserIdx++;
      if(parserIdx >= sequenceFileParsers.length) {
        break;
      }
      retVal = sequenceFileParsers[parserIdx].nextSequence();
    }
    return retVal;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public long getByteCount() {
    long retVal = 0;
    for(int idx=0; idx <= parserIdx; idx++) {
      retVal += sequenceFileParsers[idx].getByteCount();
    }
    return retVal;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void close() throws IOException {
    IOException e = null;
    int exceptions = 0;
    for(SequenceFileParser sequenceFileParser : sequenceFileParsers) {
      try {
        sequenceFileParser.close();
      }
      catch(IOException ex) {
        e = ex;
        exceptions++;
      }
    }

    if(e != null) {
      throw new IOException("Multiple Exceptions ["+exceptions+"] occured during the close. Only the last one is showed", e);
    }
  }
}
