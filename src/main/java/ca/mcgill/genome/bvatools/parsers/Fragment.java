/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.parsers;

import java.util.ArrayList;
import java.util.Iterator;

import ca.mcgill.genome.bvatools.util.ReadOnlyIterator;

public class Fragment implements Iterable<Read>{
  private String name;
  private final String readGroup;
  private final String library;
  private ArrayList<Read> reads;
  private byte avgQuality = -1;

  public Fragment(String name, String readGroup, String library) {
    this.name = name;
    this.readGroup = readGroup;
    this.library = library;
    reads = new ArrayList<Read>();
  }

  public Fragment(String name, String readGroup, String library, Read read) {
    this(name, readGroup, library);
    reads.add(read);
  }

  @Override
  public Iterator<Read> iterator() {
    return new ReadOnlyIterator<Read>(reads.iterator());
  }

  public String getName() {
    return name;
  }

  public String getReadGroup() {
    return readGroup;
  }

  public String getLibrary() {
    return library;
  }

  /**
   * Get the number of reads.
   * @param idx
   * @return
   */
  public int size() {
    return reads.size();
  }

  public void insertRead(int idx, Read read) {
    // invalid qual.
    avgQuality = -1;
    while(idx >= reads.size()) {
      reads.add(null);
    }
    reads.set(idx, read);
  }

  public Read getRead(int idx) {
    return reads.get(idx);
  }

  public byte getAvgQuality() {
    if(avgQuality == -1) {
      int qualitySum = 0;
      int nbBases = 0;
      for(Read read : reads) {
        if(read != null) {
          for(int idx=0; idx < read.getBaseQualities().length; idx++) {
            nbBases++;
            qualitySum += read.getBaseQualities()[idx];
          }
        }
      }
      avgQuality = (byte)(qualitySum/nbBases);
    }
    return avgQuality;
  }

}
