/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.parsers;

import htsjdk.samtools.Cigar;
import htsjdk.samtools.Defaults;
import htsjdk.samtools.SAMReadGroupRecord;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMRecordIterator;
import htsjdk.samtools.SamInputResource;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;
import htsjdk.samtools.util.SequenceUtil;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.input.CountingInputStream;

import ca.mcgill.genome.bvatools.util.SAMUtils;

public class BAMSequenceParser extends SequenceFileParser {
  private CountingInputStream countStream;
  private SamReader reader;
  private SAMRecordIterator recIter;
  private Map<String, Fragment> readCache = new HashMap<String, Fragment>();
  private boolean debug = false;
  
  public BAMSequenceParser(File bam) {
    try {
      countStream = new CountingInputStream(new FileInputStream(bam));
    } catch (FileNotFoundException e) {
      throw new RuntimeException(e);
    }
    SamReaderFactory samReaderFactory = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
    this.reader = samReaderFactory.open(SamInputResource.of(new BufferedInputStream(countStream, Defaults.NON_ZERO_BUFFER_SIZE)));
    this.recIter = reader.iterator();
  }

  public boolean isDebug() {
    return debug;
  }

  public void setDebug(boolean debug) {
    this.debug = debug;
  }

  public List<SAMReadGroupRecord> getReadGroups() {
    return reader.getFileHeader().getReadGroups();
  }
  
  @Override
  public Fragment nextSequence() throws IOException {
    while(recIter.hasNext()) {
      SAMRecord record = recIter.next();
      if(record.getSupplementaryAlignmentFlag() || record.getNotPrimaryAlignmentFlag())
        continue;

      if(!record.getReadPairedFlag()) {
        BAMRead read = generateBAMRead(record);
        return new Fragment(read.getHeader(), record.getReadGroup().getId(), record.getReadGroup().getLibrary(), read);
      }
      else {
        if(readCache.containsKey(record.getReadName())) {
          Fragment fragment = readCache.remove(record.getReadName());
          fillFragment(record, fragment);
          if(debug)
            System.err.println("Remaining cached: "+readCache.size());
          return fragment;
        }
        else {
          Fragment fragment = new Fragment(record.getReadName(), record.getReadGroup().getId(), record.getReadGroup().getLibrary());
          fillFragment(record, fragment);
          readCache.put(fragment.getName(), fragment);
        }
      }
    }
    
    if(!recIter.hasNext() && readCache.size() > 0) {
      String readName = readCache.keySet().iterator().next();
      Fragment fragment = readCache.remove(readName);
      return fragment;
    }
    return null;
  }

  private void fillFragment(SAMRecord record, Fragment fragment) {
    BAMRead read = generateBAMRead(record);;
    if(record.getFirstOfPairFlag()) {
      fragment.insertRead(0, read);
    }
    else {
      fragment.insertRead(1, read);
    }
  }

  @Override
  public long getByteCount() {
    return countStream.getByteCount();
  }

  @Override
  public void close() throws IOException {
    if(reader!= null) {
      recIter.close();
      reader.close();
    }
    recIter = null;
    reader = null;
  }
  
  private BAMRead generateBAMRead(SAMRecord record) {
    Cigar cigar = record.getCigar();
    if(record.getReadUnmappedFlag())
      cigar = null;

    BAMRead read;
    if(record.getReadNegativeStrandFlag())
      read = new BAMRead(record.getReadName(), SequenceUtil.reverseComplement(record.getReadString()), SAMUtils.reverseArray(record.getBaseQualities()), cigar, true);
    else
      read = new BAMRead(record.getReadName(), record.getReadString(), record.getBaseQualities(), cigar, false);
    
    return read;
  }
}
