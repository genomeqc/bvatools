/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.parsers;

public class Read {
  private final String header;
  private String sequence;
  private byte[] baseQualities;
  
  public Read(String header) {
    this.header = header;
  }

  public Read(String header, String sequence, byte baseQualities[]) {
    this(header);
    this.sequence = sequence;
    this.baseQualities = baseQualities;
  }

  public int length() {
    return baseQualities.length;
  }

  public String getSequence() {
    return sequence;
  }

  public void setSequence(String sequence) {
    this.sequence = sequence;
  }

  public byte[] getBaseQualities() {
    return baseQualities;
  }

  public void setBaseQualities(byte[] baseQualities) {
    this.baseQualities = baseQualities;
  }

  public String getHeader() {
    return header;
  }

}
