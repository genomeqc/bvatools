/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.parsers;

import htsjdk.samtools.Cigar;

public class BAMRead extends Read {
	private final Cigar cigar;
	private final boolean wasReversed;
	
	public BAMRead(String header, String sequence, byte[] baseQualities, Cigar cigar, boolean wasReversed) {
		super(header, sequence, baseQualities);
		this.cigar = cigar;
		this.wasReversed = wasReversed;
	}

//	public BAMRead(String header, boolean wasReversed) {
//		super(header);
//		this.wasReversed = wasReversed;
//	}

	public Cigar getCigar() {
		return cigar;
	}

  public boolean isWasReversed() {
    return wasReversed;
  }

}
