/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.parsers;

import java.io.File;
import java.io.IOException;

/**
 *
 */
public class FASTQPEFileParser extends SequenceFileParser {
  public enum FastqFormat { AUTO, CASAVA1_8, CASAVA1_6, NO_PAIR_ENCODING, IGNORE };

  private FASTQFileParser parser1;
  private FASTQFileParser parser2;
  private FastqFormat format;

  public FASTQPEFileParser(File fastq1, File fastq2, byte qualityOffset, FastqFormat format) {
    super();
    this.format = format;
    parser1 = new FASTQFileParser(fastq1, qualityOffset);
    parser2 = new FASTQFileParser(fastq2, qualityOffset);
  }

  @Override
  public Fragment nextSequence() throws IOException {
    Fragment sequence1 = parser1.nextSequence();
    Fragment sequence2 = parser2.nextSequence();

    if (sequence1 == null)
      return null;

    // CASAVA 1.6:
    // @HWUSI-EAS100R:6:73:941:1973#0/1
    // @HWUSI-EAS100R:6:73:941:1973#0/2

    // CASAVA 1.8:
    // @EAS139:136:FC706VJ:2:2104:15343:197393 1:Y:18:ATCACG
    // @EAS139:136:FC706VJ:2:2104:15343:197393 2:Y:18:ATCACG

    String header1 = sequence1.getName();
    String header2 = sequence2.getName();
    if(format == FastqFormat.AUTO) {
      if (header1.trim().contains(" ") && header2.trim().contains(" ")) {
        format = FastqFormat.CASAVA1_8;
      }
      else if ((header1.endsWith("/1") || header1.endsWith("/2")) && (header2.endsWith("/1") || header2.endsWith("/2"))) {
        format = FastqFormat.CASAVA1_6;
      }
      else {
        format = FastqFormat.NO_PAIR_ENCODING;
      }
    }
    
    if (format == FastqFormat.CASAVA1_8) {
      // CASAVA 1.8
      header1 = header1.substring(0, header1.indexOf(' '));
      header2 = header2.substring(0, header2.indexOf(' '));
    } else if (format == FastqFormat.CASAVA1_6) {
      // CASAVA 1.6
      header1 = header1.substring(0, sequence1.getName().length() - 2);
      header2 = header2.substring(0, sequence2.getName().length() - 2);
    }

    if (format != FastqFormat.IGNORE && !header1.equals(header2)) {
     throw new RuntimeException("The 2 fastq files don't have the same ordering of reads. " + header1 + " vs " + header2);
    }


    Fragment frag = new Fragment(sequence1.getName(), null, null, sequence1.getRead(0));
    frag.insertRead(1, sequence2.getRead(0));

    return frag;
  }

  @Override
  public long getByteCount() {
    return parser1.getByteCount() + parser2.getByteCount();
  }

  @Override
  public void close() throws IOException {
    IOException ioException = null;
    try {
      parser1.close();
    } catch (IOException e) {
      ioException = e;
    }
    try {
      parser2.close();
    } catch (IOException e) {
      ioException = e;
    }

    if (ioException != null) {
      throw new IOException("One or both of the parsers had an exception.", ioException);
    }
  }

}
