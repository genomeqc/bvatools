/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.extractSoftClip;

import htsjdk.samtools.Cigar;
import htsjdk.samtools.CigarElement;
import htsjdk.samtools.CigarOperator;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMRecordIterator;
import htsjdk.samtools.SAMSequenceDictionary;
import htsjdk.samtools.SAMSequenceRecord;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicBoolean;

public class ProcessSoftClip implements Runnable {
  private final File bam;
  private final int flank;
  private final int maxReadLength;
  private final int minMappingQuality;
  private final Queue<PotentialSoftClip> softClipsToProcess;
  private final AtomicBoolean stopProcessing;

  public ProcessSoftClip(File bam, Queue<PotentialSoftClip> softClipsToProcess, int flank, int minMappingQuality, int maxReadLength) {
    this.bam = bam;
    this.flank = flank;
    this.maxReadLength = maxReadLength;
    this.minMappingQuality = minMappingQuality;
    this.softClipsToProcess = softClipsToProcess;

    stopProcessing = new AtomicBoolean(false);
  }

  private void cleanStack(SAMSequenceDictionary dictionary, PotentialSoftClip scToProcess, Queue<SAMRecord> stackedRecords) {
    Iterator<SAMRecord> iter = stackedRecords.iterator();
    SAMSequenceRecord seqRec = dictionary.getSequence(scToProcess.getChr());
    if (stackedRecords.size() > 0) {
      if (seqRec.getSequenceIndex() > stackedRecords.peek().getReferenceIndex()) {
        stackedRecords.clear();
      } else {
        while (iter.hasNext()) {
          SAMRecord record = iter.next();

          // In case we have a read with 2 softclips 10S50M23S, we need to buffer further dowstream, at least readLength
          if (record.getAlignmentEnd() >= (scToProcess.getPos() - flank - maxReadLength)) {
            break;
          } else {
            iter.remove();
          }
        }
      }
    }

  }

  public void stopProcessing() {
    stopProcessing.set(true);
  }

  @Override
  public void run() {
    SAMRecordIterator recIter = null;
    Queue<SAMRecord> stackedRecords = new LinkedList<SAMRecord>();

    SamReaderFactory samReaderFactory = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
    try (SamReader reader = samReaderFactory.open(bam)){
      SAMSequenceDictionary dictionary = reader.getFileHeader().getSequenceDictionary();
      recIter = reader.iterator();
      while (!stopProcessing.get()) {
        PotentialSoftClip scToProcess = softClipsToProcess.poll();
        if (scToProcess != null) {
          cleanStack(dictionary, scToProcess, stackedRecords);
          SAMSequenceRecord scToProcessSeqRec = dictionary.getSequence(scToProcess.getChr());
          for (SAMRecord record : stackedRecords) {
            if (scToProcessSeqRec.getSequenceIndex() != record.getReferenceIndex() || record.getAlignmentEnd() < (scToProcess.getPos() - flank)) {
              continue;
            }
            processRecord(record, scToProcess);
          }

          while (recIter.hasNext()) {
            SAMRecord record = recIter.next();
            if (record.getReadUnmappedFlag() || record.getDuplicateReadFlag() || record.getMappingQuality() < minMappingQuality)
              continue;

            int seqIdx = dictionary.getSequenceIndex(scToProcess.getChr());
            if (record.getReferenceIndex() < seqIdx) {
              continue;
            } else if (record.getReferenceIndex() > seqIdx
                || (record.getReferenceIndex() == seqIdx && record.getAlignmentStart() > (scToProcess.getPos() + flank))) {
              stackedRecords.add(record);
              break;
            }

            if (record.getAlignmentEnd() < (scToProcess.getPos() - flank)) {
              continue;
            }

            stackedRecords.add(record);

            processRecord(record, scToProcess);
          }
          scToProcess.doneProcessing();
        } else {
          try {
            Thread.sleep(1000);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    } finally {
      if (recIter != null) {
        recIter.close();
      }
    }
  }

  private void processRecord(SAMRecord record, PotentialSoftClip scToProcess) {
    if (record.getMateUnmappedFlag()) {
      scToProcess.getOneEndAligneds().add(record.getReadName());
    }
    if (record.getMateReferenceName() != record.getReferenceName()) {
      scToProcess.getTranslocatedRead().add(record.getReadName());
    }

    if (record.getAlignmentStart() <= scToProcess.getPos() && record.getAlignmentEnd() >= scToProcess.getPos())
      scToProcess.incrementNbReads();

    if (record.getCigarString().indexOf("S") != -1) {
      Cigar cigar = record.getCigar();
      int totalLength = 0;
      List<CigarElement> cigarElements = cigar.getCigarElements();
      for (int idx = 0; idx < cigar.getCigarElements().size(); idx++) {
        CigarElement cigarElement = cigarElements.get(idx);
        CigarOperator operator = cigarElement.getOperator();
        if (operator.equals(CigarOperator.S)) {
          int scPos = record.getReferencePositionAtReadPosition(totalLength);
          if (idx == 0) {
            scPos = record.getReferencePositionAtReadPosition(cigarElement.getLength() + 1);
          }

          if (scPos == scToProcess.getPos()) {
            byte seq[] = Arrays.copyOfRange(record.getReadBases(), totalLength, totalLength + cigarElement.getLength());
            byte qual[] = Arrays.copyOfRange(record.getBaseQualities(), totalLength, totalLength + cigarElement.getLength());
            scToProcess.getReadsToOutput().add(new SoftClippedRead(record.getReadName(), record.getFirstOfPairFlag(), seq, qual));
            break;
          }
        }

        if (operator.consumesReadBases())
          totalLength += cigarElement.getLength();
      }
    }
  }

}
