/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.extractSoftClip;

import htsjdk.samtools.Cigar;
import htsjdk.samtools.CigarElement;
import htsjdk.samtools.CigarOperator;
import htsjdk.samtools.Defaults;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SamInputResource;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import org.apache.commons.io.input.CountingInputStream;

import ca.mcgill.genome.bvatools.util.MemoryUtils;

public class SoftClipExtractor implements Runnable {
  private final File bam;
  private final int minMappingQuality;
  private boolean printElapsed = false;
  private final Queue<PotentialSoftClip> softClipsToProcess;
  private final Queue<PotentialSoftClip> softClips;
  private long nbSoftClipsFound;

  public SoftClipExtractor(Queue<PotentialSoftClip> softClipsToProcess, Queue<PotentialSoftClip> softClips, File bam, int minMappingQuality) {
    super();
    this.bam = bam;
    this.minMappingQuality = minMappingQuality;
    this.softClipsToProcess = softClipsToProcess;
    this.softClips = softClips;
    this.nbSoftClipsFound = 0;
  }

  public boolean isPrintElapsed() {
    return printElapsed;
  }

  public void setPrintElapsed(boolean printElapsed) {
    this.printElapsed = printElapsed;
  }

  public long getNbSoftClipsFound() {
    return nbSoftClipsFound;
  }

  @Override
  public void run() {
    SamReader reader  = null;
    try {
      if (printElapsed)
        System.out.println("Finding SoftClips...");
      
      long fileSize = bam.length();
      CountingInputStream countingStream = new CountingInputStream(new FileInputStream(bam));
      SamReaderFactory samReaderFactory = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
      reader = samReaderFactory.open(SamInputResource.of(new BufferedInputStream(countingStream, Defaults.BUFFER_SIZE)));
      long prevPCT = 0;
      Set<String> trackSC = new HashSet<String>();
      for (SAMRecord record : reader) {
        if (record.getReadUnmappedFlag() || record.getDuplicateReadFlag() || record.getMappingQuality() < minMappingQuality)
          continue;
        if (printElapsed) {
          long pct = countingStream.getByteCount() * 100l / fileSize;
          if (prevPCT != pct) {
            prevPCT = pct;
            System.out.print("\r" + prevPCT + "%");
          }
        }

        Cigar cigar = record.getCigar();
        int totalLength = 0;
        List<CigarElement> cigarElements = cigar.getCigarElements();
        for (int idx = 0; idx < cigar.getCigarElements().size(); idx++) {
          CigarElement cigarElement = cigarElements.get(idx);
          CigarOperator operator = cigarElement.getOperator();
          if (operator.equals(CigarOperator.S)) {
            int pos = record.getReferencePositionAtReadPosition(totalLength);
            if (idx == 0) {
              pos = record.getReferencePositionAtReadPosition(cigarElement.getLength() + 1);
            }
            String chr = record.getReferenceName();
            String key = chr + ':' + pos;

            if (!trackSC.contains(key)) {
              PotentialSoftClip psc = new PotentialSoftClip(chr, pos);
              // Future<ProcessSoftClip> future = sclipExecutor.submit(psc);
              trackSC.add(key);
              softClipsToProcess.add(psc);
              softClips.add(psc);
              nbSoftClipsFound++;
            }
          }
          if (operator.consumesReadBases())
            totalLength += cigarElement.getLength();
        }
      }
      if (printElapsed) {
        System.out.println("\rDone Finding SoftClips");
        System.out.println(MemoryUtils.printGC());
      }
    } catch (FileNotFoundException e) {
      throw new RuntimeException(e);
    } finally {
      if (reader != null) {
        try {
          reader.close();
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      }
    }
  }

}
