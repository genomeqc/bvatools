/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.extractSoftClip;

public class SoftClippedRead {
  private final String readName;
  private final boolean firstRead;
  private final byte bases[];
  private final byte qualities[];

  public SoftClippedRead(String readName, boolean firstRead, byte bases[], byte qualities[]) {
    this.readName = readName;
    this.firstRead = firstRead;
    this.bases = bases;
    this.qualities = qualities;
  }

  public String getReadName() {
    return readName;
  }

  public boolean getFirstOfPairFlag() {
    return firstRead;
  }

  public boolean getSecondOfPairFlag() {
    return !firstRead;
  }

  public byte[] getBases() {
    return bases;
  }

  public byte[] getQualities() {
    return qualities;
  }
}
