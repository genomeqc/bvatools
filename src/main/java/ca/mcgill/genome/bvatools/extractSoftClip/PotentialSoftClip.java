/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.extractSoftClip;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.concurrent.atomic.AtomicBoolean;

public class PotentialSoftClip {
  private final String chr;
  private final int pos;
  private final Collection<SoftClippedRead> readsToOutput;
  private final Collection<String> oneEndAligneds;
  private final Collection<String> translocatedRead;
  private int nbReads;
  private final AtomicBoolean processed;

  public PotentialSoftClip(String chr, int pos) {
    this.chr = chr;
    this.pos = pos;
    this.nbReads = 0;

    processed = new AtomicBoolean(false);
    readsToOutput = new ArrayList<SoftClippedRead>();
    oneEndAligneds = new HashSet<String>();
    translocatedRead = new HashSet<String>();
  }

  public boolean isProcessed() {
    return processed.get();
  }

  public void doneProcessing() {
    processed.set(true);
  }

  public void incrementNbReads() {
    nbReads++;
  }

  public long getNbReads() {
    return nbReads;
  }

  public long getNbOEA() {
    return oneEndAligneds.size();
  }

  public long getNbTranslocatedRead() {
    return translocatedRead.size();
  }

  public String getChr() {
    return chr;
  }

  public int getPos() {
    return pos;
  }

  public Collection<SoftClippedRead> getReadsToOutput() {
    return readsToOutput;
  }

  public Collection<String> getOneEndAligneds() {
    return oneEndAligneds;
  }

  public Collection<String> getTranslocatedRead() {
    return translocatedRead;
  }

  public void setNbReads(int nbReads) {
    this.nbReads = nbReads;
  }
}
