/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.content;

import htsjdk.samtools.AlignmentBlock;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMRecordIterator;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

import ca.mcgill.mcb.pcingola.interval.Genome;
import ca.mcgill.mcb.pcingola.interval.Marker;
import ca.mcgill.mcb.pcingola.interval.Markers;
import ca.mcgill.mcb.pcingola.interval.tree.IntervalForest;
import ca.mcgill.mcb.pcingola.interval.tree.IntervalTree;

public class CountCategories implements Callable<Map<String, Integer>> {
	private final String NO_HIT_TYPE = "no_hit";
	private final File bamFile;
	private final Genome genome;
	private final String chromosome;
	private final IntervalTree tree;
	private final boolean countDuplicates;
	private final TypeWriter typeWriter;

	public CountCategories(Genome genome, IntervalForest forest, String chromosome, File bamFile, boolean countDuplicates, TypeWriter typeWriter) {
		this.genome = genome;
		this.chromosome = chromosome;
		this.tree = forest.getTree(chromosome);
		this.bamFile = bamFile;
		this.countDuplicates = countDuplicates;
		this.typeWriter = typeWriter;
	}

	public Map<String, Integer> call() throws Exception {
		Map<String, Integer> retVal = new HashMap<String, Integer>();
    SamReaderFactory samReaderFactory = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
    SamReader samReader = samReaderFactory.open(bamFile);

		try {
			Set<String> foundTypes = new HashSet<String>();
			SAMRecordIterator samIterator = samReader.query(chromosome, 0, 0, false);
			while (samIterator.hasNext()) {
				SAMRecord record = samIterator.next();
				if (record.getNotPrimaryAlignmentFlag() || record.getReadUnmappedFlag() || (!countDuplicates && record.getDuplicateReadFlag())) continue;

				foundTypes.clear();
				for (AlignmentBlock block : record.getAlignmentBlocks()) {
					int start = block.getReferenceStart();
					int end = block.getReferenceStart() + block.getLength() - 1;
					Marker overlap = new Marker(genome.getChromosome(chromosome), start - 1, end - 1, record.getReadNegativeStrandFlag(), null);
					Markers markers = tree.query(overlap);

					if (markers.size() == 0) {
						if (!foundTypes.contains(NO_HIT_TYPE)) {
							foundTypes.add(NO_HIT_TYPE);
							if (!retVal.containsKey(NO_HIT_TYPE)) {
								retVal.put(NO_HIT_TYPE, 1);
							} else {
								retVal.put(NO_HIT_TYPE, retVal.get(NO_HIT_TYPE) + 1);
							}
							typeWriter.addRead(NO_HIT_TYPE, record);
						}
					} else {
						for (Marker marker : markers) {
							if (!foundTypes.contains(marker.getId())) {
								foundTypes.add(marker.getId());
								if (!retVal.containsKey(marker.getId())) {
									if (marker.getId().equals(NO_HIT_TYPE)) throw new RuntimeException("No hit type found in GTF. This is a special case and shouldn't be found");
									retVal.put(marker.getId(), 1);
								} else {
									retVal.put(marker.getId(), retVal.get(marker.getId()) + 1);
								}
								
								typeWriter.addRead(marker.getId(), record);
							}
						}
					}
				}
			}
			samIterator.close();
		} finally {
			samReader.close();
		}

		return retVal;
	}
}
