/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.content;

import ca.mcgill.mcb.pcingola.interval.Marker;
import ca.mcgill.mcb.pcingola.interval.Markers;

public class CountingTranscript extends Marker {
  private static final long serialVersionUID = 1517167363725205099L;
  private Markers exons;
  private final String geneId;
  private final String geneName;

  public CountingTranscript(Marker parent, String geneId, String geneName, int start, int end, boolean strandMinus, String id) {
    super(parent, start, end, strandMinus, id);
    this.geneId = geneId;
    this.geneName = geneName;
  }

  public void addExon(CountingExon exon) {
    if(exon.getParent() != this)
      throw new RuntimeException("Parent transcript isn't the same. Transcript: " + this.getId() + " exon: " + exon.getId());
    exons.add(exon);
  }
  
  public void sortExons() {
    exons.sort();
  }

  public Markers getExons() {
    return exons;
  }

  public String getGeneId() {
    return geneId;
  }

  public String getGeneName() {
    return geneName;
  }
  
}
