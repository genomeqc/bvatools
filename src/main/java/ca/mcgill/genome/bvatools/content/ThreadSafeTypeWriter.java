/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.content;

import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.SAMFileHeader.SortOrder;
import htsjdk.samtools.SAMFileWriter;
import htsjdk.samtools.SAMFileWriterFactory;
import htsjdk.samtools.SAMRecord;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ThreadSafeTypeWriter implements TypeWriter {
	private Map<String, SAMFileWriter> writers = new HashMap<String, SAMFileWriter>();
	private SAMFileHeader header;
	private final File outputDirectory;
	
	public ThreadSafeTypeWriter(SAMFileHeader origHeader, File outputDirectory) {
		this.outputDirectory = outputDirectory;
		if(!this.outputDirectory.exists()) {
			if(!outputDirectory.mkdir()) {
				throw new RuntimeException("Couldn't create directory " + outputDirectory);
			}
		}

		if(!this.outputDirectory.isDirectory()) {
			throw new RuntimeException(outputDirectory + " is not a directory");
		}

		header = origHeader.clone();
		header.setSortOrder(SortOrder.unsorted);
	}

	@Override
	public void addRead(String type, SAMRecord record) {
		SAMFileWriter writer = writers.get(type);
		if(writer == null) {
			synchronized (writers) {
				writer = writers.get(type);
				// test again in case this changed
				if(writer == null) {
					writer = new SAMFileWriterFactory().makeSAMOrBAMWriter(header.clone(), false, new File(outputDirectory, type+".bam"));
					writers.put(type, writer);
				}
			}
		}

		synchronized (writer) {
			writer.addAlignment(record);
		}
	}

	@Override
	public void close() throws IOException {
		for(SAMFileWriter writer : writers.values()) {
			writer.close();
		}
		writers.clear();
	}

}
