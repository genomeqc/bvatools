/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.readsQC;

import java.util.ArrayList;
import java.util.List;

import org.jfree.data.Range;
import org.jfree.data.RangeInfo;
import org.jfree.data.statistics.BoxAndWhiskerItem;
import org.jfree.data.statistics.BoxAndWhiskerXYDataset;
import org.jfree.data.xy.AbstractXYDataset;

/**
 * A simple implementation of the {@link BoxAndWhiskerXYDataset} interface. This dataset implementation can hold only
 * one series.
 */
public class BinnedQualityBoxAndWhiskerXYDataset extends AbstractXYDataset implements BoxAndWhiskerXYDataset, RangeInfo {
  private static final long serialVersionUID = -1158008372302747447L;

  /** The series key. */
  private final Comparable<?> seriesKey;

  /** Bins for the qualities. */
  private final ArrayList<BoxAndWhiskerItem> perBaseQualityStats;

  /** The maximum quality value. */
  private final Number maximumQualityValue;

  /** The range of values. */
  private final Range rangeBounds;

  /**
   * The coefficient used to calculate outliers. Tukey's default value is 1.5 (see EDA) Any value which is greater than
   * Q3 + (interquartile range * outlier coefficient) is considered to be an outlier. Can be altered if the data is
   * particularly skewed.
   */
  private double outlierCoefficient = 1.5;

  /**
   * The coefficient used to calculate farouts. Tukey's default value is 2 (see EDA) Any value which is greater than Q3
   * + (interquartile range * farout coefficient) is considered to be a farout. Can be altered if the data is
   * particularly skewed.
   */
  private double faroutCoefficient = 2.0;

  /**
   * {@inheritDoc}
   */
  public BinnedQualityBoxAndWhiskerXYDataset(Number maximumQualityValue, Comparable<?> seriesKey) {
    this.maximumQualityValue = maximumQualityValue;
    this.seriesKey = seriesKey;
    this.perBaseQualityStats = new ArrayList<BoxAndWhiskerItem>();
    this.rangeBounds = new Range(0, this.maximumQualityValue.doubleValue());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public double getOutlierCoefficient() {
    return this.outlierCoefficient;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public double getFaroutCoefficient() {
    return this.faroutCoefficient;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int getSeriesCount() {
    return 1;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int getItemCount(int series) {
    return this.perBaseQualityStats.size();
  }

  /**
   * {@inheritDoc}
   */
  public void add(BoxAndWhiskerItem item) {
    perBaseQualityStats.add(item);

    if (item.getMaxRegularValue().longValue() > maximumQualityValue.doubleValue()) {
      throw new IllegalArgumentException("item has a higher quality than range");
    }
    fireDatasetChanged();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Comparable<?> getSeriesKey(int i) {
    return this.seriesKey;
  }

  /**
   * {@inheritDoc}
   */

  public BoxAndWhiskerItem getItem(int series, int item) {
    return this.perBaseQualityStats.get(item);
  }

  /**
   * {@inheritDoc}
   */

  @Override
  public Number getX(int series, int item) {
    return item;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Number getY(int series, int item) {
    return getMeanValue(series, item);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Number getMeanValue(int series, int item) {
    Number result = null;
    BoxAndWhiskerItem stats = this.perBaseQualityStats.get(item);
    if (stats != null) {
      result = stats.getMean();
    }
    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Number getMedianValue(int series, int item) {
    Number result = null;
    BoxAndWhiskerItem stats = this.perBaseQualityStats.get(item);
    if (stats != null) {
      result = stats.getMedian();
    }
    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Number getQ1Value(int series, int item) {
    Number result = null;
    BoxAndWhiskerItem stats = this.perBaseQualityStats.get(item);
    if (stats != null) {
      result = stats.getQ1();
    }
    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Number getQ3Value(int series, int item) {
    Number result = null;
    BoxAndWhiskerItem stats = this.perBaseQualityStats.get(item);
    if (stats != null) {
      result = stats.getQ3();
    }
    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Number getMinRegularValue(int series, int item) {
    Number result = null;
    BoxAndWhiskerItem stats = this.perBaseQualityStats.get(item);
    if (stats != null) {
      result = stats.getMinRegularValue();
    }
    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Number getMaxRegularValue(int series, int item) {
    Number result = null;
    BoxAndWhiskerItem stats = this.perBaseQualityStats.get(item);
    if (stats != null) {
      result = stats.getMaxRegularValue();
    }
    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Number getMinOutlier(int series, int item) {
    Number result = null;
    BoxAndWhiskerItem stats = this.perBaseQualityStats.get(item);
    if (stats != null) {
      result = stats.getMinOutlier();
    }
    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Number getMaxOutlier(int series, int item) {
    Number result = null;
    BoxAndWhiskerItem stats = this.perBaseQualityStats.get(item);
    if (stats != null) {
      result = stats.getMaxOutlier();
    }
    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<?> getOutliers(int series, int item) {
    List<?> result = null;
    BoxAndWhiskerItem stats = this.perBaseQualityStats.get(item);
    if (stats != null) {
      result = stats.getOutliers();
    }
    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public double getRangeLowerBound(boolean includeInterval) {
    return 0;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public double getRangeUpperBound(boolean includeInterval) {
    return maximumQualityValue.doubleValue();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Range getRangeBounds(boolean includeInterval) {
    return this.rangeBounds;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((seriesKey == null) ? 0 : seriesKey.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    BinnedQualityBoxAndWhiskerXYDataset other = (BinnedQualityBoxAndWhiskerXYDataset) obj;
    if (seriesKey == null) {
      if (other.seriesKey != null)
        return false;
    } else if (!seriesKey.equals(other.seriesKey))
      return false;
    return true;
  }
}