/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.readsQC;

import gnu.trove.list.array.TFloatArrayList;
import gnu.trove.list.array.TLongArrayList;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import ca.mcgill.genome.bvatools.parsers.Fragment;
import ca.mcgill.genome.bvatools.parsers.Read;

public class SequenceContentPlugin extends DefaultQCPlugin {
  public enum Bases {A,T,C,G,N};

  private final List<TLongArrayList> aReadBins = new ArrayList<TLongArrayList>();
  private final List<TLongArrayList> tReadBins = new ArrayList<TLongArrayList>();
  private final List<TLongArrayList> cReadBins = new ArrayList<TLongArrayList>();
  private final List<TLongArrayList> gReadBins = new ArrayList<TLongArrayList>();
  private final List<TLongArrayList> nReadBins = new ArrayList<TLongArrayList>();

  @Override
  public void processSequence(Fragment fragment) {
    // Add missing reads in list
    while(aReadBins.size() < fragment.size()) {
      aReadBins.add(new TLongArrayList());
      tReadBins.add(new TLongArrayList());
      cReadBins.add(new TLongArrayList());
      gReadBins.add(new TLongArrayList());
      nReadBins.add(new TLongArrayList());
    }
    
    // Add missing cycles for each read in list
    for(int idx=0; idx < fragment.size(); idx++) {
      Read read = fragment.getRead(idx);
      if(read == null)
        continue;

      for (int i = aReadBins.get(idx).size(); i < read.length(); i++) {
        aReadBins.get(idx).add(0l);
        tReadBins.get(idx).add(0l);
        cReadBins.get(idx).add(0l);
        gReadBins.get(idx).add(0l);
        nReadBins.get(idx).add(0l);
      }

      String nucleotides = read.getSequence().toLowerCase();
      for (int i = 0; i < read.length(); i++) {
        char nucleotide = nucleotides.charAt(i);
        if (nucleotide == 'a')
          aReadBins.get(idx).set(i, aReadBins.get(idx).get(i) + 1);
        else if (nucleotide == 't')
          tReadBins.get(idx).set(i, tReadBins.get(idx).get(i) + 1);
        else if (nucleotide == 'c')
          cReadBins.get(idx).set(i, cReadBins.get(idx).get(i) + 1);
        else if (nucleotide == 'g')
          gReadBins.get(idx).set(i, gReadBins.get(idx).get(i) + 1);
        else if (nucleotide == 'n')
          nReadBins.get(idx).set(i, nReadBins.get(idx).get(i) + 1);
        else
          throw new RuntimeException("Unknown nucleotide: " + nucleotide);
      }
    }
  }

  @Override
  public void generateGraph(File outputDirectory) throws IOException {
    LinkedHashMap<Bases, TFloatArrayList> bins = getBasePercentageForEachCycle();

    XYSeries aSerie = new XYSeries("%A");
    XYSeries tSerie = new XYSeries("%T");
    XYSeries cSerie = new XYSeries("%C");
    XYSeries gSerie = new XYSeries("%G");
    XYSeries nSerie = new XYSeries("%N");
    XYSeries gcSerie = new XYSeries("%GC");

    for (int i = 0; i < bins.get(Bases.A).size(); i++) {
      double sum = bins.get(Bases.A).get(i) + bins.get(Bases.T).get(i) + bins.get(Bases.C).get(i) + bins.get(Bases.G).get(i) + bins.get(Bases.N).get(i);
      aSerie.add(i, (double) bins.get(Bases.A).get(i) * 100.0 / sum);
      tSerie.add(i, (double) bins.get(Bases.T).get(i) * 100.0 / sum);
      cSerie.add(i, (double) bins.get(Bases.C).get(i) * 100.0 / sum);
      gSerie.add(i, (double) bins.get(Bases.G).get(i) * 100.0 / sum);
      nSerie.add(i, (double) bins.get(Bases.N).get(i) * 100.0 / sum);
      gcSerie.add(i, (bins.get(Bases.G).get(i) + bins.get(Bases.C).get(i)) * 100.0 / sum);
    }

    XYSeriesCollection contentCollection = new XYSeriesCollection();
    contentCollection.addSeries(aSerie);
    contentCollection.addSeries(tSerie);
    contentCollection.addSeries(cSerie);
    contentCollection.addSeries(gSerie);

    XYSeriesCollection nCollection = new XYSeriesCollection();
    nCollection.addSeries(nSerie);

    XYSeriesCollection gcCollection = new XYSeriesCollection();
    gcCollection.addSeries(gcSerie);

    NumberAxis xAxis = new UnlimitedTicksNumberAxis("Cycles");
    xAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
    xAxis.setFixedAutoRange(bins.get(Bases.A).size());
    xAxis.setLowerBound(0);
    xAxis.setUpperBound(bins.get(Bases.A).size());

    NumberAxis yAxis = new UnlimitedTicksNumberAxis("% nb Sequences");
    yAxis.setRange(0, 100);
    yAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

    XYLineAndShapeRenderer contentRenderer = new XYLineAndShapeRenderer(true, false);
    contentRenderer.setSeriesPaint(0, Color.BLUE);
    contentRenderer.setSeriesPaint(1, Color.GREEN);
    contentRenderer.setSeriesPaint(2, Color.BLACK);
    contentRenderer.setSeriesPaint(3, Color.RED);
    XYPlot contentPlot = new XYPlot(contentCollection, xAxis, yAxis, contentRenderer);

    JFreeChart chart;
    chart = new JFreeChart("Sequence Content", JFreeChart.DEFAULT_TITLE_FONT, contentPlot, true);
    chart.setBackgroundPaint(Color.white);
    contentPlot.setBackgroundPaint(Color.white);
    File outputFile = new File(outputDirectory, generateGraphFileName("03sequenceContent", ".png"));
    ChartUtilities.saveChartAsPNG(outputFile, chart, 60 + 15 * bins.get(Bases.A).size(), 600);

    XYLineAndShapeRenderer nRenderer = new XYLineAndShapeRenderer(true, false);
    XYPlot nPlot = new XYPlot(nCollection, xAxis, yAxis, nRenderer);
    chart = new JFreeChart("Sequence N Content", JFreeChart.DEFAULT_TITLE_FONT, nPlot, true);
    chart.setBackgroundPaint(Color.white);
    nPlot.setBackgroundPaint(Color.white);
    outputFile = new File(outputDirectory, generateGraphFileName("05sequenceNContent", ".png"));
    ChartUtilities.saveChartAsPNG(outputFile, chart, 60 + 15 * bins.get(Bases.A).size(), 600);
  }

  /**
   * The percent value of bases for each cycle.
   * @return
   */
  public LinkedHashMap<Bases, TFloatArrayList> getBasePercentageForEachCycle() {
    LinkedHashMap<Bases, TFloatArrayList> retVal = new LinkedHashMap<Bases, TFloatArrayList>();
    for(Bases base : Bases.values()) {
      retVal.put(base, new TFloatArrayList());
    }

    // merge them all
    retVal.put(Bases.A, new TFloatArrayList());
    retVal.put(Bases.T, new TFloatArrayList());
    retVal.put(Bases.C, new TFloatArrayList());
    retVal.put(Bases.G, new TFloatArrayList());
    retVal.put(Bases.N, new TFloatArrayList());

    for(int idx=0; idx < aReadBins.size(); idx++) {
      for (int i = 0; i < aReadBins.get(idx).size(); i++) {
        double sum = aReadBins.get(idx).get(i) + tReadBins.get(idx).get(i) + cReadBins.get(idx).get(i) + gReadBins.get(idx).get(i) + nReadBins.get(idx).get(i);

        retVal.get(Bases.A).add((float) (aReadBins.get(idx).get(i) * 100.0 / sum));
        retVal.get(Bases.T).add((float) (tReadBins.get(idx).get(i) * 100.0 / sum));
        retVal.get(Bases.C).add((float) (cReadBins.get(idx).get(i) * 100.0 / sum));
        retVal.get(Bases.G).add((float) (gReadBins.get(idx).get(i) * 100.0 / sum));
        retVal.get(Bases.N).add((float) (nReadBins.get(idx).get(i) * 100.0 / sum));
      }
    }

    return retVal;
  }

}
