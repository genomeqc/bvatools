/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.readsQC;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Marker;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYAreaRenderer;
import org.jfree.data.statistics.SimpleHistogramBin;
import org.jfree.data.statistics.SimpleHistogramDataset;

import ca.mcgill.genome.bvatools.parsers.Fragment;
import ca.mcgill.genome.bvatools.parsers.SoftClippedSequence;

public class RunTrimmingPlugin extends DefaultQCPlugin implements IReadSetMedian {
  private final List<Long> sequenceLength = new ArrayList<Long>(1000);
  private final List<Long> trimLength = new ArrayList<Long>(1000);
  private final List<Long> unTrimmedSequenceLength = new ArrayList<Long>(1000);
  private long nbReads = 0;
  private int seqLengthMedian = -1;
  private int trimLengthMedian = -1;
  private int unTrimmedSequenceLengthMedian = -1;
  private float seqLengthArithmeticMean = -1;
  private float trimLengthArithmeticMean = -1;
  private float unTrimmedLengthArithmeticMean = -1;
  private float seqLengthStd = -1;
  private float trimLengthStd = -1;
  private float unTrimmedLengthStd = -1;

  private JFreeChart chart = null;

  long sumSequenceLength = 0;
  long sumTrimLength = 0;
  long sumUnTrimLength = 0;

  @Override
  public void processSequence(Fragment fragment) {
    nbReads++;

    SoftClippedSequence clippedSequence = (SoftClippedSequence) fragment.getRead(0);
    // the longest possible reads will come from unTrimmedSequenceLength
    for (int i = unTrimmedSequenceLength.size() - 1; i < clippedSequence.getUnTrimmedBaseQualities().length; i++) {
      unTrimmedSequenceLength.add(0l);
      sequenceLength.add(0l);
      trimLength.add(0l);
    }

    int trimmedLength = clippedSequence.getBaseQualities().length;
    int fullLength = clippedSequence.getUnTrimmedBaseQualities().length;
    int clipAmmount = fullLength - trimmedLength;

    sumSequenceLength += trimmedLength;
    sumTrimLength += clipAmmount;
    sumUnTrimLength += fullLength;

    unTrimmedSequenceLength.set(fullLength, unTrimmedSequenceLength.get(fullLength) + 1);
    sequenceLength.set(trimmedLength, sequenceLength.get(trimmedLength) + 1);
    trimLength.set(clipAmmount, trimLength.get(clipAmmount) + 1);
  }

  private void computeChart() {
    if (chart != null)
      return;

    long nbReadsQ1 = nbReads / 4;
    long nbReadsQ2 = nbReads / 2;
    long nbReadsQ3 = nbReads / 4 * 3;

    int seqLengthQ1 = -1;
    int seqLengthQ3 = -1;
    int trimLengthQ1 = -1;
    int trimLengthQ3 = -1;
    int unTrimmedSequenceLengthQ1 = -1;
    int unTrimmedSequenceLengthQ3 = -1;

    SimpleHistogramDataset sequenceLengthDataset = new SimpleHistogramDataset("Sequence Length");
    SimpleHistogramDataset trimLengthDataset = new SimpleHistogramDataset("Trim Length");
    SimpleHistogramDataset unTrimmedSequenceLengthDataset = new SimpleHistogramDataset("Un-Trimmed Length");
    int seqLengthHighestCount = 0;
    int trimLengthHighestCount = 0;
    int unTrimmedSequenceLengthHighestCount = 0;
    int seqLengthNbReads = 0;
    int trimLengthNbReads = 0;
    int unTrimLengthNbReads = 0;
//    int sumSequenceDeviation = 0;
//    int sumTrimDeviation = 0;
//    int sumUnTrimDeviation = 0;

    seqLengthArithmeticMean = sumSequenceLength / (float) nbReads;
    trimLengthArithmeticMean = sumTrimLength / (float) nbReads;
    unTrimmedLengthArithmeticMean = sumUnTrimLength / (float) nbReads;

    for (int i = 0; i < sequenceLength.size(); i++) {
      // read length quartile calculation
      seqLengthNbReads += sequenceLength.get(i).intValue();
      if (seqLengthNbReads >= nbReadsQ1 && seqLengthQ1 < 0) {
        seqLengthQ1 = i;
      }
      if (seqLengthNbReads >= nbReadsQ2 && seqLengthMedian < 0) {
        seqLengthMedian = i;
      }
      if (seqLengthNbReads >= nbReadsQ3 && seqLengthQ3 < 0) {
        seqLengthQ3 = i;
      }
//      sumSequenceDeviation += sequenceLength.get(i).intValue() * Math.pow(i - seqLengthArithmeticMean, 2);

      trimLengthNbReads += trimLength.get(i).intValue();
      if (trimLengthNbReads >= nbReadsQ1 && trimLengthQ1 < 0) {
        trimLengthQ1 = i;
      }
      if (trimLengthNbReads >= nbReadsQ2 && trimLengthMedian < 0) {
        trimLengthMedian = i;
      }
      if (trimLengthNbReads >= nbReadsQ3 && trimLengthQ3 < 0) {
        trimLengthQ3 = i;
      }
//      sumTrimDeviation += trimLength.get(i).intValue() * Math.pow(i - trimLengthArithmeticMean, 2);

      unTrimLengthNbReads += unTrimmedSequenceLength.get(i).intValue();
      if (unTrimLengthNbReads >= nbReadsQ1 && unTrimmedSequenceLengthQ1 < 0) {
        unTrimmedSequenceLengthQ1 = i;
      }
      if (unTrimLengthNbReads >= nbReadsQ2 && unTrimmedSequenceLengthMedian < 0) {
        unTrimmedSequenceLengthMedian = i;
      }
      if (unTrimLengthNbReads >= nbReadsQ3 && unTrimmedSequenceLengthQ3 < 0) {
        unTrimmedSequenceLengthQ3 = i;
      }
//      sumUnTrimDeviation += unTrimmedSequenceLength.get(i).intValue() * Math.pow(i - unTrimmedLengthArithmeticMean, 2);

      SimpleHistogramBin seqLengthBin = new SimpleHistogramBin(i, i + 1, true, false);
      seqLengthBin.setItemCount(sequenceLength.get(i).intValue());
      if (seqLengthHighestCount < sequenceLength.get(i).intValue())
        seqLengthHighestCount = sequenceLength.get(i).intValue();
      sequenceLengthDataset.addBin(seqLengthBin);

      SimpleHistogramBin trimLengthBin = new SimpleHistogramBin(i, i + 1, true, false);
      trimLengthBin.setItemCount(trimLength.get(i).intValue());
      if (trimLengthHighestCount < trimLength.get(i).intValue())
        trimLengthHighestCount = trimLength.get(i).intValue();
      trimLengthDataset.addBin(trimLengthBin);

      SimpleHistogramBin unTrimmedSequenceLengthBin = new SimpleHistogramBin(i, i + 1, true, false);
      unTrimmedSequenceLengthBin.setItemCount(unTrimmedSequenceLength.get(i).intValue());
      if (unTrimmedSequenceLengthHighestCount < unTrimmedSequenceLength.get(i).intValue())
        unTrimmedSequenceLengthHighestCount = unTrimmedSequenceLength.get(i).intValue();
      unTrimmedSequenceLengthDataset.addBin(unTrimmedSequenceLengthBin);
    }

    seqLengthStd = (float) Math.sqrt((sumSequenceLength / (float) nbReads));
    trimLengthStd = (float) Math.sqrt((sumTrimLength / (float) nbReads));
    unTrimmedLengthStd = (float) Math.sqrt((sumUnTrimLength / (float) nbReads));

    NumberAxis seqLengthHighestAxis = new NumberAxis("Nb Sequences");
    seqLengthHighestAxis.setRange(0, seqLengthHighestCount);
    NumberAxis trimLengthHighestAxis = new NumberAxis("Nb Sequences");
    trimLengthHighestAxis.setRange(0, trimLengthHighestCount);
    NumberAxis unTrimmedSequenceLengthHighestAxis = new NumberAxis("Nb Sequences");
    unTrimmedSequenceLengthHighestAxis.setRange(0, unTrimmedSequenceLengthHighestCount);

    XYAreaRenderer unTrimmedRenderer = new XYAreaRenderer(XYAreaRenderer.AREA);
    XYPlot unTrimmedPlot = new XYPlot(unTrimmedSequenceLengthDataset, null, unTrimmedSequenceLengthHighestAxis, unTrimmedRenderer);
    unTrimmedPlot.setRangeAxisLocation(AxisLocation.BOTTOM_OR_LEFT);

    XYAreaRenderer trimmingRenderer = new XYAreaRenderer(XYAreaRenderer.AREA);
    XYPlot trimmingPlot = new XYPlot(trimLengthDataset, null, trimLengthHighestAxis, trimmingRenderer);
    trimmingPlot.setRangeAxisLocation(AxisLocation.BOTTOM_OR_LEFT);

    XYAreaRenderer seqRenderer = new XYAreaRenderer(XYAreaRenderer.AREA);
    XYPlot seqPlot = new XYPlot(sequenceLengthDataset, null, seqLengthHighestAxis, seqRenderer);
    seqPlot.setRangeAxisLocation(AxisLocation.BOTTOM_OR_LEFT);

    final Marker untrimLengthQ1Marker = new ValueMarker(unTrimmedSequenceLengthQ1);
    untrimLengthQ1Marker.setPaint(Color.BLUE);
    unTrimmedPlot.addDomainMarker(untrimLengthQ1Marker);
    final Marker untrimLengthMedianMarker = new ValueMarker(unTrimmedSequenceLengthMedian);
    untrimLengthMedianMarker.setPaint(Color.BLUE);
    unTrimmedPlot.addDomainMarker(untrimLengthMedianMarker);
    final Marker untrimLengthQ2Marker = new ValueMarker(unTrimmedSequenceLengthQ3);
    untrimLengthQ2Marker.setPaint(Color.BLUE);
    unTrimmedPlot.addDomainMarker(untrimLengthQ2Marker);

    final Marker trimLengthQ1Marker = new ValueMarker(trimLengthQ1);
    trimLengthQ1Marker.setPaint(Color.GREEN);
    trimmingPlot.addDomainMarker(trimLengthQ1Marker);
    final Marker trimLengthMedianMarker = new ValueMarker(trimLengthMedian);
    trimLengthMedianMarker.setPaint(Color.GREEN);
    trimmingPlot.addDomainMarker(trimLengthMedianMarker);
    final Marker trimLengthQ2Marker = new ValueMarker(trimLengthQ3);
    trimLengthQ2Marker.setPaint(Color.GREEN);
    trimmingPlot.addDomainMarker(trimLengthQ2Marker);

    // quartiles markers (only for sequence length)
    final Marker seqLengthQ1Marker = new ValueMarker(seqLengthQ1);
    seqLengthQ1Marker.setPaint(Color.RED);
    seqPlot.addDomainMarker(seqLengthQ1Marker);
    final Marker seqLengthMedianMarker = new ValueMarker(seqLengthMedian);
    seqLengthMedianMarker.setPaint(Color.RED);
    seqPlot.addDomainMarker(seqLengthMedianMarker);
    final Marker seqLengthQ2Marker = new ValueMarker(seqLengthQ3);
    seqLengthQ2Marker.setPaint(Color.RED);
    seqPlot.addDomainMarker(seqLengthQ2Marker);

    // parent plot...
    NumberAxis lengthAxis = new NumberAxis("Length");
    lengthAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
    CombinedDomainXYPlot plot = new CombinedDomainXYPlot(lengthAxis);
    plot.setGap(10.0);

    plot.add(unTrimmedPlot, 1);
    plot.add(trimmingPlot, 1);
    plot.add(seqPlot, 1);
    plot.setOrientation(PlotOrientation.VERTICAL);

    chart = new JFreeChart("Trimming Effect", JFreeChart.DEFAULT_TITLE_FONT, plot, true);
    // chart.removeLegend();
    chart.setBackgroundPaint(Color.white);
    plot.setBackgroundPaint(Color.white);
  }

  @Override
  public void generateGraph(File outputDirectory) throws IOException {
    computeChart();

    File outputFile = new File(outputDirectory, generateGraphFileName("02clippingChart", ".png"));
    ChartUtilities.saveChartAsPNG(outputFile, chart, 60 + sequenceLength.size(), 600);
  }

  @Override
  public int getMedian() {
    computeChart();
    return seqLengthMedian;
  }

  @Override
  public float getArithmeticMean() {
    computeChart();
    return seqLengthArithmeticMean;
  }

  public Float getFinalAvgReadLength() {
    computeChart();
    return seqLengthArithmeticMean;
  }

  public Integer getFinalMedianReadLength() {
    computeChart();
    return seqLengthMedian;
  }

  public Float getFinalStdReadLength() {
    computeChart();
    return seqLengthStd;
  }

  public Float getTrimAvgReadLength() {
    computeChart();
    return trimLengthArithmeticMean;
  }

  public Integer getTrimMedianReadLength() {
    computeChart();
    return trimLengthMedian;
  }

  public Float getTrimStdReadLength() {
    computeChart();
    return trimLengthStd;
  }

  public Float getRawAvgReadLength() {
    computeChart();
    return unTrimmedLengthArithmeticMean;
  }

  public Integer getRawMedianReadLength() {
    computeChart();
    return unTrimmedSequenceLengthMedian;
  }

  public Float getRawStdReadLength() {
    computeChart();
    return unTrimmedLengthStd;

  }

}
