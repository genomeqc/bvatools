/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.readsQC;

import gnu.trove.map.hash.TObjectLongHashMap;
import gnu.trove.procedure.TObjectLongProcedure;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.imageio.ImageIO;

import htsjdk.samtools.Defaults;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.DefaultCategoryDataset;

import ca.mcgill.genome.bvatools.parsers.Fragment;
import ca.mcgill.genome.bvatools.parsers.Read;

/**
 * In this class there is intentionally no thread safety. The threads work on their own sequences in the same hash,
 * there should be no collision and since the hash is initialised with all the possible keys from the start, it should
 * not grow or shrink.
 * 
 */
public class KnownSequenceCountPlugin extends DefaultQCPlugin {
  private static final int DEFAULT_HEIGHT = 200;
  private static final int ADDITIONAL_BAR_HEIGHT = 25;
  // private final Map<String, String> knownSequences;
  private final TObjectLongHashMap<String> nbKnownSequencesFound = new TObjectLongHashMap<String>();
  private long totalNbReads = 0;
  private List<IdentifiedSequence> identifiedSequences = null;
  private final List<FindSequences> findSequences = new ArrayList<FindSequences>();
  private final List<Thread> threads = new ArrayList<Thread>();

  public static InputStream getKnownSequencesStream() {
    return KnownSequenceCountPlugin.class.getResourceAsStream("knownSequences.txt");
  }

  public KnownSequenceCountPlugin() {
    this(1);
  }

  public KnownSequenceCountPlugin(int nbThreads) {
    this(getKnownSequencesStream(), nbThreads);
  }

  public KnownSequenceCountPlugin(File knownSequences) throws FileNotFoundException {
    this(knownSequences, 1);
  }

  public KnownSequenceCountPlugin(File knownSequences, int nbThreads) throws FileNotFoundException {
    this(new FileInputStream(knownSequences), nbThreads);
  }

  /**
   * This class will close the stream once it's done with it.
   * 
   * @param knownSequencesInput
   * @param nbThreads
   */
  public KnownSequenceCountPlugin(InputStream knownSequencesInput, int nbThreads) {
    Properties knownSequencesResource = new Properties();
    try {
      InputStream bufferedStream = new BufferedInputStream(knownSequencesInput, Defaults.BUFFER_SIZE);
      knownSequencesResource.load(bufferedStream);
      bufferedStream.close();
      int seqPerThread = knownSequencesResource.size() / nbThreads;
      if (seqPerThread == 0) {
        seqPerThread = 1;
      }

      int jobIdx = 0;
      int nbPut = 0;
      Map<String, String> knownSequences = new HashMap<String, String>();
      for (Entry<Object, Object> entry : knownSequencesResource.entrySet()) {
        String name = (String) entry.getKey();
        knownSequences.put(name, (String) entry.getValue());
        nbPut++;
        this.nbKnownSequencesFound.put(name, 0l);
        if (nbPut >= seqPerThread) {
          FindSequences fs = new FindSequences(knownSequences, this.nbKnownSequencesFound);
          findSequences.add(fs);
          Thread t = new Thread(fs, "KnownSequenceCountPlugin-" + jobIdx);
          t.setDaemon(true);
          t.start();
          threads.add(t);
          knownSequences = new HashMap<String, String>();
          jobIdx++;
          nbPut = 0;
        }
      }
    } catch (IOException e) {
      throw new RuntimeException("Couldn't load known sequence file");
    }
  }

  @Override
  public void processSequence(Fragment fragment) {
    totalNbReads++;
    for (FindSequences fs : findSequences) {
      fs.setFragment(fragment);
    }
    // for (Entry<String, String> entry : knownSequences.entrySet()) {
    // String knownName = entry.getKey();
    // String knownSeq = entry.getValue();
    // if (sequence.getSequence().indexOf(knownSeq) != -1) {
    // nbKnownSequencesFound.increment(knownName);
    // }
    // }
  }

  public List<IdentifiedSequence> getIdentifiedSequences() {
    computeNbKnownSequences();
    return identifiedSequences;
  }

  public void computeNbKnownSequences() {
    if (identifiedSequences != null)
      return;

    identifiedSequences = new ArrayList<IdentifiedSequence>();

    nbKnownSequencesFound.forEachEntry(new TObjectLongProcedure<String>() {
      @Override
      public boolean execute(String a, long b) {
        double percent = (double) b * 100.0d / totalNbReads;
        if (percent > 0.01) {
          identifiedSequences.add(new IdentifiedSequence(a.replace('_', ' '), percent));
        }

        return true;
      }
    });
  }

  @Override
  public void generateGraph(File outputDirectory) throws IOException {
    computeNbKnownSequences();
    DefaultCategoryDataset dataset = new DefaultCategoryDataset();
    String seriesKey = "Identified Reads (%)";
    double maxIdentifiedSequence = 0;
    for (IdentifiedSequence identifiedSequence : identifiedSequences) {
      dataset.addValue(identifiedSequence.getPercent(), seriesKey, identifiedSequence.getSequenceName());
      maxIdentifiedSequence = Math.max(maxIdentifiedSequence, identifiedSequence.getPercent());
    }

    File outputFile = new File(outputDirectory, generateGraphFileName("07knownSequences", ".png"));
    if (dataset.getColumnCount() == 0) {
      final int height = 100;
      final String text = "No known sequences found";
      BufferedImage image = new BufferedImage(DEFAULT_WIDTH, height, BufferedImage.TYPE_INT_ARGB);
      Graphics2D g = image.createGraphics();
      Font font = new Font(Font.DIALOG, Font.BOLD, 26);
      g.setFont(font);
      g.setPaint(Color.RED);

      FontMetrics fm = g.getFontMetrics(font);
      java.awt.geom.Rectangle2D rect = fm.getStringBounds(text, g);

      int textHeight = (int) (rect.getHeight());
      int textWidth = (int) (rect.getWidth());

      // Center text horizontally and vertically
      int x = (DEFAULT_WIDTH - textWidth) / 2;
      int y = (height - textHeight) / 2 + fm.getAscent();
      g.drawString(text, x, y); // Draw the string.
      g.dispose();
      ImageIO.write(image, "png", outputFile);
    } else {
      // create the chart...
      ChartFactory.setChartTheme(StandardChartTheme.createJFreeTheme());
      JFreeChart chart = ChartFactory.createBarChart("Known Sequences", // chart title
          "Known sequence name", // domain axis label
          "Reads (%)", // range axis label
          dataset, // data
          PlotOrientation.HORIZONTAL, // orientation
          true, // include legend
          true, false);
      chart.setBackgroundPaint(Color.white);
      CategoryPlot plot = (CategoryPlot) chart.getPlot();
      plot.setBackgroundPaint(Color.white);
      plot.setRangeGridlinePaint(Color.lightGray);
      plot.setRangeAxisLocation(AxisLocation.BOTTOM_OR_LEFT);
      BarRenderer renderer = (BarRenderer) plot.getRenderer();
      renderer.setBaseItemLabelsVisible(true);
      renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
      renderer.setMaximumBarWidth((double) ADDITIONAL_BAR_HEIGHT / (double) DEFAULT_HEIGHT);
      renderer.setBarPainter(new StandardBarPainter());
      CategoryAxis categoryAxis = plot.getDomainAxis();
      categoryAxis.setCategoryMargin(0.05);
      categoryAxis.setUpperMargin(0.05);
      categoryAxis.setLowerMargin(0.05);
      categoryAxis.setMaximumCategoryLabelLines(2);

      NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
      rangeAxis.setNumberFormatOverride(new DecimalFormat("#0.00"));
      rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
      rangeAxis.setRange(0, Math.ceil(maxIdentifiedSequence) + 1);
      rangeAxis.setUpperMargin(0.10);

      int additionalHeight = ADDITIONAL_BAR_HEIGHT * identifiedSequences.size() - DEFAULT_HEIGHT;
      if (additionalHeight < 0) {
        additionalHeight = 0;
      }
      ChartUtilities.saveChartAsPNG(outputFile, chart, DEFAULT_WIDTH, DEFAULT_HEIGHT + additionalHeight);
    }
  }

  public static class IdentifiedSequence implements Serializable {
    private static final long serialVersionUID = -4613751538006163608L;
    private final String sequenceName;
    private final double percent;

    public IdentifiedSequence(String sequenceName, double percent) {
      this.sequenceName = sequenceName;
      this.percent = percent;
    }

    public String getSequenceName() {
      return sequenceName;
    }

    public double getPercent() {
      return percent;
    }
  }

  static class FindSequences implements Runnable {
    private Map<String, String> knownSequences;
    private Fragment fragment;
    private Object mutex = new Object();
    private AtomicBoolean stop = new AtomicBoolean(false);
    private TObjectLongHashMap<String> nbKnownSequencesFound;

    public FindSequences(Map<String, String> knownSequences, TObjectLongHashMap<String> nbKnownSequencesFound) {
      this.knownSequences = knownSequences;
      this.nbKnownSequencesFound = nbKnownSequencesFound;
    }

    public void stop() {
      stop.set(true);
      mutex.notifyAll();
    }

    public void setFragment(Fragment fragment) {
      synchronized (mutex) {
        while (this.fragment != null) {
          try {
            mutex.wait();
          } catch (InterruptedException e) {
          }
        }
        this.fragment = fragment;
        mutex.notifyAll();
      }
    }

    @Override
    public void run() {
      try {
        while (!stop.get()) {
          synchronized (mutex) {
            while (this.fragment == null) {
              try {
                mutex.wait();
              } catch (InterruptedException e) {
              }
            }

            for (Entry<String, String> entry : knownSequences.entrySet()) {
              String knownName = entry.getKey();
              String knownSeq = entry.getValue();
              for (Read read : this.fragment) {
                if (read != null && read.getSequence().contains(knownSeq)) {
                  nbKnownSequencesFound.increment(knownName);
                  continue;
                }
              }
            }
            this.fragment = null;
            mutex.notifyAll();
          }
        }
      } catch (Exception e) {
        System.err.println("A problem occured " + e);
        e.printStackTrace();
      }
    }
  }
}
