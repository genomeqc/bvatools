/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.readsQC;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.obiba.bitwise.BitVector;

import ca.mcgill.genome.bvatools.filterdups.DuplicateKmerBuilder;
import ca.mcgill.genome.bvatools.filterdups.SequenceLengthException;
import ca.mcgill.genome.bvatools.parsers.Fragment;
import ca.mcgill.genome.bvatools.parsers.Read;

import gnu.trove.map.hash.TObjectIntHashMap;
import htsjdk.samtools.util.StringUtil;

public class DuplicateKMerPercentPlugin extends DefaultQCPlugin {
  /**
   * Small interface that provides access to the physical location information about a cluster.
   * All values should be defaulted to -1 if unavailable.  ReadGroup and Tile should only allow
   * non-zero positive integers, x and y coordinates may be negative.
   */
  public static interface PhysicalLocation {
    short getReadGroup();

    void setReadGroup(short rg);

    short getTile();

    void setTile(short tile);

    short getX();

    void setX(short x);

    short getY();

    void setY(short y);
  }

  private static final String DEFAULT_READ_NAME_REGEX = "[a-zA-Z0-9]+:[0-9]:([0-9]+):([0-9]+):([0-9]+).*".intern();
  private static final int DEFAULT_HEIGHT = 200;
  private static final int ESTIMATED_MAX_ITEMS = 45000000; // 1 lane of GA
  private static final int MAX_DISPLAYED_DUPLICATES = 10;
  private transient TObjectIntHashMap<BitVector> nbKnownSequencesFound;
  private transient HashMap<BitVector, List<PhysicalLocation>> duplicateClusterLocations = null;
  private int kmerSize = 20;
  private int offset = 15;
  private long totalNbReads = 0;
  private int totalNbDuplicatedReads = 0;
  private double percentDup = -1;
  private boolean containsMultiReadFragment = false;
  private String readNameRegEx = DEFAULT_READ_NAME_REGEX;
  private Pattern readNamePattern = null;
  private int opticalDuplicateDistance = 100;
  private final boolean testOpticalDuplicates;
  private double percentOpticalDup = -1;

  public DuplicateKMerPercentPlugin() {
    this(false);
  }

  public DuplicateKMerPercentPlugin(boolean testOpticalDuplicates) {
    this.testOpticalDuplicates = testOpticalDuplicates;
  }

  public boolean longEnoughReads(Fragment fragment) {
    int kmerBasesPerRead = kmerSize / fragment.size();
    // All reads need to be > than kmer + offset
    for (int idx = 0; idx < fragment.size(); idx++) {
      Read read = fragment.getRead(idx);
      if (read != null) {
        if (read.getBaseQualities().length < (kmerBasesPerRead + offset)) {
          return false;
        }
      }
    }
    return true;
  }

  public boolean isTestOpticalDuplicates() {
    return testOpticalDuplicates;
  }

  @Override
  public void processSequence(Fragment fragment) {
    if (!longEnoughReads(fragment)) {
      return;
    }
    if (fragment.size() > 1)
      containsMultiReadFragment = true;

    int estimatedItems = (int) (ESTIMATED_MAX_ITEMS / 0.75 + 1.0);
    if (nbKnownSequencesFound == null)
      nbKnownSequencesFound = new TObjectIntHashMap<BitVector>(estimatedItems, 0.75f);
    if (duplicateClusterLocations == null && testOpticalDuplicates)
      duplicateClusterLocations = new HashMap<BitVector, List<PhysicalLocation>>(estimatedItems, 0.75f);

    totalNbReads++;
    BitVector twoBitSequence;
    try {
      twoBitSequence = DuplicateKmerBuilder.computeKmer(offset, kmerSize, fragment);
    } catch (SequenceLengthException e) {
      throw new RuntimeException("Kmer too long, or offset to big: " + fragment.getName());
    }

    // Keep this for debugging purposes
    // if(pair1.toString().equals(pair2.toString())) {
    // System.err.println("They are equal: " + sequence.getHeader());
    // }
    if (!nbKnownSequencesFound.containsKey(twoBitSequence)) {
      nbKnownSequencesFound.put(twoBitSequence, 1);
      if (duplicateClusterLocations != null) {
        PhysicalLocation loc = new ClusterLocation();
        addLocationInformation(fragment.getName(), loc);
        List<PhysicalLocation> newList = new ArrayList<PhysicalLocation>(1);
        newList.add(loc);
        duplicateClusterLocations.put(twoBitSequence, newList);
      }
    } else {
      int value = nbKnownSequencesFound.get(twoBitSequence);
      value++;
      if (value == 2) {
        totalNbDuplicatedReads++;
      }
      nbKnownSequencesFound.put(twoBitSequence, value);

      if (duplicateClusterLocations != null) {
        PhysicalLocation loc = new ClusterLocation();
        addLocationInformation(fragment.getName(), loc);
        duplicateClusterLocations.get(twoBitSequence).add(loc);
      }
    }
  }

  public double getPercentDuplication() {
    computeDuplication();
    return percentDup;
  }

  public double getPercentOpticalDuplication() {
    computeDuplication();
    return percentOpticalDup;
  }

  public void computeDuplication() {
    if (percentDup > -1)
      return;
    if (nbKnownSequencesFound == null) {
      percentDup = 0;
    } else {
      double nbKmers = nbKnownSequencesFound.size();
      double total = totalNbReads;
      double nbDuplicateReads = total - nbKmers;
      percentDup = nbDuplicateReads * 100.0 / total;

      double nbOpticalDuplicates = countOpticalDuplicates();
      if (nbOpticalDuplicates != -1) {
        percentOpticalDup = nbOpticalDuplicates * 100.0 / nbDuplicateReads;
      }
    }
  }

  private long countOpticalDuplicates() {
    if (duplicateClusterLocations == null) {
      return -1;
    }

    long nbOpticalDups = 0;
    for (List<PhysicalLocation> list : duplicateClusterLocations.values()) {
      list.sort(Comparator.comparingInt(PhysicalLocation::getReadGroup).thenComparingInt(PhysicalLocation::getTile).thenComparingInt(PhysicalLocation::getX)
          .thenComparingInt(PhysicalLocation::getY));

      int length = list.size();
      long countPerList = 0;
      boolean optDups[] = new boolean[length];
      for (int i = 0; i < length; ++i) {
        PhysicalLocation lhs = list.get(i);
        if (lhs.getTile() < 0 || optDups[i]) {
          continue;
        }

        for (int j = i + 1; j < length; ++j) {
          PhysicalLocation rhs = list.get(j);

          if (lhs.getReadGroup() != rhs.getReadGroup()) {
            break;
          }
          if (lhs.getTile() != rhs.getTile()) {
            break;
          }
          if (rhs.getX() > lhs.getX() + opticalDuplicateDistance) {
            break;
          }

          // Not true distance, but close enough and faster to compute
          if (Math.abs(lhs.getY() - rhs.getY()) <= opticalDuplicateDistance) {
            countPerList++;
            optDups[i] = true;
            if (!optDups[j]) {
              countPerList++;
              optDups[j] = true;
            }
            break;
          }
        }
      }

      // All can't be opt dup
      if (countPerList == length) {
        countPerList--;
      }
      nbOpticalDups += countPerList;
    }
    return nbOpticalDups;
  }

  public int getKmerSize() {
    return kmerSize;
  }

  public void setKmerSize(int kmerSize) {
    this.kmerSize = kmerSize;
  }

  public int getOffset() {
    return offset;
  }

  public void setOffset(int offset) {
    this.offset = offset;
  }

  @Override
  public void generateGraph(File outputDirectory) throws IOException {
    // no graph, only duplication rate stat in xml file
  }

  private static class DuplicateRead implements Comparable<DuplicateRead> {
    private final BitVector sequence;
    private final int count;

    public DuplicateRead(BitVector sequence, int count) {
      this.sequence = sequence;
      this.count = count;
    }

    public int getCount() {
      return count;
    }

    public String getSequenceString() {
      char nucleotides[] = new char[sequence.size() / 3];

      for (int idx = 0; idx < sequence.size(); idx += 3) {
        byte value = 0;
        if (sequence.get(idx)) {
          value += 4;
        }
        if (sequence.get(idx + 1)) {
          value += 2;
        }
        if (sequence.get(idx + 2)) {
          value += 1;
        }

        switch (value) {
        case 0:
          // 000
          nucleotides[idx / 3] = 'A';
          break;
        case 1:
          // 001
          nucleotides[idx / 3] = 'C';
          break;
        case 2:
          // 010
          nucleotides[idx / 3] = 'G';
          break;
        case 3:
          // 011
          nucleotides[idx / 3] = 'T';
          break;
        case 4:
          // 100
          nucleotides[idx / 3] = 'N';
          break;
        default:
          throw new RuntimeException("Unknown value: " + value);
        }
      }
      return String.valueOf(nucleotides);
    }

    @Override
    public int compareTo(DuplicateRead o) {
      return count - o.count;
    }

    @Override
    public int hashCode() {
      return sequence.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj)
        return true;
      if (obj == null)
        return false;
      if (getClass() != obj.getClass())
        return false;
      DuplicateRead other = (DuplicateRead) obj;
      return sequence.equals(other.sequence);
    }
  }

  private final String[] tmpLocationFields = new String[10];

  private boolean addLocationInformation(final String readName, final PhysicalLocation loc) {
    // Optimized version if using the default read name regex (== used on purpose):
    if (readNameRegEx == DEFAULT_READ_NAME_REGEX) {
      final int fields = StringUtil.split(readName, tmpLocationFields, ':');
      if (fields < 5) {
        throw new RuntimeException(String.format("Default READ_NAME_REGEX '%s' did not match read name '%s'.  "
            + "You may need to specify a READ_NAME_REGEX in order to correctly identify optical duplicates.  "
            + "Note that this message will not be emitted again even if other read names do not match the regex.", readNameRegEx, readName));
      }

      loc.setTile((short) rapidParseInt(tmpLocationFields[2]));
      loc.setX((short) rapidParseInt(tmpLocationFields[3]));
      loc.setY((short) rapidParseInt(tmpLocationFields[4]));
      return true;
    } else if (readNameRegEx == null) {
      return false;
    } else {
      // Standard version that will use the regex
      if (readNamePattern == null)
        readNamePattern = Pattern.compile(readNameRegEx);

      final Matcher m = readNamePattern.matcher(readName);
      if (m.matches()) {
        loc.setTile((short) Integer.parseInt(m.group(1)));
        loc.setX((short) Integer.parseInt(m.group(2)));
        loc.setY((short) Integer.parseInt(m.group(3)));
        return true;
      } else {
        throw new RuntimeException(String.format("READ_NAME_REGEX '%s' did not match read name '%s'.  Your regex may not be correct.  "
            + "Note that this message will not be emitted again even if other read names do not match the regex.", readNameRegEx, readName));
      }
    }
  }

  /**
   * Very specialized method to rapidly parse a sequence of digits from a String up until the first non-digit character.
   * Does not handle negative numbers.
   */
  private final int rapidParseInt(final String input) {
    final int len = input.length();
    int val = 0;

    for (int i = 0; i < len; ++i) {
      final char ch = input.charAt(i);
      if (Character.isDigit(ch)) {
        val = (val * 10) + (ch - 48);
      }
    }

    return val;
  }

  private static class ClusterLocation implements PhysicalLocation {
    private short readGroup = -1;
    private short tile = -1;
    private short x = -1, y = -1;

    @Override
    public short getReadGroup() {
      return this.readGroup;
    }

    @Override
    public void setReadGroup(final short readGroup) {
      this.readGroup = readGroup;
    }

    @Override
    public short getTile() {
      return this.tile;
    }

    @Override
    public void setTile(final short tile) {
      this.tile = tile;
    }

    @Override
    public short getX() {
      return this.x;
    }

    @Override
    public void setX(final short x) {
      this.x = x;
    }

    @Override
    public short getY() {
      return this.y;
    }

    @Override
    public void setY(final short y) {
      this.y = y;
    }
  }
}
