/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.readsQC;

import gnu.trove.list.array.TLongArrayList;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import ca.mcgill.genome.bvatools.parsers.Fragment;
import ca.mcgill.genome.bvatools.parsers.Read;

public class KmerPositionPlugin extends DefaultQCPlugin {
  private final List<TLongArrayList> readPositionsCount = new ArrayList<TLongArrayList>();
  private String kmerToFind = "";
  private String title = "Kmer Position";
  private String suffix = "";
  private long totalNbReads = 0;;

  public String getKmerToFind() {
    return kmerToFind;
  }

  public void setKmerToFind(String kmerToFind) {
    this.kmerToFind = kmerToFind;
  }

  public String getTitle() {
    return title;
  }

  public String getSuffix() {
    return suffix;
  }

  public void setSuffix(String suffix) {
    this.suffix = suffix;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  @Override
  public void processSequence(Fragment fragment) {
    totalNbReads++;

    while(fragment.size() > readPositionsCount.size()) {
      readPositionsCount.add(new TLongArrayList());
    }
    
    for(int readIdx=0; readIdx < fragment.size(); readIdx++) {
      Read read = fragment.getRead(readIdx);
      if(read == null)
        continue;
      TLongArrayList positionsCount = readPositionsCount.get(readIdx);

      int idx = read.getSequence().indexOf(kmerToFind);
      if(idx != -1) {
        long seqLength = read.getSequence().length();
        for (int i = positionsCount.size(); i < seqLength; i++) {
          positionsCount.add(0l);
    	  }
        while (idx >= 0) {
          long val = positionsCount.get(idx);
          val++;
          positionsCount.set(idx, val);
          idx = read.getSequence().indexOf(kmerToFind, idx + 1);
        }
      }
    }
  }

  @Override
  public void generateGraph(File outputDirectory) throws IOException {
    TLongArrayList positionsCount = new TLongArrayList();
    for(TLongArrayList readPosCount : readPositionsCount) {
      positionsCount.addAll(readPosCount);
    }

    if (positionsCount.size() == 0)
      return;

    XYSeries positionsCountSerie = new XYSeries("Nb Sequences");
    XYSeries positionsPctSerie = new XYSeries("% Sequences");

    for (int i = 1; i < positionsCount.size() + 1; i++) {
      positionsCountSerie.add(i, (double) positionsCount.get(i - 1));
      positionsPctSerie.add(i, (double) positionsCount.get(i - 1) / (double) totalNbReads * 100.0d);
    }

    XYSeriesCollection contentCollection = new XYSeriesCollection();
    contentCollection.addSeries(positionsCountSerie);
    XYSeriesCollection pctCollection = new XYSeriesCollection();
    pctCollection.addSeries(positionsPctSerie);

    NumberAxis xAxis = new UnlimitedTicksNumberAxis("Bases");
    xAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
    xAxis.setUpperBound(positionsCount.size()+1);

    NumberAxis yAxis = new UnlimitedTicksNumberAxis("Nb Sequences");

    XYLineAndShapeRenderer contentRenderer = new XYLineAndShapeRenderer(true, false);
    contentRenderer.setSeriesPaint(0, Color.BLUE);
    XYPlot contentPlot = new XYPlot(contentCollection, xAxis, yAxis, contentRenderer);

    contentPlot.setBackgroundPaint(Color.white);

    NumberAxis pctAxis = new NumberAxis("%");
    pctAxis.setAutoRangeIncludesZero(true);
    pctAxis.setNumberFormatOverride(new DecimalFormat("#0.00"));
    contentPlot.setRangeAxis(1, pctAxis);
    contentPlot.setDataset(1, pctCollection);
    contentPlot.mapDatasetToRangeAxis(1, 1);
    XYLineAndShapeRenderer pctRenderer = new XYLineAndShapeRenderer();
    pctRenderer.setSeriesPaint(0, Color.BLUE);
    pctRenderer.setSeriesLinesVisible(0, false);
    pctRenderer.setSeriesShapesVisible(0, false);
    contentPlot.setRenderer(1, pctRenderer);

    JFreeChart chart;
    chart = new JFreeChart(title, JFreeChart.DEFAULT_TITLE_FONT, contentPlot, false);
    chart.setBackgroundPaint(Color.white);

    File outputFile = new File(outputDirectory, generateGraphFileName("05kmerPositions" + suffix, ".png"));
    int width = 60 + positionsCount.size();
    if (width < DEFAULT_WIDTH) {
      width = DEFAULT_WIDTH;
    }
    ChartUtilities.saveChartAsPNG(outputFile, chart, width, 600);
  }
}
