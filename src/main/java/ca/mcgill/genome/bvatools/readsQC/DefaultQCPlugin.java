/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.readsQC;

import java.io.File;
import java.io.IOException;

import ca.mcgill.genome.bvatools.parsers.Fragment;

/**
 *
 */
public abstract class DefaultQCPlugin implements IQCPlugin {
  public static final int DEFAULT_WIDTH = 1152;
  public static final String graphPrefix = "mpsQC";
  public String regionName = "";

  /**
   * {@inheritDoc}
   */
  @Override
  public abstract void processSequence(Fragment sequence);

  /**
   * {@inheritDoc}
   */
  @Override
  public abstract void generateGraph(File outputDirectory) throws IOException;

  /**
   * {@inheritDoc}
   */
  @Override
  public void setOutputRegionName(String regionName) {
    this.regionName = regionName;
  }

  protected String generateGraphFileName(String graphName, String extension) {
    if (graphName.indexOf('_') != -1) {
      throw new IllegalArgumentException("graphName cannot contain the '_' charater");
    }

    String retVal = graphPrefix + '_' + regionName + '_' + graphName + extension;
    return retVal;
  }

  /**
   * Example of graphFileName mpsQC_3_Index-3.C_0868_R1_05sequenceNContent.png
   * @param graphFileName
   * @return
   */
  public static String getRegionName(String graphFileName) {
    int prefix = graphFileName.indexOf('_') + 1;
    int suffix = graphFileName.lastIndexOf('_');
    String retVal = null;
    if (prefix != 0 && suffix != -1 && prefix != suffix) {
      retVal = graphFileName.substring(prefix, suffix);
    }

    return retVal;
  }
}
