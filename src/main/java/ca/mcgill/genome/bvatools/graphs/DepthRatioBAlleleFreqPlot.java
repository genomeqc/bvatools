/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.graphs;

import gnu.trove.map.TObjectLongMap;
import gnu.trove.map.hash.TObjectLongHashMap;
import htsjdk.tribble.Feature;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.imageio.ImageIO;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.IntervalMarker;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.graphics2d.svg.SVGGraphics2D;
import org.jfree.graphics2d.svg.SVGUtils;
import org.jfree.ui.RectangleAnchor;

public class DepthRatioBAlleleFreqPlot {
  public static enum DepthRatioBAF {
    DEPTH_RATIO, BAF
  };

  private final LinkedHashMap<String, Feature> genome;
  private final int width;
  private final int height;
  private PointsByRegionDataset ratioPointsDataset;
  private PointsByRegionDataset bafPointsDataset;
  private final TObjectLongMap<String> chrOffset = new TObjectLongHashMap<String>();
  private final boolean generateSVG;

  public DepthRatioBAlleleFreqPlot(LinkedHashMap<String, Feature> genome, int width, int height, boolean generateSVG) {
    this.genome = genome;
    this.width = width;
    this.height = height;
    this.generateSVG = generateSVG;

    init();
  }

  public void init() {
    long offset = 0;
    // The order is important
    List<String> chrNames = new ArrayList<String>(genome.size());
    for (String chrName : genome.keySet()) {
      chrNames.add(chrName);
      Feature chr = genome.get(chrName);
      chrOffset.put(chr.getChr(), offset);
      offset += chr.getEnd();
    }

    ratioPointsDataset = new PointsByRegionDataset(chrNames, chrOffset);
    bafPointsDataset = new PointsByRegionDataset(chrNames, chrOffset);
  }

  public void addPoint(DepthRatioBAF lrrBaf, String chr, int position, double value) {
    if (!ratioPointsDataset.containsRegion(chr))
      return;

    if (lrrBaf == DepthRatioBAF.DEPTH_RATIO) {
      ratioPointsDataset.add(chr,position, value);
    } else {
      bafPointsDataset.add(chr,position, value);
    }
  }

  public void write(String outputPrefix) throws IOException {
    write(outputPrefix, true);
  }

  /**
   * 
   * @param outputPrefix
   * @param addSmoothLine Does nothing at the moment.
   * @throws IOException
   */
  public void write(String outputPrefix, boolean addSmoothLine) throws IOException {
    ratioPointsDataset.setCurrentRegionToOutput(null);
    bafPointsDataset.setCurrentRegionToOutput(null);
    chart2Image(outputPrefix, true);

    for (String chr : chrOffset.keySet()) {
      ratioPointsDataset.setCurrentRegionToOutput(chr);
      bafPointsDataset.setCurrentRegionToOutput(chr);
      if (ratioPointsDataset.getItemCount(0) == 0)
        continue;

      // if(addSmoothLine) {
      // LoessInterpolator loessInterpolator=new LoessInterpolator(0.66,2);
      // double x[] = new double[chr2Points.get(chr).size()];
      // double y[] = new double[chr2Points.get(chr).size()];
      // Collections.sort(chr2Points.get(chr), new Comparator<Point>() {
      // @Override
      // public int compare(Point o1, Point o2) {
      // return o1.x-o2.x;
      // }
      // });
      // for(int idx=0; idx < chr2Points.get(chr).size();idx ++) {
      // x[idx] = chr2Points.get(chr).get(idx).getX();
      // y[idx] = chr2Points.get(chr).get(idx).getY();
      // }
      //
      // double ySmoothed[] = loessInterpolator.smooth(x, y);
      // for(int idx=0; idx < chr2Points.get(chr).size();idx ++) {
      // int trueX = (int)(((long)width-1l)*(long)x[idx]/genome.getChromosome(chr).getEnd());
      // chr2Image.get(chr).setRGB((int)trueX, (int)ySmoothed[idx], Color.ORANGE.getRGB());
      // }
      // }

      chart2Image(outputPrefix + '.' + chr, false);
    }
  }

  private void chart2Image(String outputPrefix, boolean addChromosomesSep) throws IOException {

    JFreeChart lrrChart = ChartFactory.createScatterPlot("Depth Ratio", "bp", "Ratio", ratioPointsDataset, PlotOrientation.VERTICAL, false, false, false);
    XYPlot lrrPlot = lrrChart.getXYPlot();
    lrrPlot.setBackgroundPaint(Color.WHITE);
    //lrrPlot.setRenderer(new XYDotRenderer());
    lrrPlot.getRenderer().setSeriesPaint(0, new Color(0.75f,0,0,0.25f));
    lrrPlot.getRenderer().setSeriesShape(0, new Ellipse2D.Double(-0.5, -0.5, 0.5, 0.5));
    lrrPlot.getRangeAxis().setRange(-2, 2);

    JFreeChart bafChart = ChartFactory.createScatterPlot("B Allele Frequency", "bp", "BAF", bafPointsDataset, PlotOrientation.VERTICAL, false, false, false);
    XYPlot bafPlot = bafChart.getXYPlot();
    bafPlot.setBackgroundPaint(Color.WHITE);
    //bafPlot.setRenderer(new XYDotRenderer());
    bafPlot.getRenderer().setSeriesPaint(0, new Color(0.75f,0,0,0.25f));
    bafPlot.getRenderer().setSeriesShape(0, new Ellipse2D.Double(-0.5, -0.5, 0.5, 0.5));
    NumberAxis domainAxis = (NumberAxis) bafPlot.getDomainAxis();
    domainAxis.setAutoRangeIncludesZero(true);
    bafPlot.getRangeAxis().setRange(0, 1);

    if (addChromosomesSep) {
      for (String chr : chrOffset.keySet()) {
        IntervalMarker marker = new IntervalMarker(chrOffset.get(chr), chrOffset.get(chr) + genome.get(chr).getEnd(), new Color(255, 255, 255, 0), new BasicStroke(3.0f), Color.BLACK, new BasicStroke(0), 1f);
        marker.setLabel(chr);
        marker.setLabelAnchor(RectangleAnchor.CENTER);
        marker.setLabelPaint(Color.BLACK);
        Font markerFont = marker.getLabelFont().deriveFont(Font.BOLD).deriveFont(14.0f);
        marker.setLabelFont(markerFont);
        lrrPlot.addDomainMarker(marker);
        bafPlot.addDomainMarker(marker);
      }
    }

    BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    Graphics2D g = (Graphics2D) img.getGraphics();
    Rectangle2D lrrRect = new Rectangle2D.Double(0, 0, width, height / 2);
    lrrChart.draw(g, lrrRect);
    Rectangle2D bafRect = new Rectangle2D.Double(0, height / 2 + 1, width, height / 2);
    bafChart.draw(g, bafRect);
    g.dispose();
    ImageIO.write(img, "png", new File(outputPrefix + ".png"));

    if(generateSVG) {
      SVGGraphics2D g2 = new SVGGraphics2D(width, height);
      lrrChart.draw(g2, lrrRect);
      bafChart.draw(g2, bafRect);
      SVGUtils.writeToSVG(new File(outputPrefix + ".svg"), g2.getSVGElement());
    }
  }
}
