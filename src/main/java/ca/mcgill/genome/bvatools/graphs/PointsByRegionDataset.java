/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.graphs;

import gnu.trove.map.TObjectLongMap;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jfree.data.DomainOrder;
import org.jfree.data.xy.AbstractXYDataset;

public class PointsByRegionDataset extends AbstractXYDataset {
  private static final long serialVersionUID = 5256946864077875087L;
  private static String EMPTY_SERIES = "Empty";

  private final List<String> regionOrder;
  private TObjectLongMap<String> regionOffset;
  private Map<String, Map<String, PointDataset>> series2Region2PointDataset;
  private String currentRegionToOutput;
  private final List<String> seriesName;

  public PointsByRegionDataset(List<String> regionOrder, TObjectLongMap<String> regionOffset) {
    this(Collections.singletonList(EMPTY_SERIES), regionOrder, regionOffset);
  }
  
  public PointsByRegionDataset(List<String> seriesName, List<String> regionOrder) {
    this(seriesName, regionOrder, null);
  }

  public PointsByRegionDataset(List<String> seriesName, List<String> regionOrder, TObjectLongMap<String> regionOffset) {
    this.regionOrder = regionOrder;
    this.regionOffset = regionOffset;
    this.seriesName = seriesName;
    this.series2Region2PointDataset = new HashMap<String, Map<String, PointDataset>>();
    
    for(String serie : seriesName) {
      series2Region2PointDataset.put(serie, new HashMap<String, PointDataset>());
      for (String chrName : regionOrder) {
        series2Region2PointDataset.get(serie).put(chrName, new PointDataset());
      }
    }
  }

  public TObjectLongMap<String> getRegionOffset() {
    return regionOffset;
  }

  public void setRegionOffset(TObjectLongMap<String> regionOffset) {
    this.regionOffset = regionOffset;
  }

  public boolean containsRegion(String region) {
    return this.containsRegion(EMPTY_SERIES, region);
  }

  public boolean containsRegion(String series, String region) {
    return series2Region2PointDataset.get(series).containsKey(region);
  }

  public String getCurrentRegionToOutput() {
    return currentRegionToOutput;
  }

  public void setCurrentRegionToOutput(String currentRegionToOutput) {
    this.currentRegionToOutput = currentRegionToOutput;
  }

  public void add(String region, double x, double y) {
    this.add(EMPTY_SERIES, region, x, y);
  }
  
  public void add(String serie, String region, double x, double y) {
    series2Region2PointDataset.get(serie).get(region).add(x, y);
  }
  
  @Override
  public Number getY(int series, int item) {
    if(currentRegionToOutput == null) {
      int nbItems=0;
      for(String region : regionOrder) {
        int regionItemCount = series2Region2PointDataset.get(seriesName.get(series)).get(region).getItemCount(series);
        if(nbItems+regionItemCount > item) {
          return series2Region2PointDataset.get(seriesName.get(series)).get(region).getY(series, item-nbItems);
        }
        nbItems += regionItemCount;
      }
      throw new ArrayIndexOutOfBoundsException("Item not found");
    }
    else {
      return series2Region2PointDataset.get(seriesName.get(series)).get(currentRegionToOutput).getY(series, item);
    }
  }

  @Override
  public Number getX(int series, int item) {
    if(currentRegionToOutput == null) {
      int nbItems=0;
      for(String region : regionOrder) {
        int regionItemCount = series2Region2PointDataset.get(seriesName.get(series)).get(region).getItemCount(series);
        if(nbItems+regionItemCount > item) {
          return series2Region2PointDataset.get(seriesName.get(series)).get(region).getX(series, item-nbItems).longValue()+regionOffset.get(region);
        }
        nbItems += regionItemCount;
      }
      throw new ArrayIndexOutOfBoundsException("Item not found");
    }
    else {
      return series2Region2PointDataset.get(seriesName.get(series)).get(currentRegionToOutput).getX(series, item);
    }

  }

  @Override
  public int getItemCount(int series) {
    if(currentRegionToOutput == null) {
      int nbItems=0;
      for(String region : regionOrder) {
        nbItems += series2Region2PointDataset.get(seriesName.get(series)).get(region).getItemCount(series);
      }
      return nbItems;
    }
    else {
      return series2Region2PointDataset.get(seriesName.get(series)).get(currentRegionToOutput).getItemCount(series);
    }
  }

  @Override
  public Comparable<?> getSeriesKey(int series) {
    return seriesName.get(series);
  }

  @Override
  public int getSeriesCount() {
    return seriesName.size();
  }

  @Override
  public DomainOrder getDomainOrder() {
    return DomainOrder.NONE;
  }
}
