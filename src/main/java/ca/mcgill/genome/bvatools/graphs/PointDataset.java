/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.graphs;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.jfree.data.DomainOrder;
import org.jfree.data.xy.AbstractXYDataset;

public class PointDataset extends AbstractXYDataset {
  private static final long serialVersionUID = 1843542156686450049L;

  private List<Point2D.Double> points = new ArrayList<Point2D.Double>();

  public void add(double x, double y) {
    points.add(new Point2D.Double(x, y));
  }

  @Override
  public Number getY(int series, int item) {
    return points.get(item).getY();
  }

  @Override
  public Number getX(int series, int item) {
    return points.get(item).getX();
  }

  @Override
  public int getItemCount(int series) {
    return points.size();
  }

  @Override
  public Comparable<?> getSeriesKey(int series) {
    return 0;
  }

  @Override
  public int getSeriesCount() {
    return 1;
  }

  @Override
  public DomainOrder getDomainOrder() {
    return DomainOrder.NONE;
  }
}
