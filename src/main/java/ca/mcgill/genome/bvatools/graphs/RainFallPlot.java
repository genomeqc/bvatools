/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.graphs;

import gnu.trove.map.TObjectLongMap;
import gnu.trove.map.hash.TObjectLongHashMap;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.LogarithmicAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.IntervalMarker;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.AbstractRenderer;
import org.jfree.graphics2d.svg.SVGGraphics2D;
import org.jfree.graphics2d.svg.SVGUtils;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.RectangleEdge;

import htsjdk.tribble.Feature;
import scala.actors.threadpool.Arrays;

public class RainFallPlot {
  public static final Map<String,String> key2Class = new HashMap<String,String>();
  public static final Map<String,Color> class2Color = new HashMap<String,Color>();
  public static final String classOrder[]      = {       "C>A",                 "C>G",                "C>T",                "T>A",                  "T>C",                    "T>G" };
  public static final Color  classColorOrder[] = { new Color(36,87,168), new Color(35,31,32), new Color(237,79,40), new Color(173,125,177), new Color(243,238,118), new Color(154,216,133)};
  
  private final LinkedHashMap<String, Feature> genome;
  private final int width;
  private final int height;
  private PointsByRegionDataset pointsDataset;
  private final TObjectLongMap<String> chrOffset = new TObjectLongHashMap<String>();
  private final boolean generateSVG;

  static {
    key2Class.put("A>T", "T>A");
    key2Class.put("A>C", "T>G");
    key2Class.put("A>G", "T>G");
    key2Class.put("T>A", "T>A");
    key2Class.put("T>C", "T>C");
    key2Class.put("T>G", "T>G");
    key2Class.put("C>A", "C>A");
    key2Class.put("C>G", "C>G");
    key2Class.put("C>T", "C>T");
    key2Class.put("G>A", "C>T");
    key2Class.put("G>C", "C>G");
    key2Class.put("G>T", "C>A");
  }

  public RainFallPlot(LinkedHashMap<String, Feature> genome, int width, int height, boolean generateSVG) {
    this.genome = genome;
    this.width = width;
    this.height = height;
    this.generateSVG = generateSVG;

    init();
  }

  @SuppressWarnings("unchecked")
  public void init() {
    // The order is important
    Map<String, PointDataset> chr2Dataset = new HashMap<String, PointDataset>();
    List<String> chrNames = new ArrayList<String>(genome.size());
    for (String chrName : genome.keySet()) {
      chrNames.add(chrName);
      Feature chr = genome.get(chrName);
      chrOffset.put(chr.getChr(), -1);

      chr2Dataset.put(chr.getChr(), new PointDataset());
    }

    pointsDataset = new PointsByRegionDataset(Arrays.asList(classOrder), chrNames);
  }

  public void addPoint(String substitution, String chr, int variantIdx, double value) {
    if (!pointsDataset.containsRegion(key2Class.get(substitution), chr))
      return;

    if(chrOffset.get(chr) == -1)
      chrOffset.put(chr, variantIdx);

    if(value == 0)
      value = 0.1;
    pointsDataset.add(key2Class.get(substitution), chr, variantIdx-chrOffset.get(chr), value);
  }

  public void write(String outputPrefix) throws IOException {
    write(outputPrefix, true);
  }

  /**
   * 
   * @param outputPrefix
   * @param addSmoothLine Does nothing at the moment.
   * @throws IOException
   */
  public void write(String outputPrefix, boolean addSmoothLine) throws IOException {
    long prevIdx=0;
    for(String chr : genome.keySet()) {
      if(chrOffset.get(chr) == -1)
        chrOffset.put(chr, prevIdx);
      else
        prevIdx = chrOffset.get(chr);
    }

    pointsDataset.setRegionOffset(chrOffset);
    pointsDataset.setCurrentRegionToOutput(null);
    chart2Image(outputPrefix, true);

    for (String chr : chrOffset.keySet()) {
      pointsDataset.setCurrentRegionToOutput(chr);
      boolean foundNonZero = false;
      for(int idx=0; idx < classOrder.length ; idx++) {
        if (pointsDataset.getItemCount(idx) > 0) {
          foundNonZero = true;
          break;
        }
      }
      if(foundNonZero)
        chart2Image(outputPrefix + '.' + chr, false);
    }
  }

  private void chart2Image(String outputPrefix, boolean addChromosomesSep) throws IOException {

    JFreeChart chart = ChartFactory.createScatterPlot("Delta Distance", "Variant Index", "Distance", pointsDataset, PlotOrientation.VERTICAL, true, false, false);
    chart.getLegend().setPosition(RectangleEdge.BOTTOM); 
    XYPlot plot = chart.getXYPlot();
    plot.setBackgroundPaint(Color.WHITE);
    //plot.setRenderer(new XYDotRenderer());
    for(int idx=0; idx < classOrder.length ; idx++) {
      plot.getRenderer().setSeriesPaint(idx, classColorOrder[idx]);
      plot.getRenderer().setSeriesShape(idx, new Ellipse2D.Double(-2, -2, 2, 2));
      ((AbstractRenderer)plot.getRenderer()).setBaseLegendShape(new Ellipse2D.Double(-3, -3, 3, 3));
    }
//    plot.getDomainAxis().setTickLabelsVisible(true);
    
    final LogarithmicAxis rangeAxis = new LogarithmicAxis("Distance");
    rangeAxis.setLog10TickLabelsFlag(true);
    rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
    rangeAxis.setAutoRangeStickyZero(true);
    plot.setRangeAxis(rangeAxis);

    if (addChromosomesSep) {
      for (String chr : genome.keySet()) {
        long curPos = chrOffset.get(chr);
        if(curPos < 1)
          continue;
        IntervalMarker marker = new IntervalMarker(curPos, curPos, new Color(255, 255, 255, 0), new BasicStroke(3.0f), Color.BLACK, new BasicStroke(0), 1f);
        marker.setLabel(chr);
        marker.setLabelAnchor(RectangleAnchor.TOP);
        marker.setLabelPaint(Color.BLACK);
        Font markerFont = marker.getLabelFont().deriveFont(Font.BOLD).deriveFont(14.0f);
        marker.setLabelFont(markerFont);
        plot.addDomainMarker(marker);
      }
    }

    BufferedImage img = chart.createBufferedImage(width, height, BufferedImage.TYPE_INT_RGB, null);
    ImageIO.write(img, "png", new File(outputPrefix + ".png"));

    if(generateSVG) {
      SVGGraphics2D g2 = new SVGGraphics2D(width, height);
      chart.draw(g2, new Rectangle(width, height));
      SVGUtils.writeToSVG(new File(outputPrefix + ".svg"), g2.getSVGElement());
    }
  }
}
