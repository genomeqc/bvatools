/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.depth;

import java.util.Arrays;
import java.util.List;

import htsjdk.samtools.reference.ReferenceSequenceFile;
import htsjdk.samtools.AlignmentBlock;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.util.CoordMath;
import ca.mcgill.genome.bvatools.depth.RegionDepthCounter.RangeFilter;

public class ReadDepthHandler {
  private final DepthInterval interval;
  private final boolean ommitN;
  private final boolean computeGC;
  private final int minMappingQuality;
  private QualityFilter filter;
  private RegionDepthCounter depthCounter;

  public ReadDepthHandler(DepthInterval interval, boolean ommitN, boolean computeGC, int minMappingQuality, int minBaseQuality) {
    // DepthInterval IS NOT thread-safe. Might need to clone.
    this.interval = interval;
    this.ommitN = ommitN;
    this.computeGC = computeGC;
    this.minMappingQuality = minMappingQuality;
    this.filter = new QualityFilter((byte) minBaseQuality);
    this.depthCounter = null;
  }

  public void adjustPosition(RegionDepthCounter depthCounter, int position) {
    List<BasePairCount> binList = depthCounter.adjustToPosition(position);
    for (BasePairCount bpCount : binList) {
      if (bpCount.getBpCount() != BasePairCount.NO_VALUE) {
        interval.incrementNbAccessibleBases();
        interval.incrementDepthBin(bpCount.getBpCount());
      }

      if (bpCount.getBase() != BasePairCount.MISSING_BASE) {
        interval.incrementNbBases();
        if (bpCount.getBase() == 'G' || bpCount.getBase() == 'C' || bpCount.getBase() == 'g' || bpCount.getBase() == 'c') {
          interval.incrementNbGCBases();
        }
      }
    }
  }

  public DepthInterval getInterval() {
    return interval;
  }

  public void initInterval(SAMRecord record, ReferenceSequenceFile fastaRef) {
    depthCounter = new RegionDepthCounter(fastaRef, interval.getChromosome(), interval.getStart(), ommitN);

    int start = record.getAlignmentStart();
    if (start < interval.getStart())
      start = interval.getStart();
    byte bases[];
    if (fastaRef != null) {
      bases = depthCounter.bases(interval.getStart(), start);
    } else {
      bases = new byte[start - interval.getStart() + 1];
      Arrays.fill(bases, BasePairCount.MISSING_BASE);
    }

    for (int basePos = interval.getStart(); basePos < start; basePos++) {
      byte base = bases[basePos - interval.getStart()];
      if (!ommitN || !depthCounter.ommit(base)) {
        interval.incrementNbAccessibleBases();
        interval.incrementDepthBin(0);
      }

      if (computeGC) {
        interval.incrementNbBases();
        if (base == 'G' || base == 'g' || base == 'C' || base == 'c') {
          interval.incrementNbGCBases();
        }
      }
    }
  }

  public void updateDepth(SAMRecord record, ReferenceSequenceFile fastaRef) {
    if (record.getNotPrimaryAlignmentFlag() || record.getReadUnmappedFlag() || record.getDuplicateReadFlag() || record.getMappingQuality() < minMappingQuality)
      return;

    if (depthCounter == null) {
      initInterval(record, fastaRef);
    }

    interval.incrementNbReads();
    if (record.getReadPairedFlag()) {
      interval.incrementNbPairedReads();
      if (record.getProperPairFlag()) {
        interval.incrementNbProperlyPaired();
      }
    }

    int recStart = record.getAlignmentStart();
    if (recStart < interval.getStart())
      recStart = interval.getStart();

    int resize = record.getAlignmentEnd() + record.getAlignmentEnd() - record.getAlignmentStart();
    if (resize > interval.getEnd()) {
      resize = interval.getEnd();
    }

    depthCounter.resizeTo(resize);
    adjustPosition(depthCounter, recStart);

    filter.setQualities(record.getBaseQualities());

    for (AlignmentBlock block : record.getAlignmentBlocks()) {
      int blockStart = block.getReferenceStart();
      if (blockStart > interval.getEnd())
        break;

      if (blockStart < interval.getStart())
        blockStart = interval.getStart();

      int blockEnd = CoordMath.getEnd(block.getReferenceStart(), block.getLength());
      if (blockEnd > interval.getEnd())
        blockEnd = interval.getEnd();

      filter.setBlock(block);
      depthCounter.incrementRangeFiltered(blockStart, blockEnd, filter);
    }
  }

  public void finalizeInterval() {
    if(depthCounter != null) {
      depthCounter.resizeTo(interval.getEnd());
      adjustPosition(depthCounter, interval.getEnd());
    }
  }

  private static class QualityFilter implements RangeFilter {
    private byte qualities[];
    private AlignmentBlock block;
    private final byte minBaseQuality;

    public QualityFilter(byte minBaseQuality) {
      this.minBaseQuality = minBaseQuality;
    }

    @Override
    public boolean isValidToIncrement(int rangeIndex, LinkableBasePairCount basePairCount) {
      // getReadStart is one-based like the rest
      return qualities[block.getReadStart() - 1 + rangeIndex] >= minBaseQuality;
    }

    public void setQualities(byte[] qualities) {
      this.qualities = qualities;
    }

    public void setBlock(AlignmentBlock block) {
      this.block = block;
    }
  }
}
