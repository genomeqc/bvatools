/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.depth;

import gnu.trove.list.TLinkableAdapter;

public class LinkableBasePairCount extends TLinkableAdapter<LinkableBasePairCount> implements BasePairCount {
  private static final long serialVersionUID = 7273875257830849545L;

  private int bpCount;
  private final byte base;

  public LinkableBasePairCount(int value, byte base) {
    bpCount = value;
    this.base = base;
  }

  @Override
  public int getBpCount() {
    return bpCount;
  }

  @Override
  public byte getBase() {
    return base;
  }

  @Override
  public void setBpCount(int bpCount) {
    this.bpCount = bpCount;
  }

  @Override
  public void increment() {
    bpCount++;
  }

  @Override
  public void incrementValid() {
    if (bpCount != NO_VALUE) {
      bpCount++;
    }
  }
}
