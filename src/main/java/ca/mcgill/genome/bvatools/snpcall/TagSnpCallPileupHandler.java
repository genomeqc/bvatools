/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.snpcall;

import gnu.trove.map.TByteIntMap;
import gnu.trove.map.hash.TByteIntHashMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import gnu.trove.set.TByteSet;
import gnu.trove.set.hash.TByteHashSet;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.reference.IndexedFastaSequenceFile;
import htsjdk.samtools.reference.ReferenceSequence;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.mcgill.genome.bvatools.pileup.Pileup;
import ca.mcgill.genome.bvatools.pileup.PileupHandler;

public class TagSnpCallPileupHandler implements PileupHandler {
  private final IndexedFastaSequenceFile refSequence;
  private final int tagLength;
  private int minMappingQuality;
  private int minBaseQuality;
  private int filterTagCount = 0;
  private final List<TagVariant> tagVariants;

  public TagSnpCallPileupHandler(int tagLength, IndexedFastaSequenceFile refSequence, int minMappingQuality, int minBaseQuality) {
    this.tagLength = tagLength;
    this.refSequence = refSequence;
    this.minMappingQuality = minMappingQuality;
    this.minBaseQuality = minBaseQuality;
    this.tagVariants = new ArrayList<TagVariant>();
  }

  public List<TagVariant> getTagVariants() {
    return tagVariants;
  }

  public int getFilterTagCount() {
    return filterTagCount;
  }

  public void setFilterTagCount(int filterTagCount) {
    this.filterTagCount = filterTagCount;
  }

  @Override
  public void handle(Pileup pileup) {
    ReferenceSequence base = refSequence.getSubsequenceAt(pileup.getChromosome(), pileup.getPosition(), pileup.getPosition());
    byte refBase = base.getBases()[0];

    Map<String, TagInfo> tags = new HashMap<String, TagInfo>();
    TByteSet mutantBases = new TByteHashSet();
    for (int idx = 0; idx < pileup.getNbReads(); idx++) {
      int basePosition = pileup.getReadBasePositions().get(idx) - 1;
      SAMRecord read = pileup.getReads().get(idx);
      String tag = read.getReadName().substring(0, tagLength);

      if (read.getMappingQuality() < minMappingQuality)
        continue;

      byte readBase = read.getReadBases()[basePosition];
      byte quality = read.getBaseQualities()[basePosition];
      if (quality < minBaseQuality)
        continue;

      if (!tags.containsKey(tag)) {
        tags.put(tag, new TagInfo(tag));
      }
      TagInfo tagInfo = tags.get(tag);
      tagInfo.getBaseCounts().adjustOrPutValue(readBase, 1, 1);
      if (readBase != refBase) {
        tagInfo.setMutant(true);
        mutantBases.add(readBase);
      }
    }

    if (mutantBases.size() > 0) {
      handleVariant(pileup, refBase, mutantBases.toArray(), tags);
    }
  }

  private void handleVariant(Pileup pileup, byte refBase, byte mutantAlleles[], Map<String, TagInfo> tags) {
    TObjectIntHashMap<String> refTags = new TObjectIntHashMap<String>();
    @SuppressWarnings("unchecked")
    TObjectIntHashMap<String> altTags[] = new TObjectIntHashMap[mutantAlleles.length];
    //TObjectIntHashMap<String> disagreeTags = new TObjectIntHashMap<String>();
    int nbDisagreeingTags = 0;
    int refReadCounts=0;
    int altsReadCounts[] = new int[mutantAlleles.length];

    for (int idx = 0; idx < mutantAlleles.length; idx++) {
      altTags[idx] = new TObjectIntHashMap<String>();
    }
    
    boolean mutantTagAdded = false;
    for (TagInfo tagInfo : tags.values()) {
      if(tagInfo.getReadCount() < filterTagCount)
        continue;

      if (!tagInfo.isMutant()) {
        refTags.put(tagInfo.getTag(), tagInfo.getBaseCounts().get(refBase));
        refReadCounts += tagInfo.getBaseCounts().get(refBase);
      } else {
        mutantTagAdded = true;
        if (tagInfo.getBaseCounts().containsKey(refBase)) {
          //disagreeTags.put(tagInfo.getTag(), tagInfo.getBaseCounts().get(refBase));
          nbDisagreeingTags++;
          refReadCounts += tagInfo.getBaseCounts().get(refBase);
        } else {
          int mutantAllelesCount=0;
          boolean disagreeTag = false;
          int mutIdx = -1;
          for (int idx = 0; idx < mutantAlleles.length; idx++) {
            int mutantCount = tagInfo.getBaseCounts().get(mutantAlleles[idx]);
            if(mutantCount > 0) {
              if(mutantAllelesCount > 0) {
                disagreeTag = true;
                mutIdx=-1;
              }
              else {
                mutIdx = idx;
              }
            }
            mutantAllelesCount += mutantCount;
            altsReadCounts[idx] += mutantCount;
          }
          
          if(disagreeTag) {
            //disagreeTags.put(tagInfo.getTag(), mutantAllelesCount);
            nbDisagreeingTags++;
          }
          else {
            altTags[mutIdx].put(tagInfo.getTag(), tagInfo.getBaseCounts().get(mutantAlleles[mutIdx]));
          }
        }
      }
    }

    // They might all be filtered out...
    if(mutantTagAdded) {
      TagVariant variant = new TagVariant(pileup.getChromosome(), pileup.getPosition(), refBase, mutantAlleles);
      variant.setDisagreeCount(nbDisagreeingTags);
  
      int refTagCounts[] = refTags.values();
      variant.setRefReadCount(refReadCounts);
      if(refTagCounts.length == 0) {
        variant.setAvgCountPerTagRef(0);
        variant.setHighestCountPerTagRef(0);
        variant.setLowestCountPerTagRef(0);
        variant.setMedCountPerTagRef(0);
        variant.setRefTagCount(0);
      }
      else {
        Arrays.sort(refTagCounts);
        variant.setAvgCountPerTagRef(average(refTagCounts));
        variant.setHighestCountPerTagRef(refTagCounts[refTagCounts.length-1]);
        variant.setLowestCountPerTagRef(refTagCounts[0]);
        variant.setMedCountPerTagRef(median(refTagCounts));
        variant.setRefTagCount(refTagCounts.length);
      }
  
      int altsTagCounts[][] = new int[mutantAlleles.length][];
      for (int idx = 0; idx < mutantAlleles.length; idx++) {
        altsTagCounts[idx] = altTags[idx].values();
        variant.getAltsReadCount()[idx] = altsReadCounts[idx];
        if(altsTagCounts[idx].length == 0) {
          variant.getAvgCountPerTagAlts()[idx] = 0;
          variant.getHighestCountPerTagAlts()[idx] = 0;
          variant.getLowestCountPerTagAlts()[idx] = 0;
          variant.getMedCountPerTagAlts()[idx] = 0;
          variant.getAltsTagCount()[idx] = 0;
        }
        else {
          Arrays.sort(altsTagCounts[idx]);
  
          variant.getAvgCountPerTagAlts()[idx] = average(altsTagCounts[idx]);
          variant.getHighestCountPerTagAlts()[idx] = altsTagCounts[idx][altsTagCounts[idx].length-1];
          variant.getLowestCountPerTagAlts()[idx] = altsTagCounts[idx][0];
          variant.getMedCountPerTagAlts()[idx] = median(altsTagCounts[idx]);
          variant.getAltsReadCount()[idx] = altsReadCounts[idx];
          variant.getAltsTagCount()[idx] = altsTagCounts[idx].length;
        }
      }
  
      tagVariants.add(variant);
    }// mutantTagAdded
  }

  public static int median(int values[]) {
    if (values.length % 2 == 1)
      return values[(values.length + 1) / 2 - 1];
    else {
      int lower = values[values.length / 2 - 1];
      int upper = values[values.length / 2];

      return (lower + upper) / 2;
    }
  }

  public static int average(int values[]) {
    if(values.length == 0)
      return 0;

    long total=0;
    for(int idx=0; idx < values.length; idx++) {
      total += (long)values[idx];
    }

    return (int)(total/(long)values.length);
  }

  private static class TagInfo {
    private final String tag;
    private final TByteIntMap baseCounts;
    private boolean mutant;

    public TagInfo(String tag) {
      super();
      this.tag = tag;
      this.baseCounts = new TByteIntHashMap();
      this.mutant = false;
    }

    public String getTag() {
      return tag;
    }

    public int getReadCount() {
      int retVal = 0;
      for(int idx=0; idx < baseCounts.values().length;idx++)
        retVal += baseCounts.values()[idx];
      return retVal;
    }

    public TByteIntMap getBaseCounts() {
      return baseCounts;
    }

    public boolean isMutant() {
      return mutant;
    }

    public void setMutant(boolean mutant) {
      this.mutant = mutant;
    }

  }
}
