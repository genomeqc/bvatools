/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.snpcall;

import gnu.trove.list.array.TLongArrayList;
import gnu.trove.map.TIntIntMap;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.map.hash.TObjectIntHashMap;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import htsjdk.samtools.SAMRecord;

import org.apache.commons.math3.util.Pair;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import ca.mcgill.genome.bvatools.pileup.ReadHandler;

public class TagDistribution implements ReadHandler {
  private final int tagLength;
  private final TObjectIntMap<String> distribution = new TObjectIntHashMap<String>();

  public TagDistribution(int tagLength) {
    this.tagLength = tagLength;
  }

  @Override
  public void readToPileup(SAMRecord read) {
    String tag = read.getReadName().substring(0, tagLength);
    distribution.adjustOrPutValue(tag, 1, 1);
  }

  public TObjectIntMap<String> getTagDistribution() {
    return distribution;
  }

  public void generateContent(File outputFile) throws IOException {
    TLongArrayList aBins = new TLongArrayList();
    TLongArrayList tBins = new TLongArrayList();
    TLongArrayList cBins = new TLongArrayList();
    TLongArrayList gBins = new TLongArrayList();
    TLongArrayList nBins = new TLongArrayList();
    for (String tag : distribution.keySet()) {
      tag = tag.replaceAll("-", "");
      for (int i = aBins.size(); i < tag.length(); i++) {
        aBins.add(0l);
        tBins.add(0l);
        cBins.add(0l);
        gBins.add(0l);
        nBins.add(0l);
      }

      for (int i = 0; i < tag.length(); i++) {
        char nucleotide = tag.charAt(i);
        if (nucleotide == 'A')
          aBins.set(i, aBins.get(i) + 1);
        else if (nucleotide == 'T')
          tBins.set(i, tBins.get(i) + 1);
        else if (nucleotide == 'C')
          cBins.set(i, cBins.get(i) + 1);
        else if (nucleotide == 'G')
          gBins.set(i, gBins.get(i) + 1);
        else if (nucleotide == 'N')
          nBins.set(i, nBins.get(i) + 1);
        else
          throw new RuntimeException("Unknown base: " + nucleotide);
      }
    }
    XYSeries aSerie = new XYSeries("%A");
    XYSeries tSerie = new XYSeries("%T");
    XYSeries cSerie = new XYSeries("%C");
    XYSeries gSerie = new XYSeries("%G");
    XYSeries nSerie = new XYSeries("%N");

    for (int i = 0; i < aBins.size(); i++) {
      double sum = aBins.get(i) + tBins.get(i) + cBins.get(i) + gBins.get(i) + nBins.get(i);
      aSerie.add(i + 1, aBins.get(i) * 100.0 / sum);
      tSerie.add(i + 1, tBins.get(i) * 100.0 / sum);
      cSerie.add(i + 1, cBins.get(i) * 100.0 / sum);
      gSerie.add(i + 1, gBins.get(i) * 100.0 / sum);
      nSerie.add(i + 1, nBins.get(i) * 100.0 / sum);
    }
    XYSeriesCollection contentCollection = new XYSeriesCollection();
    contentCollection.addSeries(aSerie);
    contentCollection.addSeries(tSerie);
    contentCollection.addSeries(cSerie);
    contentCollection.addSeries(gSerie);
    contentCollection.addSeries(nSerie);

    NumberAxis xAxis = new NumberAxis("Cycles");
    xAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
    xAxis.setFixedAutoRange(aBins.size());
    xAxis.setLowerBound(1);
    xAxis.setUpperBound(aBins.size());

    NumberAxis yAxis = new NumberAxis("% nb Sequences");
    yAxis.setRange(0, 100);
    yAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

    XYLineAndShapeRenderer contentRenderer = new XYLineAndShapeRenderer(true, false);
    contentRenderer.setSeriesPaint(0, Color.BLUE);
    contentRenderer.setSeriesPaint(1, Color.GREEN);
    contentRenderer.setSeriesPaint(2, Color.BLACK);
    contentRenderer.setSeriesPaint(3, Color.RED);
    contentRenderer.setSeriesPaint(3, Color.ORANGE);
    XYPlot contentPlot = new XYPlot(contentCollection, xAxis, yAxis, contentRenderer);
    contentRenderer.setBaseItemLabelsVisible(true);

    JFreeChart chart = new JFreeChart("Tag Sequence Content", JFreeChart.DEFAULT_TITLE_FONT, contentPlot, true);
    chart.setBackgroundPaint(Color.white);
    contentPlot.setBackgroundPaint(Color.white);
    ChartUtilities.saveChartAsPNG(outputFile, chart, 1152, 512);
  }

  public void generateDistribution(String outputPrefix) throws IOException {
    File graphOutput = new File(outputPrefix + ".tagDistribution.png");
    File valueOutput = new File(outputPrefix + ".tagDistribution.csv");

    HistogramDataset histogramdataset = new HistogramDataset();
    double values[] = new double[distribution.size()];
    int keyIdx = 0;
    double max = 0;
    TIntIntMap bins = new TIntIntHashMap();
    List<Pair<String, Integer>> pairs = new ArrayList<Pair<String, Integer>>();
    for (String key : distribution.keySet()) {
      int v = distribution.get(key);
      bins.adjustOrPutValue(v, 1, 1);
      pairs.add(new Pair<String, Integer>(key, v));
      values[keyIdx] = v;
      if (values[keyIdx] > max) {
        max = values[keyIdx];
      }
      keyIdx++;
    }
    pairs.sort((o1, o2) -> o2.getValue() - o1.getValue());
    PrintWriter out = new PrintWriter(valueOutput);
    for (Pair<String, Integer> pair : pairs) {
      out.print(pair.getKey());
      out.print(',');
      out.println(pair.getValue());
    }
    out.close();
    pairs = null;

    int[] valueDistribution = bins.values();
    Arrays.sort(valueDistribution);
    Arrays.sort(values);

    histogramdataset.addSeries("Seen Tag Counts", values, (int) max);
    JFreeChart chart = ChartFactory.createHistogram("Seen Tag Counts", null, null, histogramdataset, PlotOrientation.VERTICAL, false, false, false);
    chart.setBackgroundPaint(Color.white);
    XYPlot xyplot = (XYPlot) chart.getPlot();
    xyplot.setBackgroundPaint(Color.white);
    xyplot.setForegroundAlpha(0.85F);
    if(values[values.length-1] - values[0] > 100) {
      // Pick 99.9% of the samples, and lose the 0.1% since they are usually far out outliers
      xyplot.getDomainAxis().setRange(1, (int) values[(int) (values.length * 0.999)]+1);
    }
    else {
      xyplot.getDomainAxis().setRange(1, values[values.length-1]+1);
    }
    xyplot.getDomainAxis().setVerticalTickLabels(true);
    xyplot.getDomainAxis().setStandardTickUnits(NumberAxis.createIntegerTickUnits()); 
    XYBarRenderer xybarrenderer = (XYBarRenderer) xyplot.getRenderer();
    xybarrenderer.setDrawBarOutline(false);
    xybarrenderer.setBaseItemLabelsVisible(true);
    xybarrenderer.setBarPainter(new StandardXYBarPainter());
    ChartUtilities.saveChartAsPNG(graphOutput, chart, 1152, 512);
  }
}
