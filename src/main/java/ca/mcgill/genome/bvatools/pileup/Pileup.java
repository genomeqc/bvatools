/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.pileup;

import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;

import java.util.ArrayList;
import java.util.List;

import htsjdk.samtools.SAMRecord;

public class Pileup {
  private final String chromosome;
  private final int position;
  private final List<SAMRecord> reads = new ArrayList<SAMRecord>();
  private final TIntList readBasePositions = new TIntArrayList();

  public Pileup(String chromosome, int position) {
    this.chromosome = chromosome;
    this.position = position;
  }

  public void add(SAMRecord read, int readPosition) {
    reads.add(read);
    readBasePositions.add(readPosition);
  }

  public int getNbReads() {
    return readBasePositions.size();
  }

  public String getChromosome() {
    return chromosome;
  }

  public int getPosition() {
    return position;
  }

  public List<SAMRecord> getReads() {
    return reads;
  }

  public TIntList getReadBasePositions() {
    return readBasePositions;
  }
}
