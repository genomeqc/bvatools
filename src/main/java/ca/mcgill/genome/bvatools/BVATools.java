/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import ca.mcgill.genome.bvatools.commands.AlleleSpecificMethyl;
import ca.mcgill.genome.bvatools.commands.Bam2Fastq;
import ca.mcgill.genome.bvatools.commands.BaseFrequency;
import ca.mcgill.genome.bvatools.commands.BinnedReadCounter;
import ca.mcgill.genome.bvatools.commands.ClusterFrequency;
import ca.mcgill.genome.bvatools.commands.CompareFrequency;
import ca.mcgill.genome.bvatools.commands.CompareSamples;
import ca.mcgill.genome.bvatools.commands.CountVariantCaller;
import ca.mcgill.genome.bvatools.commands.DepthOfCoverage;
import ca.mcgill.genome.bvatools.commands.DepthRatioBAF;
import ca.mcgill.genome.bvatools.commands.ExtractSoftClips;
import ca.mcgill.genome.bvatools.commands.ExtractSnpEffTracks;
import ca.mcgill.genome.bvatools.commands.FilterDuplicates;
import ca.mcgill.genome.bvatools.commands.FrequencyLOH;
import ca.mcgill.genome.bvatools.commands.GenderEstimator;
import ca.mcgill.genome.bvatools.commands.GenePerPatient;
import ca.mcgill.genome.bvatools.commands.GroupFixMate;
import ca.mcgill.genome.bvatools.commands.MotifCISBPRNA;
import ca.mcgill.genome.bvatools.commands.MutationRates;
import ca.mcgill.genome.bvatools.commands.PlotSubstitutionRatios;
import ca.mcgill.genome.bvatools.commands.Rainfall;
import ca.mcgill.genome.bvatools.commands.ReadsQC;
import ca.mcgill.genome.bvatools.commands.ReplaceReadGroups;
import ca.mcgill.genome.bvatools.commands.SmallestInterval;
import ca.mcgill.genome.bvatools.commands.SplitRename;
import ca.mcgill.genome.bvatools.commands.TagSnpCall;
import ca.mcgill.genome.bvatools.commands.VCF2MAF;
import ca.mcgill.genome.bvatools.commands.VCFBAF;
import ca.mcgill.genome.bvatools.commands.VCFFrequency;
import ca.mcgill.genome.bvatools.commands.VCFPlots;
import ca.mcgill.genome.bvatools.commands.VariantAlleleFrequency;
import ca.mcgill.genome.bvatools.commands.VariantValidator;

public class BVATools {
  private static Map<String, DefaultTool> tools = new LinkedHashMap<String, DefaultTool>();

  static {
    // see htsjdk.samtools.Defaults.BUFFER_SIZE
    // Don't call htsjdk.samtools.Defaults before doing this or the static
    // block will run before we override the default Property.
    String bufferSize = System.getProperty("samjdk." + "buffer_size");
    if(bufferSize == null || bufferSize.isEmpty()) {
      int defaultBuffer = 4*1024*1024;
      System.setProperty("samjdk." + "buffer_size", String.valueOf(defaultBuffer));
    }

    DefaultTool tool;
    tool = new AlleleSpecificMethyl();
    tools.put(tool.getCmdName(), tool);
    tool = new Bam2Fastq();
    tools.put(tool.getCmdName(), tool);
    tool = new BaseFrequency();
    tools.put(tool.getCmdName(), tool);
    tool = new ClusterFrequency();
    tools.put(tool.getCmdName(), tool);
    tool = new CompareFrequency();
    tools.put(tool.getCmdName(), tool);
    tool = new CompareSamples();
    tools.put(tool.getCmdName(), tool);
    tool = new CountVariantCaller();
    tools.put(tool.getCmdName(), tool);
    tool = new DepthOfCoverage();
    tools.put(tool.getCmdName(), tool);
    tool = new DepthRatioBAF();
    tools.put(tool.getCmdName(), tool);
    tool = new ExtractSnpEffTracks();
    tools.put(tool.getCmdName(), tool);
    tool = new ExtractSoftClips();
    tools.put(tool.getCmdName(), tool);
    tool = new FilterDuplicates();
    tools.put(tool.getCmdName(), tool);
    tool = new FrequencyLOH();
    tools.put(tool.getCmdName(), tool);
    tool = new GenderEstimator();
    tools.put(tool.getCmdName(), tool);
    tool = new GenePerPatient();
    tools.put(tool.getCmdName(), tool);
    tool = new GroupFixMate();
    tools.put(tool.getCmdName(), tool);
    tool = new MotifCISBPRNA();
    tools.put(tool.getCmdName(), tool);
    tool = new MutationRates();
    tools.put(tool.getCmdName(), tool);
    tool = new PlotSubstitutionRatios();
    tools.put(tool.getCmdName(), tool);
    tool = new ReadsQC();
    tools.put(tool.getCmdName(), tool);
    tool = new ReplaceReadGroups();
    tools.put(tool.getCmdName(), tool);
    tool = new SplitRename();
    tools.put(tool.getCmdName(), tool);
    tool = new BinnedReadCounter();
    tools.put(tool.getCmdName(), tool);
    tool = new Rainfall();
    tools.put(tool.getCmdName(), tool);
    tool = new SmallestInterval();
    tools.put(tool.getCmdName(), tool);
    tool = new TagSnpCall();
    tools.put(tool.getCmdName(), tool);
    tool = new VariantAlleleFrequency();
    tools.put(tool.getCmdName(), tool);
    tool = new VariantValidator();
    tools.put(tool.getCmdName(), tool);
    tool = new VCF2MAF();
    tools.put(tool.getCmdName(), tool);
    tool = new VCFBAF();
    tools.put(tool.getCmdName(), tool);
    tool = new VCFFrequency();
    tools.put(tool.getCmdName(), tool);
  }

  /**
   * @param args
   */
  public static void main(String[] args) {
    if (args.length < 1) {
      printUsageAndExit();
    }

    DefaultTool tool = tools.get(args[0]);
    if (tool == null) {
      printUsageAndExit();
    }

    LinkedList<String> arguments = new LinkedList<String>();
    for(int i=1; i < args.length; i++) {
      arguments.add(args[i]);
    }
    args = null;

    LinkedList<String> unusedArgs = new LinkedList<String>();
    int exitCode = tool.parseArgs(arguments, unusedArgs);
    if(exitCode != 0)
      System.exit(exitCode);

    if(!arguments.isEmpty() || !unusedArgs.isEmpty()) {
      tool.printUsage("Unused arguments: " + StringUtils.join(unusedArgs, ' '));
        System.exit(1);
    }
    try {
      System.exit(tool.run());
    }
    catch(Throwable e) {
      e.printStackTrace();
      System.exit(1);
    }
  }

  private static void printUsageAndExit() {
    System.out.println("BVATools v1.7-dev");
    System.out.println("Usage: BVATools <cmd>");
    for (String toolName : tools.keySet()) {
      System.out.print('\t');
      System.out.println(toolName);
    }
    System.exit(1);
  }
}
