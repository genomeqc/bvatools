/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package htsjdk.samtools;

import java.util.Set;

import htsjdk.samtools.SAMRecord.SAMTagAndValue;

public class TextTagCodecBVAWriter {
  private TextTagCodec tagCodec = new TextTagCodec();
  
  public String format(Set<String> tagsName, SAMRecord read) {
    StringBuilder retVal = new StringBuilder();
    for(SAMTagAndValue tagAndValue : read.getAttributes()) {
      if(tagsName.contains(tagAndValue.tag)) {
        retVal.append(' ');
        if(read.isUnsignedArrayAttribute(tagAndValue.tag)) {
          retVal.append(tagCodec.encodeUnsignedArray(tagAndValue.tag, tagAndValue.value));
        }
        else {
          retVal.append(tagCodec.encode(tagAndValue.tag, tagAndValue.value));
        }
      }
    }
    return retVal.toString();
  }
}
