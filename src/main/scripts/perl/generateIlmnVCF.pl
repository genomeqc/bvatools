#!/usr/bin/perl

use strict;

my $sampleName=$ARGV[0];
my $pennCNVFile=$ARGV[1];
my $fixedManifestFile=$ARGV[2];

open(FIXED, $fixedManifestFile) or die "Can't open $fixedManifestFile\n";
my %probeDetails;
while(<FIXED>) {
	chomp;
	my @values = split(/,/);

	$probeDetails{$values[2]} = ();
	$probeDetails{$values[2]}->{"chr"} = $values[0];
	$probeDetails{$values[2]}->{"pos"} = $values[1];
	$probeDetails{$values[2]}->{"probeID"} = $values[2];
	$probeDetails{$values[2]}->{"refAllele"} = $values[3];
	$probeDetails{$values[2]}->{"alleleA"} = $values[4];
	$probeDetails{$values[2]}->{"alleleB"} = $values[5];
}
close(FIXED);

print STDOUT <<EOF
##fileformat=VCFv4.1
##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">
##FORMAT=<ID=DP,Number=1,Type=Integer,Description="Faked read depth">
##FORMAT=<ID=GQ,Number=1,Type=Integer,Description="Faked Genotype Quality">
##FORMAT=<ID=LRR,Number=1,Type=Float,Description="Log R Ratio">
##FORMAT=<ID=BAF,Number=1,Type=Float,Description="B Allele Frequency">
#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	$sampleName
EOF
;

#ProbeSetName    Call

open(PENNCNV, $pennCNVFile) or die "Can't open $pennCNVFile\n";
$_ = <PENNCNV>;
chomp;
my @values = split(/\t/);
my $chrIdx = -1;
my $posIdx = -1;
my $gTypeIdx = -1;
my $nameIdx = -1;
my $lrrIdx = -1;
my $bafIdx = -1;

for(my $idx=0; $idx < @values; $idx++) {
  if($values[$idx] eq "Name") {
    $nameIdx = $idx;
  }
  elsif($values[$idx] eq "Chr") {
    $chrIdx = $idx;
  }
  elsif($values[$idx] eq "Position") {
    $posIdx = $idx;
  }
  elsif($values[$idx] =~ /GType/) {
    $gTypeIdx = $idx;
  }
  elsif($values[$idx] =~ /Log R Ratio/) {
    $lrrIdx = $idx;
  }
  elsif($values[$idx] =~ /B Allele Freq/) {
    $bafIdx = $idx;
  }
}

while(<PENNCNV>) {
	chomp;
	@values = split(/\t/);
	if($values[$gTypeIdx] eq 'NC' || $values[$gTypeIdx] eq 'UU') {
		next;
	}
	if($values[$gTypeIdx] ne "AA" && $values[$gTypeIdx] ne "AB" && $values[$gTypeIdx] ne "BB") {
		die "Unknown call: ".$values[$nameIdx]."\n";
	}
	elsif(defined($probeDetails{$values[$nameIdx]})) {
		my $lineToPrint="";
		$lineToPrint .= $probeDetails{$values[$nameIdx]}->{"chr"};
		$lineToPrint .= "\t".$probeDetails{$values[$nameIdx]}->{"pos"};
		$lineToPrint .= "\t".'.';
		$lineToPrint .= "\t".$probeDetails{$values[$nameIdx]}->{"refAllele"};
		my $gtValue = $values[$gTypeIdx];
    my $genotype;
    if($probeDetails{$values[$nameIdx]}->{"refAllele"} eq $probeDetails{$values[$nameIdx]}->{"alleleA"}) {
		  $lineToPrint .= "\t".$probeDetails{$values[$nameIdx]}->{"alleleB"};
			if($values[$gTypeIdx] eq 'BB') {
        $genotype = '1/1';
      } elsif($values[$gTypeIdx] eq 'AB') {
        $genotype = '0/1';
      } elsif($values[$gTypeIdx] eq 'AA') {
        $genotype = '0/0';
      }
    } elsif($probeDetails{$values[$nameIdx]}->{"refAllele"} eq $probeDetails{$values[$nameIdx]}->{"alleleB"}) {
      $lineToPrint .= "\t".$probeDetails{$values[$nameIdx]}->{"alleleA"};
			if($values[$gTypeIdx] eq 'AA') {
        $genotype = '1/1';
      } elsif($values[$gTypeIdx] eq 'AB') {
        $genotype = '0/1';
      } elsif($values[$gTypeIdx] eq 'BB') {
        $genotype = '0/0';
      }
    } else {
      print STDERR "Allele != Ref: ".$values[$nameIdx]."\n";
      next;
    }

		if($genotype eq '0/0') {
			# Not a genotype
			next;
		}

    $lineToPrint .= "\t999";
		$lineToPrint .= "\t".'.';
		$lineToPrint .= "\t".'.';
		$lineToPrint .= "\t".'GT:DP:LRR:BAF:GQ';
		$lineToPrint .= "\t".$genotype.':100:'.$values[$lrrIdx].':'.$values[$bafIdx].':99';
		print $lineToPrint."\n";
	}
}
close(PENNCNV);
